#testData.py

import datetime, os

os.system('rm ../db.db')

from red.config import config

config.add_section('Database')
config.set('Database', 'connectionstring', 'sqlite:///../db.db')

from models.testdata import createEmptyDb

createEmptyDb()

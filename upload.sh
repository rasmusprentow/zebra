#!/bin/bash

if [ -z "$1" ]
then
    ip="192.168.7.2"
else 
    ip=$1
fi

rsync -avz * --delete --exclude=tests  --exclude=*.pyc root@$ip:/opt/zebra/

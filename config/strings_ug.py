#strings_ug.py

#error strings

GENERAL_ERROR = "An error occurred"
INVALID_PIN = "Invalid pin"
PIN_TOO_SHORT = "Pin too short"
INVALID_POCKET = "The card was invalid"
INSUFFICIENT_COIN = 'Card out of credit'
OVERLIMIT_COIN = 'The transaction violates the limit'
SERIAL_NOT_FOUND = 'Invalid card'
INSUFFICIENT_VOUCHER = "Card out of discount points"
OLD_DID_NOT_EXIST = "Old card did not exists"
NEW_POCKET_EXISTED = "New card already registered"
ALREADY_REGISTERED = "Already registered"

#success strings
PURCHASE = "Successful purchase"
INSERT_COINS = "Successfully inserted coins"
INSERT_DISCOUNT = "Successfully inserted discount"
VOUCHER_PURCHASE = "Successful voucher purchase"
RECEIVED_COINS = 'Success. You received: '
RECEIVED_DISCOUNT = 'Success. You received: '

#cancel strings
CUSTOMER_CANCEL = "Customer canceled"
CASHIER_CANCEL = "Cashier canceled"
YOU_CANCEL = "You canceled"

#loading strings
VALIDATING_POCKET = "Validating Pocket"
GENERAL_LOADING = "Please wait"
CASHIER_LOADING = "Please wait for cashier"
BALANCE_LOADING = "Customer is checking balance"


#Products
PRODUCT1 = 'Toilet'
PRODUCT2 = 'Bath'

#General
PRESS_SCREEN_TO_CONTINUE = "Press Screen to continue"



def prepro(product):
    """ Returns Pretifiedd version of the product """
    if product == 'product1':
        return PRODUCT1
    elif product == 'product2':
        return PRODUCT2
    else:
        return product


# Credit, Coins, UGX
#purchaseReceit.py

from activities.customer.customerActivity import CustomerActivity
from red.config import config, get_config
from config.strings import *


class Purchasereceipt(CustomerActivity):
    """docstring for Purchase"""
    
    def onCreate(self, data):

        self.setLayout("common/purchaseReceipt")
        self.invokeLayoutFunction("updateAmount", data['amount'])
        self.invokeLayoutFunction("updateVoucherAmount", data['vamount'])
        self.invokeLayoutFunction("updateProduct", prepro(data['product']))
        self.invokeLayoutFunction("updateTotalVoucherAmount", data['balance_vamount'])
        self.invokeLayoutFunction("updateStack",[(data['balance_vamount'] - data['vamount']),  (data['vamount'])] )
        
        # notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        # self.setTimedActivity('welcome', notificationSleepTime)

    def _receiveCashierMessage(self, msg):
        self.logger.debug(msg)



 #    def receiveDisplayMessage(self, message):
 #        if message['head'] == 'button_clicked':
 #            if message['data'] == 'cancel':
 #                self.switchActivity('welcome')
 # 
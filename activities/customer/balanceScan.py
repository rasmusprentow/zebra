#scanPocket.py

""" Scanpocket """

from activities.customer.customerActivity import CustomerActivity
from threading import Timer
from activities.scanPocketMixin import ScanPocketMixin

class Balancescan(CustomerActivity, ScanPocketMixin):
    """docstring for Selectproduct"""
    
    def onCreate(self, data=None):
        self.layout = 'common/balanceScan' ## Brug generisk layout i stedet
        self.setLayout(self.layout)
        self.m_onCreate()

    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     
    def _receiveCashierMessage(self, message):
        if message['head'] == 'valid_pocket':
            self.switchActivity('balance', {'snr' : message['data']['snr']})                        
        elif message['head'] == "error":
            self.setErrorLayout(message=message['data'], nextActivity='welcome', time=self.errorNotificationSleepTime)

    def onPocket(self, snr):
        self.snr = snr
        self.send('cashier',{'head' : 'validate_pocket' , 'data' : {'snr' : snr}})
      

    def receiveDisplayMessage(self, message):
        self.m_receiveDisplayMessage(message)
        
    def onCancel(self):
        self.switchActivity('welcome')
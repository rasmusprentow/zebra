#purchaseReceit.py

from activities.customer.customerActivity import CustomerActivity
from red.config import config, get_config
from config.strings import *


class Buycoinreceipt(CustomerActivity):
    """docstring for Purchase"""
    
    def onCreate(self, data):

        self.setLayout("customer/addCurrencyReceipt")
        if data['type'] == 'discount':
            self.invokeLayoutFunction("updateECoin", data['amount'])
            currency = "Discount Points"
        else: 
            self.invokeLayoutFunction("updateVoucher",  data['amount'])
            currency = "E-Coins"
        
        self.invokeLayoutFunction("updateCurrency", currency)

    
 
        # notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        # self.setTimedActivity('welcome', notificationSleepTime)

    def _receiveCashierMessage(self, msg):
        self.logger.debug(msg)



 #    def receiveDisplayMessage(self, message):
 #        if message['head'] == 'button_clicked':
 #            if message['data'] == 'cancel':
 #                self.switchActivity('welcome')
 # 
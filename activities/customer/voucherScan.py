#voucherPurchase

from activities.customer.customerActivity import CustomerActivity
from activities.scanPocketMixin import ScanPocketMixin
from config.strings import *
from red.config import config, get_config


class Voucherscan(CustomerActivity, ScanPocketMixin):
    """docstring for Voucherscan"""
    
    def onCreate(self, data=None):
        self.layout = 'customer/voucherScan' ## Brug generisk layout i stedet
        self.setLayout(self.layout)
        self.invokeLayoutFunction("updateAmount", data['amount'])
        self.m_onCreate()
  
    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     
    def _receiveCashierMessage(self, message):
        if message['head'] == 'valid_pocket':
            self.switchActivity('authenticate', {'snr' : message['data']['snr']})
        elif message['head'] == 'error':
            if "data" in message:
                errorMsg = message["data"]
            else:
                errorMsg = GENERAL_ERROR
            self.setErrorLayout(message=errorMsg, nextActivity='welcome', time=self.errorNotificationSleepTime)


    def onPocket(self, snr):
        self.send('cashier',{'head' : 'validate_pocket' , 'data' : {'snr' : snr}})
      

    def receiveDisplayMessage(self, message):
        self.m_receiveDisplayMessage(message)
        
    def onCancel(self):
        self.send("cashier", {'head' : "button_clicked", 'data' : 'cancel'})
        self.setLoadingScreen(GENERAL_LOADING)
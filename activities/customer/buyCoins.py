#buyCoins.py

from activities.customer.customerActivity import CustomerActivity
from activities.scanPocketMixin import ScanPocketMixin
from red.config import config, get_config
from config.strings import * 

class Buycoins(CustomerActivity, ScanPocketMixin):
    """docstring for Buycoins"""
    
    def onCreate(self, data=None):
        self.setLayout("customer/buycoins")
        self.amount = data['amount']
        self.type = data['type']
        if self.type == "discount":
            self.invokeLayoutFunction("updateAmountText", " - " + str(self.amount) + ' Discount Points')  
        else:
            self.invokeLayoutFunction("updateAmountText", " - " + str(self.amount) + ' E-Coins')
        self.m_onCreate()
        self.notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        self.errorNotificationSleepTime = get_config(config, 'Timeouts', 'error_notification', ctype=int)


    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     
  
    
    def onPocket(self, snr):
        self.send('cashier',{'head': 'pocket_received', 'data' : snr})
        

    def receiveDisplayMessage(self, message):
        if message["head"] == "button_clicked":
            if message["data"] == "cancel":
                self.send("cashier", message)
                self.setLoadingScreen(YOU_CANCEL)


    def _receiveCashierMessage(self, message):
        if message["head"] == "success":
            self.switchActivity('buycoinReceipt', {'type':self.type, 'amount':self.amount}) 

            #self.setSuccessLayout(message=RECEIVED_COINS + str(self.amount) + " " + currency, nextActivity='welcome', time=self.notificationSleepTime)
        elif message["head"] == "error":
            if "data" in message:
                errorMsg = message["data"]
            else:
                errorMsg = GENERAL_ERROR
            self.setErrorLayout(message=errorMsg, nextActivity='welcome', time=self.errorNotificationSleepTime)
       
        else: 
            self.logger.info("Unknown message: " + str(message))


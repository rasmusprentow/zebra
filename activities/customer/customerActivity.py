


from red.activity import Activity
from config.strings import * 
from red.config import config, get_config

class CustomerActivity(Activity):
    
    def __init__(self, kernel):
        super(CustomerActivity, self).__init__(kernel)
        self.notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        self.errorNotificationSleepTime = get_config(config, 'Timeouts', 'error_notification', ctype=int)

    def receiveCashierMessage(self, message):
        #if message['head'] == 'balance':
        #    self.switchActivity(('balance'), message['data'])
        if message['head'] == "switch_activity":
            time = 0
            if 'time' in message['data']:
                time = message['data']['time']
            
            self.setTimedActivity(message['data']['activity'], time=time)
        else:
            self.logger.debug(message)
            self._receiveCashierMessage(message)




    def receiveDisplayMessage(self, message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'cancel':
                self.switchActivity('welcome')
 
        self._receiveDisplayMessage(message)
#welcome.py

from activities.customer.customerActivity import CustomerActivity
from config.strings import * 

class Welcome(CustomerActivity):
    """docstring for Welcome"""
    
    def onCreate(self, data=None):
        self.setLayout("standAlone/welcome")
        #self.setLoadingScreen(CASHIER_LOADING)
        
        self.send('cashier', {'head' : 'finished'}) #Tell cashier we are fineshed doing whatever

    
    def receiveCashierMessage(self, message):

        if message['head'] == 'insert_coins':
            self.switchActivity('buyCoins', {"amount" :message['data'], "type" : "coins"})
        elif message['head'] == 'add_discount':
            self.switchActivity('buyCoins', {"amount" :message['data'], "type" : "discount"})
        elif message['head'] == 'purchase':
            self.switchActivity('purchase', message['data'])
        elif message['head'] == 'voucher_purchase':
            self.switchActivity("voucherScan", {'amount' : message['data']['amount']})
        elif message['head'] == 'check_balance':
            self.switchActivity('balanceScan')
        elif message['head'] == 'update':
            self.switchActivity('update')
        elif message['head'] == 'shutdown':
            self.switchActivity('shutdown')   
        elif message['head'] == 'setlayout':
            if "data" in message:
                if isinstance(message['data'], dict):
                    self.setLayout(message['data']['layout'])
                    self.invokeLayoutFunction(message['data']['function'],message['data']['param'])
                elif message["data"] == "loading":
                    self.setLoadingScreen(CASHIER_LOADING)
                elif message["data"] == "welcome":
                    self.setLayout("standAlone/welcome")

        elif message['head'] == 'error':
            self.setErrorLayout(message["data"], nextActivity='welcome', time=self.errorNotificationSleepTime)
       
           


    def receiveDisplayMessage(self, message):
         if message['head'] == 'button_clicked':
            if message['data'] == 'screen':
                self.send('cashier', {'head' : 'checking_balance'})
   
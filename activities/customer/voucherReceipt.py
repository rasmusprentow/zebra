from activities.customer.customerActivity import CustomerActivity
from red.config import config, get_config

class Voucherreceipt(CustomerActivity):
    """docstring for Voucherpurchase"""
    
    def onCreate(self, data):
        self.setLayout("customer/voucher")
        self.invokeLayoutFunction("updateAmount", data['amount'])
        self.invokeLayoutFunction("updateTotalAmount", data['balance_vamount'])

        self.invokeLayoutFunction("updateStack",[(data['balance_vamount'] + data['amount']),  - (data['amount'])] )
        
        # notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        # self.setTimedActivity('welcome', notificationSleepTime)


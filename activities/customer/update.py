



 
from activities.cashier.cashierActivity import CashierActivity
import subprocess
import time
class Update(CashierActivity):
    


    def onCreate(self, data=None):
        #self.setLoadingScreen(message="Updating")
           # self.invokeLayoutFunction("updateSubInfoText", "Pulling data")
       # self.invokeLayoutFunction("updateSubInfoText", "Pulling data")
        # self.finished = False
        self.runCommand("git pull") 
        self.runCommand("git submodule update") 
        # self.invokeLayoutFunction("updateSubInfoText", "Updating Customer Side")
        self.send("cashier", {"head":"finished_updating"})
        time.sleep(1)
        self.kernel.stop()
        self.kernel.app.quit()
        self.runCommand("shutdown -r now") 
        # self.setSuccessLayout(message="Updated Successfully", nextActivity='welcome', time=self.notificationSleepTime)
        
            


    def runCommand(self, cmd, message=""):
        self.send("cashier", {"head":"progress", "data": cmd})
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        self.logger.debug(process.communicate()[0])
        





#welcome.py

from activities.customer.customerActivity import CustomerActivity
from config.strings import * 

class Login(CustomerActivity):
    """docstring for Welcome"""
    
    def onCreate(self, data=None):
        self.setLoadingScreen(CASHIER_LOADING)
        
    
    def receiveCashierMessage(self, message):

        if message['head'] == 'welcome':
            self.switchActivity('welcome')
      

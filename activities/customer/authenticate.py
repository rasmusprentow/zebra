from activities.customer.customerActivity import CustomerActivity
from activities.authenticateMixin import AuthenticateMixin
from config.strings import *

class Authenticate(CustomerActivity, AuthenticateMixin):
    """docstring for Authenticate"""
    
    def onCreate(self, data=None):
        # Perform check on data
        self.m_onCreate(data)  
        self.onAccept(); # Technically disable pin NOPIN


    def receiveDisplayMessage(self,message):
          
        if message['head'] == 'continue_e':
            self.setLayout("common/authenticate")
 
        self.m_receiveDisplayMessage(message)

    def _receiveCashierMessage(self, message):
        #if message['head'] == 'balance':
        #    self.switchActivity(('balance'), message['data'])
        if message['head'] == 'purchaseReceipt':
            self.switchActivity('purchaseReceipt', message['data'])
        if message['head'] == 'voucherReceipt':
            self.switchActivity('voucherReceipt', message['data'])
        elif message['head'] == 'pin_error':
            self.clearPin()
            self.m_wrongPinError()
        elif message['head'] == 'error':
            self.clearPin()
            if 'data' in message:
                errorMsg = message["data"]
            else:
                errorMsg = GENERAL_ERROR
            self.setErrorLayout(message=errorMsg, nextActivity="welcome", time=self.errorNotificationSleepTime)

    def onAccept(self):
        self.send('cashier',{'head' : 'pin_received' , 'data' : {'snr' : self.snr, 'pin' : self.pin}})


    def onCancel(self):
        self.send("cashier", {'head' : 'button_clicked', 'data' : 'cancel'})
        self.setLoadingScreen(YOU_CANCEL)
#balance.py


from activities.customer.customerActivity import CustomerActivity
from red.config import config, get_config

class Balance(CustomerActivity):
    """docstring for Balance"""
    
    def onCreate(self, data=None):
        self.send("cashier", {"head" : "get_balance", 'data' : {'snr' : data['snr']}})
        
    def receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if "data" in message and message["data"] == "cancel":
                self.switchActivity('welcome')

    def _receiveCashierMessage(self, message):
        if message['head'] == 'balance':
            data = message['data']
            self.setLayout("customer/balance");   
            self.amount = data['amount']
            self.vamount = data['vamount']
            self.invokeLayoutFunction("updateAmount", self.amount)
            self.invokeLayoutFunction("updateVoucherAmount", self.vamount)
            self.invokeLayoutFunction("updateStack",[self.vamount,  0] )
         
            #self.setTimedActivity('welcome', self.notificationSleepTime)
         
        elif message['head'] == 'error':
            self.setErrorLayout(message=message["data"],nextctivity='welcome', time=self.errorNotificationSleepTime)


    def onPocket(self, snr):
        self.send('cashier',{'head' : 'pocket' , 'data' : {'snr' : snr}})
          
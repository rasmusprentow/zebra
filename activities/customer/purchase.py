#purchase.py

from activities.customer.customerActivity import CustomerActivity
from activities.scanPocketMixin import ScanPocketMixin
from config.strings import *


class Purchase(CustomerActivity, ScanPocketMixin):
    """docstring for Purchase"""
    
    def onCreate(self, data=None):
        self.amount = data['amount']
        self.product = data['product']
        self.setLayout("common/purchaseScan")
        self.invokeLayoutFunction('updateProduct', prepro(self.product))
        self.invokeLayoutFunction("updateAmount", self.amount)
        self.m_onCreate()

    def receiveDisplayMessage(self, message):
        if message["head"] == "button_clicked":
            if message["data"] == "cancel":
                self.send("cashier", message)
                self.setLoadingScreen(YOU_CANCEL)
  
    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)

    def onPocket(self, snr):
        self.send('cashier',{'head': 'validate_pocket', 'data' : {'snr' : snr}})
  
    def _receiveCashierMessage(self, message):
        if message['head'] == "valid_pocket":
            self.switchActivity('authenticate', {'snr' : message['data']['snr']})
        elif message["head"] == "error":
            self.setLayout("error")
            if "data" in message:
                errorMsg = message["data"]
            else:
                errorMsg = GENERAL_ERROR

            self.setErrorLayout(message=errorMsg, nextActivity='welcome', time=self.errorNotificationSleepTime)

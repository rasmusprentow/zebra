
#management.py


from activities.cashier.cashierActivity import CashierActivity
# from models.model import Pocket, Shop
# from red.config import config
# from helpers.helperFactory import HelperFactory
# from helpers.helperFactory import HelperFactory
# from excep.excep import ConfigException
# from config.strings import *


class System(CashierActivity):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.setLayout("cashier/system")
        self.returnActivity = "management"
        # self.authenticator = HelperFactory.getAuthenticator(self.session)

    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'update':
                self.switchActivity('update')
            elif message['data'] == 'shutdown':
                self.switchActivity('shutdown')
          
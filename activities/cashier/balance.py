#welcome.py

from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket, Shop
from red.config import config
from helpers.helperFactory import HelperFactory
from helpers.transactor import Transactor
from excep.excep import ConfigException
from config.strings import *


class Balance(CashierActivity):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.setLoadingScreen(BALANCE_LOADING)
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.send("customer", {'head' : 'check_balance'})

    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'purchase':
                self.switchActivity('purchase')
            elif message['data'] == 'insertCoins':
                self.switchActivity('enterAmount', {'next_activity' : 'insertCoins'})
            elif message['data'] == 'voucherPurchase':
                self.switchActivity('enterAmount', {'next_activity' : 'voucherPurchase'})
        
    def _receiveCustomerMessage(self, message):
        if message['head'] == 'get_balance':
            if 'snr' in message['data']:
                self._showBalance(message['data']['snr'])

        elif message['head'] == 'validate_pocket':
            self._validatePocket(message['data']['snr'])

        elif message['head'] == 'button_clicked':
            if "data" in message and message["data"] == "cancel":
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome", 'time' : self.errorNotificationSleepTime}})
                self.setErrorLayout(message=CUSTOMER_CANCEL, nextActivity='main', time=self.errorNotificationSleepTime)
                
    
    def _showBalance(self, snr):
        
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        if pocket == None:
            self.send('customer', {'head' : 'error', 'data' : INVALID_POCKET})
            self.setErrorLayout(message=INVALID_POCKET)
            return
        
        shopName = config.get('Shop', 'name')
        shop = self.session.query(Shop).filter_by(name=shopName).first()
        if shop == None:
            raise ConfigException("Shop was not in customer")
        
        voucher = Transactor.getShopVoucher(pocket, shop)
        if voucher == None:
            vamount = 0
        else:
            vamount = voucher.amount


        self.send('customer',{'head' : 'balance', 'data' : {'amount' : pocket.safe.amount, 'vamount' : vamount}})
        
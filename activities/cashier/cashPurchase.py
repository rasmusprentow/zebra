#insertCoins.py

from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from excep.excep import InsufficientCoinException, SerialNotFoundException
from red.config import config, get_config
from config.strings import *
import traceback
class Cashpurchase(CashierActivity):
    """docstring for Insertcoins"""
    
    def onCreate(self, data=None):
        self.setLayout("standAlone/selectproduct")

            
    
    def _receiveDisplayMessage(self, message):
        """docstring for receiveDisplayMessage"""
        if message['head'] == 'button_clicked':
            if 'product' in message['data']:
                self.product = message['data']
                self.amount = int(config.get('Products', self.product))
                self.setLayout('cashier/cashbuy')
                self.invokeLayoutFunction("updateText", "Receive " + str(self.amount) + " from Customer")
                self.send("customer", {"head" : "setlayout" , "data" : {'layout':"common/notification", "function" : "updateText", 'param' : "Pay " + str(self.amount) + " UGX"}})
            elif message["data"] == 'paid':
                # TODO insert Logic here

                try: 
                    HelperFactory.getTransactor(self.session).cashPurchase(self.amount)
                except: 
                    self.logger.fatal("Error: " + traceback.format_exc())
                self.send("customer", {"head" : "setlayout" , "data" : "success" }) #{'layout':"common/success", "function" : "updateSuccessText", 'param' : "Success"}})
                self.setSuccessLayout(message="Success", nextActivity='main', time=self.notificationSleepTime)
                #self.switchActivity('main')
            elif message["data"] == 'cancel':
                self.switchActivity('main')
            elif message["data"] == 'continue_s':
                self.switchActivity('main')
            else:
                self.logger.warning('Unknown product')
          
     
  
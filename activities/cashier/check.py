



#reregister.py


from activities.scanPocketMixin import ScanPocketMixin
from activities.cashier.cashierActivity import CashierActivity
# from models.model import Pocket, Shop
# from red.config import config
from helpers.helperFactory import HelperFactory
# from helpers.helperFactory import HelperFactory
# from excep.excep import ConfigException
from config.strings import *
from excep.excep import SerialNotFoundException,PocketAlreadyRegistered,InvalidSerialException
from models.model import Pocket, Shop
from red.config import config

from helpers.transactor import Transactor
from excep.excep import ConfigException

class Check(CashierActivity, ScanPocketMixin):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        
        self.setLayout("cashier/checkScan")
        self.m_onCreate()
        

    def initialize(self):
        self.m_onCreate()      

    def getVoucherBalance(self, pocket):
        shopName = config.get('Shop', 'name')
        shop = self.session.query(Shop).filter_by(name=shopName).first()
        if shop == None:
            raise ConfigException("Shop was not in customer")
        
        voucher = Transactor.getShopVoucher(pocket, shop)
        if voucher == None:
            vamount = 0
        else:
            vamount = voucher.amount
        return vamount

    def onPocket(self, snr):
        try:
            decsnr = self.authenticator.snrToDecsnr(snr) 
            pin = self.authenticator.getPin(snr)     
            pocket = self.session.query(Pocket).filter_by(snr=snr).first()
            self.setSuccessLayout(message="Pin: " + str(pin) + "\nDiscount Balance: " + str(self.getVoucherBalance(pocket)) + "\nE-Coin Balance: " + str(pocket.safe.amount) + "\nCardno.: " + str(decsnr) + "\n"+ PRESS_SCREEN_TO_CONTINUE)
        except PocketAlreadyRegistered:
            self.setErrorLayout(message=NEW_POCKET_EXISTED, nextActivity='management', time=self.errorNotificationSleepTime)
                
        except SerialNotFoundException:
            self.setErrorLayout(message=INVALID_POCKET, nextActivity='management', time=self.errorNotificationSleepTime)
        
        except InvalidSerialException:
            self.setErrorLayout(message=INVALID_POCKET, nextActivity='management', time=self.errorNotificationSleepTime)  

    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     
    def _receiveDisplayMessage(self, message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'cancel':
                self.switchActivity('management')
            elif message['data'] == 'continue_s':   # From the notification
                self.switchActivity('management')
            elif message["data"] == 'continue_e':
                self.switchActivity('management')    
        self.m_receiveDisplayMessage(message)
    

    def onCancel(self):
        self.switchActivity("management")
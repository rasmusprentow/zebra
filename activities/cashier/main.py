#welcome.py

from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket, Shop
from red.config import config
from helpers.helperFactory import HelperFactory
from helpers.helperFactory import HelperFactory
from excep.excep import ConfigException
from config.strings import *

from red.utils.run_once import run_once
   
import datetime, threading, traceback

class Main(CashierActivity):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.setLayout("cashier/main")
        self.send("customer", {"head" : "welcome"})
        self.send("customer", {"head" : "setlayout" , "data" : "welcome"})
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.startTimeUpdater()
   
        try: 
            pass
            self.startHwClock()
        except:
            self.logger.debug("Failed to start Hardware clock got: " + traceback.format_exc())
        
    @run_once
    def startHwClock(self):
        bashCommand = "systemctl start rtc-ds1307.service"
        import subprocess
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.logger.info(process.communicate()[1])
        #self.logger.info(process.communicate()[1])
 
 
    @run_once
    def startTimeUpdater(self):
        pass 

        #self.updatetime()

    def updatetime(self):
        threading.Timer(1.0, self.updatetime).start()
        self.invokeLayoutFunction("setTime", str(datetime.datetime.utcnow()) )
      #  self.invokeLayoutFunction("setTime", datetime.datetime.utcnow()) 
    
    
    def receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            self.send("customer", {"head" : "setlayout" , "data" : "loading"})
            if message['data'] == 'purchase':
                self.switchActivity('purchase')
            elif message['data'] == 'insertCoins':
                self.switchActivity('enterAmount', {'next_activity' : 'insertCoins'})
            elif message['data'] == 'voucherPurchase':
                self.switchActivity('enterAmount', {'next_activity' : 'voucherPurchase'})
            elif message['data'] == 'management':
                self.switchActivity('management')
            elif message['data'] == 'checkinout':
                self.switchActivity('checkinout')  
            elif message['data'] == 'cashPurchase':
                self.switchActivity('cashPurchase') 
            elif message['data'] == 'register':
                self.switchActivity('register')
                 
    def _receiveCustomerMessage(self, message):

        if message['head'] == 'checking_balance':
            self.switchActivity('balance')



    def periodic(self, scheduler, interval, action, actionargs=()):
        scheduler.enter(interval, 1, periodic,
                      (scheduler, interval, action, actionargs))
        action(*actionargs)
 
from activities.cashier.cashierActivity import CashierActivity
class Enteramount(CashierActivity):
    
    def onCreate(self, data=None):
        self.setLayout("cashier/insertcoins")
        self.amount = ''
        if data == None or 'next_activity' not in data:
            raise ValueError('No activity send to enterAmount')
        else:
            self.nextActivity = data['next_activity']

    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if 'num' in message['data']:
                if len(self.amount) < 5:
                    self.amount += message['data'][3]
                self.invokeLayoutFunction("updateAmountText", self.amount)
            elif message['data'] == 'backspace':
                self.amount = self.amount[:-1]
                self.invokeLayoutFunction("updateAmountText", self.amount)
            elif message['data'] == 'accept':
                if len(self.amount) > 0:
                    self.switchActivity(self.nextActivity, {'amount' : self.amount})
            elif message['data'] == 'cancel':
                if self.nextActivity == "addDiscount":
                    self.switchActivity('management')
                else:
                    self.switchActivity('main')
#register.py


from activities.scanPocketMixin import ScanPocketMixin
from activities.cashier.cashierActivity import CashierActivity
# from models.model import Pocket, Shop
# from red.config import config
from helpers.helperFactory import HelperFactory
# from helpers.helperFactory import HelperFactory
# from excep.excep import ConfigException
from config.strings import *
from excep.excep import SerialNotFoundException,PocketAlreadyRegistered,InvalidSerialException


class Register(CashierActivity, ScanPocketMixin):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.transactor = HelperFactory.getTransactor(self.session)
        self.prefilled = False;
        self.initialize()
        self.returnActivity = "main"

        # self.authenticator = HelperFactory.getAuthenticator(self.session)

    def initialize(self):
        self.setLayout("cashier/registerScan")
        if self.prefilled:
            self.invokeLayoutFunction("setPrefilledOn", "")
        else:
            self.invokeLayoutFunction("setPrefilledOff", "")
        self.m_onCreate()      


    def onPocket(self, snr):
        try:
            pin = self.authenticator.registerPocket(snr)
            decsnr = self.authenticator.snrToDecsnr(snr)
            if self.prefilled:
                self.transactor.insertCoins(snr, 3000)
            self.setSuccessLayout(message="Pin: "+ str(pin) + "\nCardno.: " + str(decsnr) + "\n" + PRESS_SCREEN_TO_CONTINUE)

        except PocketAlreadyRegistered:
            self.setErrorLayout(message=ALREADY_REGISTERED, nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)

        except InvalidSerialException:
            self.setErrorLayout(message=INVALID_POCKET, nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)

    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     
    def _receiveDisplayMessage(self, message):  
      
        if message["data"] == "continue_s":
            self.initialize()
        elif message["data"] == "empty":
            self.prefilled = False
        elif message["data"] == "prefilled":
            self.prefilled = True

        self.m_receiveDisplayMessage(message)
    

    def onCancel(self):
        self.switchActivity(self.returnActivity)

      
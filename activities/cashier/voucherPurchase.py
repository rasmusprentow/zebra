#voucherPurchase.py

from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from helpers.helperFactory import HelperFactory
from helpers.authenticator import Authenticator
from excep.excep import InsufficientVoucherException, SerialNotFoundException, NoAmountException
from red.config import config, get_config
from config.strings import *


class Voucherpurchase(CashierActivity):
    """docstring for voucherPurchase"""
    
    def onCreate(self, data=None):
        self.setLayout("cashier/waitForCustomer")
        self.returnActivity = "main"
        if data == None or 'amount' not in data:
            raise NoAmountException('No amount received')

        self.amount = int(data['amount'])
        self.send('customer', {'head' : 'voucher_purchase', 'data' : {'amount' : self.amount}})

    def _receiveCustomerMessage(self, message):
        if message['head'] == 'validate_pocket':
            self._validatePocket(message['data']['snr'])
        elif message['head'] == 'pin_received':
            self.onReceivedPin(message['data'])    
        elif message['head'] == 'button_clicked':
            if "data" in message and message["data"] == "cancel":
                self.onCustomerCancel()
    

    def conductPurchase(self, snr):
        try:
            HelperFactory.getTransactor(self.session).voucherPurchase(snr, int(self.amount))
        except Exception as e:
            self.errorHandler(e)
            return

        balance_vamount = self.getBalanceVoucher(snr)
        self.send('customer',{'head':'voucherReceipt', 'data' : {'balance_vamount' : balance_vamount  ,'amount' : self.amount}})
        self.setSuccessLayout(message=VOUCHER_PURCHASE + "\nDiscount Balance: " + str(balance_vamount) + "\nCardno.: " + str(Authenticator().snrToDecsnr(snr)))
            
   
    def onCustomerCancel(self):
        self.setErrorLayout(message="Customer Canceled", time=self.errorNotificationSleepTime)
        self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome"}})
        self.switchActivity("main")


    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == "continue_s":
                self.switchActivity(self.returnActivity)  
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome"}})
   
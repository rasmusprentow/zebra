
from red.activity import Activity
from config.strings import * 
from red.config import config, get_config
from excep.excep import InsufficientCoinException, SerialNotFoundException, InsufficientVoucherException,OverLimitCoinException
from helpers.helperFactory import HelperFactory
from helpers.helperFactory import HelperFactory
import traceback
from red.utils.run_once import run_once

from models.model import Pocket, Shop
from helpers.transactor import Transactor

class CashierActivity(Activity):
    
    def __init__(self, kernel):
        super(CashierActivity, self).__init__(kernel)
        self.notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        self.errorNotificationSleepTime = get_config(config, 'Timeouts', 'error_notification', ctype=int)
        

    def errorHandler(self, exception):
        self.logger.critical('The following exception occurred: ' +   traceback.format_exc())
        msg = GENERAL_ERROR
        if isinstance(exception, InsufficientCoinException):
            msg = INSUFFICIENT_COIN
        elif isinstance(exception, InsufficientVoucherException):
            msg = INSUFFICIENT_VOUCHER
        elif isinstance(exception, SerialNotFoundException):
            msg = SERIAL_NOT_FOUND
        elif isinstance(exception, OverLimitCoinException):
            msg = OVERLIMIT_COIN
        self.send('customer',{'head':'error', 'data':msg})
        self.setErrorLayout(message=msg, nextActivity='main', time=self.errorNotificationSleepTime)


    def receiveCustomerMessage(self, message):
        if message['head'] == 'finished':
            self.switchActivity("main")
        else:
            if hasattr(self, "_receiveCustomerMessage"):
                self._receiveCustomerMessage(message)

    def _validatePocket(self, snr):
        if HelperFactory.getAuthenticator(self.session).isValidSerial(snr):
            self.send("customer", {'head' : 'valid_pocket', 'data' : {'snr' : snr}})
        else:
            self.send("customer", {'head' : 'error', 'data' : INVALID_POCKET})
            self.setErrorLayout(message=INVALID_POCKET, nextActivity='main', time=self.errorNotificationSleepTime)

   
    def onReceivedPin(self, data):
        snr = data['snr']
        if HelperFactory.getAuthenticator(self.session).validatePin(data['snr'], data['pin']):
        
            try:
                self.conductPurchase(snr)
            except Exception as e:
                self.errorHandler(e)

        else:
            self.logger.warning('Invalid pin ' + str(data['pin']) + ' for SNR: ' + str(snr))
            self.send('customer', {'head':'pin_error','data' : INVALID_PIN})
            self.setErrorLayout(message=INVALID_PIN, nextLayout='cashier/waitForCustomer', time=self.errorNotificationSleepTime)
 
    def getBalanceVoucher(self, snr):
        
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        if pocket == None:
            self.send('customer', {'head' : 'error', 'data' : INVALID_POCKET})
            self.setErrorLayout(message=INVALID_POCKET)
            return
        
        shopName = config.get('Shop', 'name')
        shop = self.session.query(Shop).filter_by(name=shopName).first()
        if shop == None:
            raise ConfigException("Shop was not in customer")
        
        voucher = Transactor.getShopVoucher(pocket, shop)
        if voucher == None:
            vamount = 0
        else:
            vamount = voucher.amount


        return vamount
        
  
    def receiveDisplayMessage(self, message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'cancel':
                if hasattr(self, "returnActivity"):
                    self.switchActivity(self.returnActivity)
                else:
                    self.switchActivity('management')
 
        self._receiveDisplayMessage(message)
#status.py



from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket, Shop
from red.config import config
from helpers.helperFactory import HelperFactory
from helpers.transactor import Transactor
from excep.excep import ConfigException
from config.strings import *


class Status(CashierActivity):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.setLayout("cashier/status")
        self.transactor = HelperFactory.getTransactor(self.session)
        self.status = None
        self.updateStatusToday()

    def getStatus(self):
        if self.status == None:
            self.status = self.transactor.getStatus()
        return self.status

    def updateStatusToday(self):
        status = self.getStatus()
        self.invokeLayoutFunction("updateTopbars", "Today")
        self.invokeLayoutFunction("updateButtonText", "All Time")
        self.invokeLayoutFunction("updateButtonCallId", "total")
        self.invokeLayoutFunction("updateCoinin",     status["incoins_today"]  )
        self.invokeLayoutFunction("updateCoinout",    status["outcoins_today"]  )
        self.invokeLayoutFunction("updateVoucherin",  status["invouchers_today"] )
        self.invokeLayoutFunction("updateVoucherout", status["outvouchers_today"])
        self.updateTotals(status)


    def updateStatusAllTime(self):
        status = self.getStatus()
        self.invokeLayoutFunction("updateTopbars", "All Time")
        self.invokeLayoutFunction("updateButtonText", "Today")
        self.invokeLayoutFunction("updateButtonCallId", "today")
        self.invokeLayoutFunction("updateCoinin",     status["incoins"]   )
        self.invokeLayoutFunction("updateCoinout",    status["outcoins"]    )
        self.invokeLayoutFunction("updateVoucherin",  status["invouchers"] )
        self.invokeLayoutFunction("updateVoucherout", status["outvouchers"])

        self.updateTotals(status)


    def updateTotals(self, status):
        self.invokeLayoutFunction("updateTotalVoucher", status["total_vouchers"])
        self.invokeLayoutFunction("updateTotalCoin",    status["total_coins"])
        self.invokeLayoutFunction("updateTotalFees",    status["total_fees"])
        if status["voucher_status"] == True:
            self.invokeLayoutFunction("updateStatus",    "ok")
       
    


    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'continue':
                self.switchActivity('management')
            if message['data'] == 'cancel':
                self.switchActivity('management')
                
            elif message['data'] == 'total':
                self.updateStatusAllTime()
            elif message['data'] == 'today':
                self.updateStatusToday()
        
   
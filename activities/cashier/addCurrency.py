#addCurrency.py

from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from excep.excep import InsufficientCoinException, SerialNotFoundException
from red.config import config, get_config
from config.strings import *
from activities.scanPocketMixin import ScanPocketMixin
from helpers.authenticator import Authenticator

class Addcurrency(CashierActivity, ScanPocketMixin):
    """docstring for Insertcoins"""
    
    def onCreate(self, data=None):
        self.setLayout("cashier/scan")
        self.invokeLayoutFunction("updateHeader", "Scan to authenticate")
        
        self.m_onCreate()

        if data == None:
            raise ValueError('No data received')

        if 'amount' not in data:
            raise ValueError('No amount in the data received')

        if 'currency' not in data:
            raise ValueError('No currency in the data received')
  
        try: 
            self.amount = int(data['amount'])
        except TypeError:
            raise ValueError("The amount could not be parsed to an int ")
        
        self.currency = data['currency'] 

    def onPocket(self, snr):
        if not HelperFactory.getAuthenticator(self.session).validateCashierPocket(snr):
            self.setErrorLayout(message=INVALID_POCKET, nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)
            return 
        self.masterSnr = snr
        self.setLayout("cashier/waitForCustomer")

        if self.currency == "discount":
            self.send('customer', {'head' : 'add_discount', 'data' : self.amount})
        else:
            self.send('customer', {'head' : 'insert_coins', 'data' : self.amount})

            
    def _receiveCustomerMessage(self, message):
        if message['head'] == 'pocket_received':
            try:
                snr = message["data"]
                if self.currency == "discount":
                    HelperFactory.getTransactor(self.session).voucherPurchase(snr, - int(self.amount), self.masterSnr)
                    self.setSuccessLayout(message=INSERT_DISCOUNT+ "\nDiscount: " + str(self.amount) + "\nDiscount Balance: " + str(self.getBalanceVoucher(snr))+"\nCardno.: " + str(Authenticator().snrToDecsnr(snr)) ) #, nextActivity=self.returnActivity, time=self.notificationSleepTime)
                else:
                    HelperFactory.getTransactor(self.session).insertCoins(snr,int(self.amount), self.masterSnr, obeyLimit=True)
                    pocket = self.session.query(Pocket).filter_by(snr=snr).first()
                    self.setSuccessLayout(message=INSERT_COINS + "\nE-Coins: " + str(self.amount) + "\nE-Coin Balance: " + str(pocket.safe.amount) + "\nCardno.: " + str(Authenticator().snrToDecsnr(snr))) #, nextActivity='main', time=self.notificationSleepTime)
                
               
                self.send('customer',{'head':'success', 'data':''})
                return;
            except Exception as e:
                self.errorHandler(e)
        elif message['head'] == 'button_clicked':
            if "data" in message and message["data"] == "cancel":
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome", 'time' : self.errorNotificationSleepTime}})
                self.setErrorLayout(message=CUSTOMER_CANCEL, nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)
                   
    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == "continue_s":
                self.switchActivity(self.returnActivity)  
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome"}})
                

        self.m_receiveDisplayMessage(message)    
     
    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
      
    def onCancel(self):
        self.switchActivity(self.returnActivity)



#reregister.py


from activities.scanPocketMixin import ScanPocketMixin
from activities.cashier.cashierActivity import CashierActivity
# from models.model import Pocket, Shop
# from red.config import config
from helpers.helperFactory import HelperFactory
# from helpers.helperFactory import HelperFactory
# from excep.excep import ConfigException
from config.strings import *
from excep.excep import SerialNotFoundException,PocketAlreadyRegistered,InvalidSerialException

class Reregister(CashierActivity, ScanPocketMixin):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.setLayout("cashier/entersnr")
        # self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.decsnr = ""

    def initialize(self):
        self.m_onCreate()      

    def onPocket(self, newsnr):
        try:
            if len(self.decsnr) < 10:
                raise InvalidSerialException("Too short")
            
            oldsnr = self.authenticator.decsnrToSnr(self.decsnr)
            self.authenticator.reRegisterPocket(oldsnr, newsnr)
            decsnr = self.authenticator.snrToDecsnr(newsnr)
            self.setSuccessLayout(message="Cardno.: " + str(decsnr) + "\n"+ PRESS_SCREEN_TO_CONTINUE)
        except PocketAlreadyRegistered:
            self.setErrorLayout(message=NEW_POCKET_EXISTED, nextActivity='management', time=self.errorNotificationSleepTime)
                
        except SerialNotFoundException:
            self.setErrorLayout(message=OLD_DID_NOT_EXIST, nextActivity='management', time=self.errorNotificationSleepTime)
        
        except InvalidSerialException:
            self.setErrorLayout(message=INVALID_POCKET, nextActivity='management', time=self.errorNotificationSleepTime)  

    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     
    def _receiveDisplayMessage(self, message):
        if message['head'] == 'button_clicked':
            if 'num' in message['data']:
                if len(self.decsnr) < 10:
                    self.decsnr += message['data'][3]
                    self.invokeLayoutFunction("updateSnrText", self.decsnr)
            elif message['data'] == 'backspace':
                self.decsnr = self.decsnr[:-1]
                self.invokeLayoutFunction("updateSnrText", self.decsnr)
            elif message['data'] == 'accept':
                if len(self.decsnr) == 10:
                    snr = self.authenticator.decsnrToSnr(self.decsnr)
                    if self.authenticator.isValidSerial(snr):
                        self.setLayout("cashier/scan")
                        self.invokeLayoutFunction("updateHeader", "Scan to re-register")
                        self.initialize()
                    else:
                        self.setErrorLayout(message=OLD_DID_NOT_EXIST, nextLayout="cashier/entersnr", time=self.errorNotificationSleepTime)
                        self.decsnr = ""
   
            elif message['data'] == 'cancel':
                self.switchActivity('management')
            elif message['data'] == 'continue_s':   # From the notification
                self.switchActivity('management')

        self.m_receiveDisplayMessage(message)
    

    def onCancel(self):
        self.switchActivity("management")
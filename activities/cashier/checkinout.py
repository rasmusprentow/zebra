


 
from activities.cashier.cashierActivity import CashierActivity
from helpers.staffmanagement import StaffManagement
from excep.excep import SerialNotFoundException,PocketAlreadyRegistered,InvalidSerialException
from activities.scanPocketMixin import ScanPocketMixin
from config.strings import *

class Checkinout(CashierActivity,ScanPocketMixin):
    
    def onCreate(self, data=None):
        self.setLayout("cashier/checkinout")
        self.direction = ""
        self.manager = StaffManagement(self.session)
        self.m_onCreate()
        self.returnActivity = "management"
 
    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
     

    def _receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == "in":
                self.direction = "in"
                self.setLayout("cashier/scan")
                self.invokeLayoutFunction("updateHeader", "Scan to check In")
            elif message['data'] == 'out':
                self.direction = "out"
                self.setLayout("cashier/scan")
                self.invokeLayoutFunction("updateHeader", "Scan to check Out")
            elif message["data"] == 'continue_s':
                self.switchActivity(self.returnActivity)
            elif message["data"] == 'continue_e':
                self.switchActivity(self.returnActivity)

        self.m_receiveDisplayMessage(message)  

    def onCancel(self):
        self.switchActivity(self.returnActivity)


    def onPocket(self, snr):
        try:
            if self.direction == "in":
                self.manager.checkIn(snr)
                self.setSuccessLayout(message="You have checked in", nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)
            else: 
                self.manager.checkOut(snr) 
                self.setSuccessLayout(message="You have checked out", nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)
      



        except SerialNotFoundException:
            self.setErrorLayout(message=INVALID_POCKET, nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)
      
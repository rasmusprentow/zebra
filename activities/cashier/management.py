#management.py


from activities.cashier.cashierActivity import CashierActivity
# from models.model import Pocket, Shop
# from red.config import config
# from helpers.helperFactory import HelperFactory
# from helpers.helperFactory import HelperFactory
# from excep.excep import ConfigException
# from config.strings import *


class Management(CashierActivity):
    """docstring for Main"""
    
    def onCreate(self, data=None):
        self.setLayout("cashier/management")
        # self.authenticator = HelperFactory.getAuthenticator(self.session)

    def receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'register':
                self.switchActivity('register')
            elif message['data'] == 'back':
                self.switchActivity('main')
            elif message['data'] == 'cancel':
                self.switchActivity('main')
            elif message['data'] == 'reregister':
                self.switchActivity('reregister')
            elif message['data'] == 'status':
                self.switchActivity('status')
            elif message['data'] == 'check':
                self.switchActivity('check')
            elif message['data'] == 'checkpocket':
                self.switchActivity('check')
            elif message['data'] == 'system':
                self.switchActivity('system')
            elif message['data'] == 'checkinout':
                self.switchActivity('checkinout') 
            elif message['data'] == 'insertdiscount':
                self.switchActivity('enterAmount', {'next_activity' : 'addDiscount'})
          
         
    
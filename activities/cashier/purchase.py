#product.py
"""docstring for Purchase package"""

from activities.cashier.cashierActivity import CashierActivity
from helpers.helperFactory import HelperFactory
from helpers.helperFactory import HelperFactory
from models.model import Pocket, Shop
from red.config import config, get_config
from config.strings import *
from helpers.transactor import Transactor
from helpers.authenticator import Authenticator

class Purchase(CashierActivity):
    """docstring for Purchase"""
    
    def onCreate(self, data=None):
        self.setLayout("standAlone/selectproduct")
        self.product = ''
        self.amount = None

    def _receiveDisplayMessage(self, message):
        """docstring for receiveDisplayMessage"""
        if message['head'] == 'button_clicked':
            if 'product' in message['data']:
                self.product = message['data']
                self.amount = int(config.get('Products', self.product))
                self.send('customer', {'head' : 'purchase', 'data' : {'product' : self.product, 'amount' : self.amount}})
                self.setLayout('cashier/waitForCustomer')
            elif message["data"] == 'cancel':
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome"}})
                self.switchActivity('main')
            elif message["data"] == 'continue_s':
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome"}})
                self.switchActivity('main')
            else:
                self.logger.warning('Unknown product')
        
    def _receiveCustomerMessage(self, message):
        """docstring for receiveCustomerMessage"""
        if message['head'] == 'validate_pocket':
            self._validatePocket(message['data']['snr'])
        if message['head'] == 'pin_received':
            self.onReceivedPin(message['data'])    
        elif message['head'] == 'button_clicked':
            if "data" in message and message["data"] == "cancel":
                self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome", 'time' : self.errorNotificationSleepTime}})
                self.setErrorLayout(message=CUSTOMER_CANCEL, nextActivity='main', time=self.errorNotificationSleepTime)
                

    def conductPurchase(self, snr):             
        transaction = HelperFactory.getTransactor(self.session).purchase(snr, self.amount)
        balance_vamount = self.getBalanceVoucher(snr)
        self.send('customer', {'head' : 'purchaseReceipt', 'data' : {'balance_vamount' : balance_vamount  , 'snr': snr, 'product' : self.product, 'amount' : transaction.amount, 'vamount' : transaction.voucherAmount}})
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        self.setSuccessLayout(message=PURCHASE  + "\nDiscount Balance: " + str(balance_vamount) + "\nE-Coin Balance: " + str(pocket.safe.amount) + "\nCardno.: " + str(Authenticator().snrToDecsnr(snr)))
          


   
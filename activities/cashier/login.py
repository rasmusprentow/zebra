
from activities.cashier.cashierActivity import CashierActivity
from models.model import Pocket, Shop
from red.config import config
from helpers.helperFactory import HelperFactory
from activities.scanPocketMixin import ScanPocketMixin

from excep.excep import ConfigException
from config.strings import *
import datetime, threading

class Login(CashierActivity, ScanPocketMixin):
    """docstring for Login"""
    
    def onCreate(self, data=None):
        self.layout = "cashier/login"
        self.setLayout(self.layout)
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.m_onCreate()
        
    def onPocket(self, snr):
        if self.authenticator.validateCashierPocket(snr):
            self.switchActivity("main")
        else:
            self.setErrorLayout(message="Not a Master Pocket", nextActivity="login", time=self.errorNotificationSleepTime)
            

    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
#insertCoins.py


from activities.cashier.addCurrency import Addcurrency

class Insertcoins(Addcurrency):
    """docstring for Insertcoins"""
    
    def onCreate(self, data=None):
        self.returnActivity = "main"
        if data == None:
            raise ValueError('No data received')

        if 'amount' not in data:
            raise ValueError('No amount in the data received')
        super(Insertcoins, self).onCreate({"amount":data['amount'], "currency":"ecoins"})



#         #insertCoins.py

# from activities.cashier.cashierActivity import CashierActivity
# from models.model import Pocket
# from helpers.helperFactory import HelperFactory
# from excep.excep import InsufficientCoinException, SerialNotFoundException
# from red.config import config, get_config
# from config.strings import *
# from activities.scanPocketMixin import ScanPocketMixin

# class Insertcoins(CashierActivity, ScanPocketMixin):
#     """docstring for Insertcoins"""
    
#     def onCreate(self, data=None):
#         self.setLayout("cashier/scan")
#         self.invokeLayoutFunction("updateHeader", "Scan to authenticate")
#         self.returnActivity = "main"
#         self.m_onCreate()

#         if data == None:
#             raise ValueError('No data received')

#         if 'amount' not in data:
#             raise ValueError('No amount in the data received')

#         try: 
#             self.amount = int(data['amount'])
#         except TypeError:
#             raise ValueError("The amount could not be parsed to an int ")
            

#     def onPocket(self, snr):
#         if not HelperFactory.getAuthenticator(self.session).validateCashierPocket(snr):
#             self.setErrorLayout(message=INVALID_POCKET, nextActivity=self.returnActivity, time=self.errorNotificationSleepTime)
#             return 
#         self.masterSnr = snr
#         self.setLayout("cashier/waitForCustomer")

#         self.send('customer', {'head' : 'insert_coins', 'data' : self.amount})

            
#     def _receiveCustomerMessage(self, message):
#         if message['head'] == 'pocket_received':
#             try:
#                 snr = message["data"]
#                 HelperFactory.getTransactor(self.session).insertCoins(snr,int(self.amount), self.masterSnr, obeyLimit=True)
#                 self.send('customer',{'head':'success', 'data':''})
#                 pocket = self.session.query(Pocket).filter_by(snr=snr).first()
#                 self.setSuccessLayout(message=INSERT_COINS + "\nE-Coins: " + str(self.amount) + "\nE-Coin Balance: " + str(pocket.safe.amount), nextActivity='main', time=self.notificationSleepTime)
#                 return;
#             except Exception as e:
#                 self.errorHandler(e)
#         elif message['head'] == 'button_clicked':
#             if "data" in message and message["data"] == "cancel":
#                 self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome", 'time' : self.errorNotificationSleepTime}})
#                 self.setErrorLayout(message=CUSTOMER_CANCEL, nextActivity='main', time=self.errorNotificationSleepTime)
                   
#     def _receiveDisplayMessage(self,message):
#         if message['head'] == 'button_clicked':
#             if message['data'] == "continue_s":
#                 self.switchActivity('main')  
#                 self.send("customer", {"head" : "switch_activity", "data" : {'activity' : "welcome"}})
                

#         self.m_receiveDisplayMessage(message)    
     
#     def receiveLpcMessage(self, message):
#         self.m_receiveLpcMessage(message)
      
#     def onCancel(self):
#         self.switchActivity(self.returnActivity)

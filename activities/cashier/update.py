

 
from activities.cashier.cashierActivity import CashierActivity
import subprocess

class Update(CashierActivity):
    


    def onCreate(self, data=None):
        self.setLayout("cashier/confirm")
        self.invokeLayoutFunction("updateHeader", "Update?")
           # self.invokeLayoutFunction("updateSubInfoText", "Pulling data")
       # self.invokeLayoutFunction("updateSubInfoText", "Pulling data")
        # self.finished = False
        # self.runCommand("git pull") 
        # self.runCommand("git submodule update") 
        # self.invokeLayoutFunction("updateSubInfoText", "Updating Customer Side")
        # self.send("customer", {"head":"update"})
        # # self.runCommand("echo shutdown -r now") 


    def runCommand(self, cmd, message=""):
        self.invokeLayoutFunction("updateSubInfoText", cmd)
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        self.logger.debug(process.communicate()[0])
        returncode = process.wait()
        if returncode != 0:
            raise Exception("Failed")
        
    def _receiveDisplayMessage(self, message):
        if message['head'] == "button_clicked":
            if message['data'] == "yes":
                self.setLoadingScreen(message="Updating")
                try:
                    self.runCommand("git pull") 
                    self.runCommand("git submodule update") 
                    self.invokeLayoutFunction("updateSubInfoText", "Updating Customer Side")
                    self.send("customer", {"head":"update"})
                except:
                    self.displayError()
            else:
                self.switchActivity("management")

    def receiveCustomerMessage(self, message):
        if message['head'] == "progress":
            self.invokeLayoutFunction("updateSubInfoText", "Customer: " + message['data'])
        elif message['head'] == "finished_updating":
            self.kernel.stop()
            self.kernel.app.quit()
            self.runCommand("shutdown -r now") 
        elif message['head'] == "error":
            self.displayError()     

    def displayError(self):
        self.setErrorLayout(message="Failed to update", nextActivity='management', time=self.errorNotificationSleepTime)
 
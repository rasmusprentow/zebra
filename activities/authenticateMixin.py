from config.strings import *
from red.config import config, get_config
#welcomeMixin.py
class AuthenticateMixin(object):
    """docstring for Welcome"""
    
    def m_onCreate(self, data=None):
       # self.setLayout("common/authenticate") # Don't change latout as it caused delay when no Pin NOPIN
        self.errorNotificationSleepTime = get_config(config, 'Timeouts', 'error_notification', ctype=int)
 
        self.notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        self.snr = data['snr']
        self.pin = ''    
        self.pin_secret = ''

    def m_receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if 'num' in message['data']:
                if len(self.pin) < 4:
                    self.pin += message['data'][3]
                    self.pin_secret += '*'
                    self.invokeLayoutFunction("updatePinText", self.pin_secret)
                
            elif message['data'] == 'accept':
                if len(self.pin) == 4:
                    self.onAccept()
                else:
                    self.m_wrongPinError(PIN_TOO_SHORT)
                 
            elif message['data'] == 'cancel':
                if self.pin != '':
                    self.clearPin()
                else:
                    self.onCancel()
            #elif message['data'] == 'continue':
            #    if self.timer != None:
            #        self.timer.cancel()
            #    self.setLayout("authenticate")
          
    

    def clearPin(self):
        self.pin_secret = ''
        self.pin = ''
        self.invokeLayoutFunction("updatePinText", self.pin_secret)

    def onAccept(self):
        self.logger.critical('No onAccept defined.')


    def onCancel(self):
        self.switchActivity("welcome")

    def m_wrongPinError(self, msg=INVALID_PIN):
        self.clearPin()
        self.setErrorLayout(message=msg, nextLayout='common/authenticate',time=self.errorNotificationSleepTime)

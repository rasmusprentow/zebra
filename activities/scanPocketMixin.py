""" #scanpocketMixin.py """
from red.config import config, get_config

class ScanPocketMixin(object):
    """ Yeah """

    def m_onCreate(self):
        """ Call during on create """
        self.clearLpc()
        self.send("lpc", {"head":"get_tag"})
  

    def m_receiveLpcMessage(self, message):
        """ Call onPocket when a pocket is received """
        if message['head'] == 'tag' and 'data' in message:
            snr = message['data']
            self.send("lpc", {"head":"activate_buzzer"})
            self.onPocket(snr)
            # snr_len = get_config(config,'Pocket','snr_len',ctype=int)

            # if len(message['data']) == snr_len:
                # self.send("lpc", {"head":"activate_buzzer"})
                # self.onPocket(snr)
            # else:
                # self.logger.info("Received invalid tag, trying again")
                # self.send("lpc", {"head":"get_tag"})   
    
    def m_receiveDisplayMessage(self, message):
        if message['head'] == 'button_clicked':
            if "data" in message and message["data"] == "cancel":
                self.onCancel()
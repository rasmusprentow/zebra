#payment.py

from activities.standAlone.standAloneActivity import StandAloneActivity
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from excep.excep import InsufficientCoinException
from red.config import config, get_config
from threading import Timer
import logging, logging.config


class Selectproduct(StandAloneActivity):
    """docstring for Selectproduct"""
    
    def onCreate(self, data=None):
        self.setLayout('standAlone/selectproduct')
        self.logger = logging.getLogger('activity.selectProduct')
        self.setTimedActivity("welcome", time=self.timeout)

    def receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'cancel':
                self.switchActivity("welcome")
            else:
                amount = int(config.get('Products', message['data']))
                self.switchActivity('scanPocket', {'product' : message['data'], 'amount' : amount})
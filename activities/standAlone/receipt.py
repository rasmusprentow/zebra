#payment.py

from activities.standAlone.standAloneActivity import StandAloneActivity
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from excep.excep import SerialNotFoundException, InsufficientCoinException
from red.config import config, get_config
import logging, logging.config
from config.strings import *

class Receipt(StandAloneActivity):
    """docstring for Payment"""
    
    def onCreate(self, data=None):
        self.setLoadingScreen('Processing payment')
        self.logger = logging.getLogger('activity.payment')
        
        if data == None:
            raise ValueError('No data received.')

        snr_len = 8
         #get_config(config,'Pocket','snr_len',ctype=int)

        if 'snr' in data and len(data['snr']) == snr_len:
            self.snr = data['snr']
        else:
            raise ValueError('Not a snr received.')

        if 'product' in data:
            self.product = data['product']
        else: 
            raise ValueError('No product received.')

        self.amount = int(config.get('Products',self.product))
        
        self.pay(self.snr, self.amount, self.product)

    def receiveDisplayMessage(self,message):
        if message['head'] == 'button_clicked':
            if message['data'] == 'cancel':
                self.switchActivity("welcome")

    def pay(self, snr, amount, product):
        try:
            transaction = HelperFactory.getTransactor(self.session).purchase(snr, amount)
            self.setLayout('common/purchaseReceipt')
            self.invokeLayoutFunction("updateAmount", str(amount))
            self.invokeLayoutFunction("updateVoucherAmount", transaction.voucherAmount)
            self.invokeLayoutFunction("updateProduct", prepro(str(product)))
            self.setTimedActivity('welcome', time=self.notificationSleepTime)
        except Exception as e:
            self.errorHandler(e)

  
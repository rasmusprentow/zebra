#welcome.py

from activities.standAlone.standAloneActivity import StandAloneActivity
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from excep.excep import SerialNotFoundException


class Welcome(StandAloneActivity):
    """docstring for Welcome"""

    def onCreate(self, data=None):
        self.setLayout("standAlone/welcome")

    def receiveDisplayMessage(self, message):
         if message['head'] == 'button_clicked':
            if message['data'] == 'screen':
                self.switchActivity('selectProduct')
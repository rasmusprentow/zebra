
from red.activity import Activity
from config.strings import * 
from red.config import config, get_config
from excep.excep import InsufficientCoinException, SerialNotFoundException
from helpers.helperFactory import HelperFactory
from helpers.helperFactory import HelperFactory
from config.strings import *
import traceback
class StandAloneActivity(Activity):
    
    def __init__(self, kernel):
        super(StandAloneActivity, self).__init__(kernel)
        self.notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        self.errorNotificationSleepTime = get_config(config, 'Timeouts', 'error_notification', ctype=int)
        self.timeout = get_config(config, 'Timeouts', 'timeout', ctype=int)
        

    def errorHandler(self, exception):
        msg = GENERAL_ERROR
        if isinstance(exception, InsufficientCoinException):
            msg = INSUFFICIENT_COIN
        elif isinstance(exception, SerialNotFoundException):
            msg = SERIAL_NOT_FOUND
        else:
            self.logger.critical("Unkown error occored: " + traceback.format_exc())
        self.setErrorLayout(message=msg, nextActivity='welcome', time=self.errorNotificationSleepTime)


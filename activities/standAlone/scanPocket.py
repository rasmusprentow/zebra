""" Scanpocket """

from activities.standAlone.standAloneActivity import StandAloneActivity
from threading import Timer
from activities.scanPocketMixin import ScanPocketMixin

from config.strings import *
class Scanpocket(StandAloneActivity, ScanPocketMixin):
    """docstring for Selectproduct"""
    
    def onCreate(self, data=None):
        self.setLayout('common/purchaseScan')
        self.invokeLayoutFunction("updateAmount", str(data['amount']))
        product = data['product']
       
        self.invokeLayoutFunction("updateProduct", prepro(product))
        self.product = data['product']
        self.amount = data['amount']
        self.m_onCreate()

    def receiveLpcMessage(self, message):
        self.m_receiveLpcMessage(message)
      
    def receiveDisplayMessage(self, message):
        self.m_receiveDisplayMessage(message)
        
    def onCancel(self):
        self.switchActivity('selectProduct')

    def onPocket(self, snr):
        self.switchActivity('authenticate', {'snr' : snr, 'product' : self.product, 'amount' : self.amount})
from activities.standAlone.standAloneActivity import StandAloneActivity
from red.config import config, get_config
from models.model import Pocket
from helpers.helperFactory import HelperFactory
from activities.authenticateMixin import AuthenticateMixin
from excep.excep import SerialNotFoundException
from config.strings import *

class Authenticate(StandAloneActivity, AuthenticateMixin):
    """docstring for Welcome"""

    def onCreate(self, data=None):
        self.m_onCreate(data)
        self.setLoadingScreen(VALIDATING_POCKET)
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.notificationSleepTime = get_config(config, 'Timeouts', 'notification', ctype=int)
        self.product = data['product']
  
        try: 

            if not self.authenticator.isValidSerial(data['snr']):
                self.setErrorLayout(message=SERIAL_NOT_FOUND, nextActivity='welcome', time=self.errorNotificationSleepTime)
            else: 
                self.setLayout("common/authenticate")
        except Exception as e: 

            self.logger.fatal("Received Exception: " +  str(e))
            
            self.setErrorLayout(message="Connection Unavailable", nextActivity='welcome', time=self.errorNotificationSleepTime)

    def receiveDisplayMessage(self, message):
        self.m_receiveDisplayMessage(message)

    def onAccept(self):
        try:
            if self.authenticator.validatePin(self.snr, self.pin):
                self.switchActivity("receipt", {'snr' : self.snr, 'product' : self.product})
            else:
                self.m_wrongPinError()    
        except Exception as e:
            self.errorHandler(e)

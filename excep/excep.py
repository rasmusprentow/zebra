#exceptions.py



###################################################################################
class InsufficientCoinException(Exception):
    """Thrown when a safe have insufficient coins"""
   
    def __init__(self, msg):
        super(InsufficientCoinException, self).__init__(msg)
###################################################################################

###################################################################################
class InsufficientVoucherException(Exception):
    """hrown when a pocket have insufficient vouchers"""
   
    def __init__(self, msg):
        super(InsufficientVoucherException, self).__init__(msg)
###################################################################################

###################################################################################
class SerialNotFoundException(Exception):
    """Thrown when a serial do not lead to a pocket"""
   
    def __init__(self, msg):
        super(SerialNotFoundException, self).__init__(msg)
###################################################################################

###################################################################################
class InvalidSerialException(Exception):
    """Thrown when a serial is not 8 chars"""
   
    def __init__(self, msg):
        super(InvalidSerialException, self).__init__(msg)
###################################################################################

###################################################################################
class NoAmountException(Exception):
    """Thrown when there is no amount """
   
    def __init__(self, msg):
        super(NoAmountException, self).__init__(msg)
###################################################################################

###################################################################################
class ConfigException(Exception):
    """Thrown when a serial is not 8 chars"""
   
    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
###################################################################################

###################################################################################
class MalformedDataException(Exception):
    """Thrown when some data is malformed"""
   
    def __init__(self, msg):
        super(MalformedDataException, self).__init__(msg)
###################################################################################


class PocketAlreadyRegistered(Exception):
    """Thrown when some data is malformed"""
   
    def __init__(self, msg):
        super(PocketAlreadyRegistered, self).__init__(msg)

class TimedOutException(Exception):
    """Thrown when some data is malformed"""
   
    def __init__(self, msg):
        super(TimedOutException, self).__init__(msg)



###################################################################################
class OverLimitCoinException(Exception):
    """Thrown when someone tryes to insert too much"""
   
    def __init__(self, msg):
        super(OverLimitCoinException, self).__init__(msg)
###################################################################################

#synchronize.py



 
from red.services.base import Service
from threading import Thread

import json
from red.config import config
import requests
import time
from helpers.synchronizer import Synchronizer
import datetime
import traceback
from red.utils.run_once import run_once
   
from models.model import engine, Transaction
from sqlalchemy.orm import sessionmaker

class Synchronize(Service, Thread):

    def onCreate(self):
        self.session = sessionmaker(bind=engine)()
        self.serverurl = config.get('Server', 'url')
        self.location = config.get('Shop', 'location')
        self.shop = config.get('Shop', 'name')
        self.synchronizer = Synchronizer(self.session)
        self.running = True

    @run_once
    def startHwClock(self):
        bashCommand = "systemctl start rtc-ds1307.service"
        import subprocess
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.logger.info(process.communicate()[1])
        #self.logger.info(process.communicate()[1])
 
    def run(self):
        self.logger.info("Sync is starting")
        time.sleep(20)
        try: 
            pass
            self.startHwClock()
        except:
            self.logger.debug("Failed to start Hardware clock got: " + traceback.format_exc())
        self.logger.info("Sync is started")
        
        while self.running:
            try:
                self.syncTransactions()
            except:         
                self.logger.warning("Exception caught in service " + traceback.format_exc())


            time.sleep(60)
        if self.running != True:
            self.logger.info("Sync is stopping")

    # def processMessage(self, msg)
    def _push(self, data):
        data = json.dumps(data)
        self.logger.info("Pushing to " + self.serverurl)
        try:
            headers = {'content-type': 'application/json'}
            response = requests.post(self.serverurl + '/push_transaction', data=data, headers=headers)
            return str(response.status_code) == '200'
        except:
            self.logger.info("Failed pushing, received:  " + traceback.format_exc())
            return False

    def _get(self):
        headers = {'content-type': 'application/json'}
        payload = {'location': self.location, 'shop' : self.shop}
        response = requests.get(self.serverurl + '/get_transactions', params=payload, headers=headers)

        return response.text 
        
    def _processResponse(self, response):
        transactions = response['data']
        for transaction in transactions:
            self.synchronizer.processTransaction(transaction)

    def syncTransactions(self):
        time = datetime.datetime.utcnow()
        transactions = self.session.query(Transaction).filter_by(synced='').all()
        self.logger.info("Transactions included: " + str(transactions))
        for t in transactions:
            data =  {'head' : 'transaction', 'data' : self.synchronizer._convertTransaction(t)}
            response = self._push(data)
            if response == True: 
                t.synced = datetime.datetime.utcnow()
                self.session.commit()
            else:
                #Do some errorhandling aka. log the shiat
                self.logger.warning("Something went wrong. Response was: " + str(response))
                pass

        gets = self._get()
        try :
            data = json.loads(gets)
        except: 
            self.logger.error(gets)

            return

        self._processResponse(data)

        payload = {'shop': self.shop, 'location': self.location, 'time' : time}
        requests.get(self.serverurl + '/set_last_synced', params=payload)
       



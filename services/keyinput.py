#input.py


from red.services.base import Service
import zmq

import logging
from threading import Thread
logger = logging.getLogger(__file__)

class Keyinput(Service,Thread):
    # Overrides run, so doesn't wait for messages
    def run(self):
        try:
            while True:
                rin = raw_input("in:")
                for string in list(rin):
                    self.socket.send_json({"head":"key_pressed","data":string})
                    if string == 's':
                        logger.info("Shutting down KeyInputHandler")
                        #self.sock.close();
                        return
                    
        except zmq.error.ContextTerminated:
            return
 

from red.services.base import Service
from red.config import config, get_config

from threading import Thread
import Adafruit_BBIO.GPIO as GPIO

class Audiovisual(Service, Thread):
    """ For communication between Terminals """

    def onCreate(self):
      
        self.running = True
        self.redPin = "P9_23"
        self.greenPin = "P9_25"
        GPIO.setup(self.redPin, GPIO.OUT)
        GPIO.setup(self.greenPin, GPIO.OUT)

    def negative_feedback(self):
        GPIO.output(self.redPin, GPIO.HIGH)
        time.sleep(5)
        GPIO.cleanup()

    def positive_feedback(self):
        GPIO.output(self.greenPin, GPIO.HIGH)
        time.sleep(5)
        GPIO.cleanup()

    def processMessage(self, message):
        if message["head"] == "negative_feedback":
            t = threading.Thread(target=self.negative_feedback)
            t.daemon = True
            t.start() 
            return True
        elif message["head"] == "positive_feedback":
            t = threading.Thread(target=self.positive_feedback)
            t.daemon = True
            t.start() 
            return True
        else:
            return False

  

    # def processMessage(self, message):
    #     """ Receives ZMQ messages, we happily accept everything """

    #     return True



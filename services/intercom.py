""" Inter-Terminal Communication """
#intercom.py

from red.services.base import Service
from red.config import config, get_config
from red.drivers.uart import Uart
import json, time

from models.model import engine
from sqlalchemy.orm import sessionmaker
from threading import Thread
from helpers.helperFactory import HelperFactory

class Intercom(Service, Thread):
    """ For communication between Terminals """

    def onCreate(self):
        self.session = sessionmaker(bind=engine)()
        self.uart = Uart()
        self.uart.startFromConfig(config)
        self.running = True
    
    def run(self):
        while self.running:
            try:
                self.onJsonMessage(self.uart.receiveJson())
                time.sleep(0.4)
            except:
                pass

    
  

    # def processMessage(self, message):
    #     """ Receives ZMQ messages, we happily accept everything """

    #     return True



    def onJsonMessage(self, message):
        self.logger.info("Received: " + str(message))
        response = self.generateResponse(message)
        self.uart.sendJson(response)
    
    def generateResponse(self, message):
        snr = message['data']['snr']
        
        try: 
            if message['head'] == 'validatePin':
                pin = message['data']['pin']
                response = HelperFactory.getAuthenticator(self.session).validatePin(snr, pin)
            elif message['head'] == 'isValidSerial':
                response = HelperFactory.getAuthenticator(self.session).isValidSerial(snr)
            elif message['head'] == 'purchase':
                transaction = HelperFactory.getTransactor(self.session).purchase(snr, message['data']['amount'])
                response = {'voucherAmount' : transaction.voucherAmount, 'amount' : transaction.amount}
    
            return {"head" : 'ok', 'data' : response}
        except Exception as e:
            return {'head' : 'error', 'data' : {'exception' : e.__class__.__name__, 'message' : e.message}}

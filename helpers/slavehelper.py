
import excep.excep
import inspect

def reportErrors(message):
    if message['head'] == 'ok':
        return True
    error = message['data']['exception']
    for name, obj in inspect.getmembers(excep.excep):
        if inspect.isclass(obj):
            if error == obj.__name__:
                raise obj(message['data']['message'])
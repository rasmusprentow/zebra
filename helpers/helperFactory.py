
from helpers.master_authenticator import MasterAuthenticator
from helpers.slave_authenticator import SlaveAuthenticator
from helpers.master_transactor import MasterTransactor
from helpers.slave_transactor import SlaveTransactor


class HelperFactory(object):
    @staticmethod
    def getTransactor(session=None):
        if session == None:
            return SlaveTransactor()
        else: 
            return MasterTransactor(session)


    @staticmethod
    def getAuthenticator(session=None):
        if session == None:
            return SlaveAuthenticator()
        else: 
            return MasterAuthenticator(session)
    
#transaction.py
from models.model import Transaction, Voucher, Shop, Pocket, Safe, Location
from red.config import config, get_config
from excep.excep import InsufficientVoucherException,OverLimitCoinException, InsufficientCoinException, SerialNotFoundException, ConfigException
import math, datetime

from helpers.transactor import Transactor

from sqlalchemy.sql import func



class MasterTransactor(Transactor):
    """Helps with tranactions"""

    def __init__(self, session):
        self.session = session
        self.shopName = config.get( 'Shop', 'name'    )
        self.locationName  = config.get( 'Shop', 'location')
        self.coin_limit = get_config(config,'Pocket', 'coin_limit', ctype=int, default=20000)
        self.discount_limit = get_config(config,'Pocket', 'discount_limit', ctype=int, default=100)
       
        self.allowNegativeShopBalance = True
        if self._getShop() == None:
            raise ConfigException("The shop specified did not exist in the database. The shop was: " + str(self.shopName))
        
        if self._getLocation() == None:
            raise ConfigException("The location specified did not exist in the database. The location was: " + str(self.locationName))
      
        self.feeDecimal = float(config.get('Shop', 'fee_percentage')) / 100        


    def _getShop(self):
        return self.session.query(Shop).filter_by(name=self.shopName).first()

    def _getLocation(self):
        return self.session.query(Location).filter_by(name=self.locationName).first()
       
        


    def insertCoins(self, snr, amount,note="", obeyLimit=False):
        Transactor.assertAmount(amount)
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        
        if not isinstance(pocket, Pocket):
            raise SerialNotFoundException('Serial not found in model')

        if obeyLimit and amount + pocket.safe.amount > int(self.coin_limit):

            raise OverLimitCoinException("Amount was to high")
        if not self.allowNegativeShopBalance:
            if not Transactor.checkCoins(self._getShop().safe, amount) : 
                raise InsufficientCoinException("Insufficient coins in shop safe")

        t = Transaction(self._getShop().safe, pocket.safe, amount, 0, 0, self._getLocation(), note=note)

        self.deductPurchase(self._getShop().safe, pocket.safe, amount, 0, True)
        
        self.session.add(t)
        self.session.commit()

    # def insertCoinsFromPES(self, amount):
    #     Transactor.assertAmount(amount)
     
    #     pes = self.session.query(Pocket).filter_by(name='PES').first()
     
        
    #     t = Transaction(pes.safe, self._getShop().safe, amount, 0, 0)

    #     self.deductPurchase(pes.safe,  self._getShop().safe, amount, 0)
        
    #     self.session.add(t)
    #     self.session.commit()


    def purchase(self, snr, amount, note=""):
        Transactor.assertAmount(amount)
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        if not isinstance(pocket, Pocket):
            raise SerialNotFoundException('Serial not found in model')
        
        if not Transactor.checkCoins(pocket.safe, amount):
            raise InsufficientCoinException("Insufficient coins in pocket safe")

        voucherAmount = Transactor.getVoucherAmount(amount)
        voucherAmount = self.correctVoucherAmount(pocket, voucherAmount)
        fee = math.ceil(amount * self.feeDecimal)
        t = Transaction(pocket.safe, self._getShop().safe, amount, voucherAmount, fee, self._getLocation(),note=note)
        
        
       
        MasterTransactor.useVoucher(self.session, pocket, self._getShop(), voucherAmount)

        self.deductPurchase(pocket.safe, self._getShop().safe, amount, fee)
        
        self.session.add(t)
        self.session.commit()
        return t

    def cashPurchase(self, amount):
        
        Transactor.assertAmount(amount)
        self.insertCoins("cash_buy", amount)
        
        pocket = self.session.query(Pocket).filter_by(snr="cash_buy").first()
        if not isinstance(pocket, Pocket):
            raise SerialNotFoundException('Serial not found in model')
        
       
        voucherAmount =0 
        fee = 0
        t = Transaction(pocket.safe, self._getShop().safe, amount, voucherAmount, fee, self._getLocation())
        
        self.deductPurchase(pocket.safe, self._getShop().safe, amount, fee)
        
        self.session.add(t)
        self.session.commit()
        return t
           
    def registerTransaction(self, snr):
        pass
        self.purchase(snr, 0)


    def reRegisterTransaction(self, oldSnr, newSnr):
        self.purchase(newSnr, 0, note="reregister:" + oldSnr)

    def voucherPurchase(self, snr, amount, note=""):
        
        Transactor.assertAmount(amount, True)
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        if not isinstance(pocket, Pocket):
            raise SerialNotFoundException('Serial not found in model')

        corrected_amount = - self.correctVoucherAmount(pocket, - amount) ## Quite Confusing i suppose
     
        t = Transaction(pocket.safe, self._getShop().safe, 0, - corrected_amount, 0, self._getLocation(), note=note)
        Transactor.useVoucher(self.session, pocket, self._getShop(), - corrected_amount)
        
        self.session.add(t)
        self.session.commit()
        
    def correctVoucherAmount(self, pocket, amount):
        shopVoucher = Transactor.getShopVoucher(pocket,self._getShop())
        if shopVoucher == None:
            shopVoucher = 0
        else:
            shopVoucher = shopVoucher.amount
        total_amount  = shopVoucher + amount


        corrected_amount = amount
        if total_amount >= self.discount_limit:
            corrected_amount = self.discount_limit - shopVoucher
        return corrected_amount

    def getStatus(self):
        status  = {}
        today = datetime.datetime.combine((datetime.datetime.utcnow().date()), datetime.time(0,0,0))
        status["outcoins_today"] = self.session.query(func.sum(Transaction.amount).label("amount")).filter_by(srcId=self._getShop().safe.id).filter(Transaction.timestamp > today).first().amount
        status["incoins_today"]  = self.session.query(func.sum(Transaction.amount).label("amount")).filter_by(destId=self._getShop().safe.id).filter(Transaction.timestamp > today).first().amount
        
        invouchers_today_temp =  self.session.query(func.sum(Transaction.voucherAmount).label("vouchers")).filter_by(destId=self._getShop().safe.id).filter(Transaction.timestamp > today).filter(Transaction.voucherAmount<0).first().vouchers 
        if invouchers_today_temp == None:
            status["invouchers_today"]  =  0 
        else: 
           status["invouchers_today"] = - invouchers_today_temp

        status["outvouchers_today"] =   self.session.query(func.sum(Transaction.voucherAmount).label("vouchers")).filter_by(destId=self._getShop().safe.id).filter(Transaction.timestamp > today).filter(Transaction.voucherAmount>0).first().vouchers
       
        status["outcoins"]    = self.session.query(func.sum(Transaction.amount).label("amount")).filter_by(srcId=self._getShop().safe.id).first().amount
        status["incoins"]     = self.session.query(func.sum(Transaction.amount).label("amount")).filter_by(destId=self._getShop().safe.id).first().amount
        
        invouchers_temp = self.session.query(func.sum(Transaction.voucherAmount).label("vouchers")).filter_by(destId=self._getShop().safe.id).filter(Transaction.voucherAmount<0).first().vouchers
        
        if invouchers_temp == None:
            status["invouchers"]  =  0 
        else: 
           status["invouchers"] = - invouchers_temp

        status["outvouchers"] =   self.session.query(func.sum(Transaction.voucherAmount).label("vouchers")).filter_by(destId=self._getShop().safe.id).filter(Transaction.voucherAmount>0).first().vouchers
       
        status["total_vouchers"] = self.session.query(func.sum(Voucher.amount).label("vouchers")).filter_by(sid=self._getShop().id).first().vouchers
        status["total_coins"] = self.session.query(func.sum(Safe.amount).label("amount")).first().amount
        status["total_fees"] = self.session.query(func.sum(Transaction.fee).label("fee")).first().fee
       

        for key in status:
            if status[key] == None:
                status[key] = 0 
        status["voucher_status"] = status["total_vouchers"]  ==  (status["outvouchers"]  - status["invouchers"]) # Total is what we given minus what people have used
        

        return status



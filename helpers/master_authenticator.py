#authenticator.py


#transaction.py
from models.model import Pocket, Safe, PocketType, ShopType, Shop
from red.config import config

from excep.excep import InvalidSerialException, SerialNotFoundException,PocketAlreadyRegistered
from random import SystemRandom
from werkzeug.security import generate_password_hash, check_password_hash

PIN_CHARS = '123456789'
PIN_LENGTH = 4

_sys_rng = SystemRandom()

from helpers.authenticator import Authenticator

from helpers.master_transactor import MasterTransactor



class MasterAuthenticator(Authenticator):
    """Helps with tranactions"""

    def __init__(self, session):
        self.session = session
        self.hashedpins = False



    def validatePin(self, snr, pin):
        """ 
        Validates a pin against the database 
        Returns: True if valid and false if not
        Throws exceptions if snr is invalid
        """
        
        return True # Disable pin for good NOPIN

        if len(snr) != 8:
            raise InvalidSerialException("The snr was invalid. It was: " + str(snr))

        pin = str(pin)
        if len(pin) != PIN_LENGTH:
            return False

        pocket = self._getPocket(snr)
        print(pin + " Poicket pin: " + pocket.pin)
  
        if self.hashedpins:
            return check_password_hash(str(pocket.pin), str(pin))
        else:
            return str(pocket.pin) == str(pin)

    def _getPocket(self, snr):

        pocket = self.session.query(Pocket).filter_by(snr=str(snr)).first()

        if not isinstance(pocket, Pocket):
            raise SerialNotFoundException("The snr '" + str(snr) + "' did not lead to a valid pocket")

        return pocket

    def isValidSerial(self, snr):
        try: 
            pocket = self._getPocket(snr)
            return isinstance(pocket, Pocket)
        except:
            return False

  
    def registerPocket(self, snr, pin=None, registerPocket=True):
        """ 
        Creates a pocket using the snr. 
        Returns the pin code of the pocket in clear text
        """
        if len(snr) != 8:
            raise InvalidSerialException("The snr was invalid")

        try: 
            pocket = self._getPocket(snr)
            if pocket != None:
                raise PocketAlreadyRegistered("Pocket already existed, with: " + snr)
        except SerialNotFoundException:
            pass

            
        if pin == None:
            pin = MasterAuthenticator.generateRandomPin()
        
        if self.hashedpins:
            hashed_pin = MasterAuthenticator.hashPin(pin)
        else:
            hashed_pin = pin

        safe = Safe(0)
        self.session.add(safe)
        self.session.commit()
        
        pocketType = self.session.query(PocketType).filter_by(name='customer').first()
        if not isinstance(pocketType, PocketType):
            pocketType = PocketType('customer')
            self.session.add(pocketType)
            self.session.commit()
        pocket = Pocket(safe=safe, snr=snr, pin=hashed_pin, ptype=pocketType)

        self.session.add(pocket)
        if registerPocket: 
            MasterTransactor(self.session).registerTransaction(snr)
        self.session.commit()

        return pin

    def registerShop(self, shopName):
        """ 
        Creates a pocket using the snr. 
        Returns the pin code of the pocket in clear text
        """
  
        safe = Safe(0)
        self.session.add(safe)
        self.session.commit()
        shopType = self.session.query(ShopType).filter_by(name='vendor').first()
        if not isinstance(shopType, ShopType):
            shopType = ShopType('vendor')
            self.session.add(shopType)
            self.session.commit()
        shop = Shop(safe=safe, name=shopName, stype=shopType)

        self.session.add(shop)
        self.session.commit()

        return shop

    def getPin(self, snr):
        if self.hashedpins:
            for pin in ['{0:04}'.format(num) for num in xrange(0, 10000)]:
                if check_password_hash(str(self._getPocket(snr).pin), str(pin)):
                    return pin
            
        else: 
            return self._getPocket(snr).pin


    def reRegisterPocket(self, oldSnr, newSnr, notify=True):
        """ 
        Removes a pocket using the snr. 
        """
        if notify:
            if not self.isValidSerial(oldSnr):
                raise SerialNotFoundException("Old pocket was not found " + str(oldSnr))
            if self.isValidSerial(newSnr):
                raise PocketAlreadyRegistered("New pocket already existed, " + str(newSnr))
            if self.validateCashierPocket(oldSnr):
                raise InvalidSerialException("Old pocket was cashier") 
        
        try:
            self._getPocket(newSnr)
        except:
            pocket = self._getPocket(oldSnr)
            pocket.snr = newSnr
            self.session.commit()
    
            if notify:
                MasterTransactor(self.session).reRegisterTransaction(oldSnr, newSnr)
      

    

    def validateCashierPocket(self, snr):
        try: 
            pocket = self._getPocket(snr)
            return pocket.pocketType.name == "cashier"
        except SerialNotFoundException:
            return False
       
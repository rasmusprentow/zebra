#synchronizer.py


from helpers.transactor import Transactor
from helpers.authenticator import Authenticator
from helpers.master_authenticator import MasterAuthenticator

from excep.excep import *
import datetime
from models.model import Pocket, Safe, Shop, Transaction, Location
import logging 

logger = logging.getLogger("Server.Synchronizor")
#EXAMPLE_DATA = {
#     'head' : 'transaction',
#     'data' : {
#         'timestamp': '2014-04-17 09:16:12.378366',
#         'srcOwn' : {
#             'type' : 'pocket',
#             'serial' : '12345678'},
#         'destOwn' : {
#             'type' : 'shop',
#             'name' : 'enviclean'},
#         'amount' : 300,
#         'voucherAmount' : 100,
#         'fee' : 3,
#         'location' : 'onestop1',
#         }
#     }


class Synchronizer(object):
    """ 
    Makes Docs
    """

    def __init__(self, session):
        self.session = session
        self.authenticator = MasterAuthenticator(self.session)

    def processTransaction(self, data):
        if "note" in data:
            note = data['note']
        else:
            note = ""
        amount = data['amount']
        fee = data['fee']
        voucherAmount = data['voucherAmount']
        timestamp = data['timestamp']
        Transactor.assertAmount(amount)
        if "reregister" in note:
            print data
            if data['destOwn']['type'] == "pocket":
                snr = data['destOwn']['id']
            else: 
                snr = data['srcOwn']['id']
            self.authenticator.reRegisterPocket(note.split(":")[1],snr , notify=False)
            
        location = self._getLocation(data['location'], data['shop'])
        toInstance = self._getOwner(data['destOwn'])
        fromInstance = self._getOwner(data['srcOwn'])
      
        


        
    
        old = self.session.query(Transaction).filter_by(timestamp=timestamp, destId=toInstance.safe.id).first()
        ### Should make veyr much mor elaborate check
        if old != None:
            logger.info("The same transaction was placed twice. With time: " + timestamp)
            return True

        # if not Transactor.checkCoins(fromInstance.safe, amount):
        #     raise InsufficientCoinException("Insufficient coins in pocket safe")

        t = Transaction(fromInstance.safe, toInstance.safe, amount, voucherAmount, fee, location, synced=datetime.datetime.utcnow(), timestamp=timestamp, note=note)
        Transactor.deductPurchase(fromInstance.safe, toInstance.safe, amount, fee, True)
        if voucherAmount != 0:
            if not isinstance(fromInstance, Pocket) or not isinstance(toInstance, Shop):
                logger.critical("voucherAmount was but the from was not a pocket or to was not a shop")

            else: 
                Transactor.useVoucher(self.session, pocket=fromInstance, shop=toInstance, amount=voucherAmount, obeyLimit=False)
        
        self.session.add(t)
        self.session.commit()
        return True

 
    def _getOwner(self, data):

        if data['type'] == 'pocket':
            pocket = self.session.query(Pocket).filter_by(snr=data['id']).first()
            if not isinstance(pocket, Pocket):
                self.authenticator.registerPocket(data['id'], data['pin'],registerPocket=False)
                pocket = self.session.query(Pocket).filter_by(snr=data['id']).first()
            
            return pocket

        elif data['type'] == 'shop':
            shop = self.session.query(Shop).filter_by(name=data['id']).first()
            if not isinstance(shop, Shop):
                self.authenticator.registerShop(data['id'])
                shop = self.session.query(Shop).filter_by(name=data['id']).first()
           
            return shop

        else:
            raise MalformedDataException("Type was neither pocket nor shop")



    def getData(self, timestamp):
        """ Takes a timestamp and returns the data which was synced earlier """
        transactions = self.session.query(Transaction).filter(Transaction.synced >= timestamp).all()


        result =  {'head' : 'list_of_transactions', 'data' : map(self._convertTransaction, transactions)}
        return result


    def _convertTransaction(self, transaction):

        srcOwner, srcType, pinSrc = Transactor.getSafeOwner(self.session, transaction.src)
        destOwner, destType, pinDest = Transactor.getSafeOwner(self.session, transaction.dest)

         
        return {
           'timestamp': transaction.timestamp,
           'srcOwn' : {
               'type' : (srcType),
               'id' : (srcOwner),
               'pin' : pinSrc},
           'destOwn' : {
               'type' : (destType),
               'id' : (destOwner),
               'pin' : pinDest},
           'amount' : (transaction.amount),
           'voucherAmount' : (transaction.voucherAmount),
           'fee' : transaction.fee,
           'location' : transaction.location.name,
           'shop' : transaction.location.shop.name,
           'note' : transaction.note
         }

    def getLastSynced(self, locationName, shopName):
        """ Returns the timestamp for last sync for the specified location """
        l = self._getLocation(locationName, shopName)
        logger.info("Last sync for Location: " + str(locationName) + " was " + str(l))
        return l.last_synced

    def setLastSynced(self, session, locationName, time, shopName):
        l = self._getLocation(locationName, shopName)
        l.last_synced = time
        session.commit()

    def _getShop(self, shopName):
        shop =  self.session.query(Shop).filter_by(name=shopName).first()
        if not isinstance (shop, Shop):
            shop = self.authenticator.registerShop(shopName)
            
        return shop

    def _getLocation(self, locationName, shopName):
        location =  self.session.query(Location).filter_by(name=locationName).first()
        if not isinstance (location, Location):
            shop = self._getShop(shopName)
            location = Location(locationName, shop)
            self.session.add(location)
            self.session.commit()
        return location


# EXAMPLE_DATA = {
#     'head' : 'transaction',
#     'data' : {
#         'timestamp': '2014-04-17 09:16:12.378366',
#         'srcOwn' : {
#             'type' : 'pocket',
#             'serial' : '12345678'},
#         'destOwn' : {
#             'type' : 'shop',
#             'name' : 'enviclean'},
#         'amount' : 300,
#         'voucherAmount' : 100,
#         'fee' : 3,
#         'location' : 'onestop1',
#         'shop' : 'enviclean'
#         }
#     }

# T1 = "some transaction"
# T2 = "some transaction"

# RETURN_DATA = {'head' : 'list_of_transactions', 'data' : [ T1, T2]}


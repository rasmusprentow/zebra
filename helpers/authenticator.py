#authenticator.py


#transaction.py
from models.model import Pocket, Safe, PocketType
from red.config import config

from excep.excep import InvalidSerialException, SerialNotFoundException
from random import SystemRandom
from werkzeug.security import generate_password_hash, check_password_hash

PIN_CHARS = '123456789'
PIN_LENGTH = 4

_sys_rng = SystemRandom()


class Authenticator(object):
    """Helps with tranactions"""

    def decsnrToSnr(self, decsnr):
        self.hexlength = 8
        return format(int(decsnr), 'x').zfill(self.hexlength)

    def snrToDecsnr(self, snr):
        self.declength = 10
        return str(int(snr, 16)).zfill(self.declength)

    def validatePin(self, snr, pin):
        pass

    def isValidSerial(self, snr):
        pass

    def registerPocket(self, snr):
        pass

    @staticmethod
    def generateRandomPin():
        """ Generates a 4 digit pin. """
        return ''.join(_sys_rng.choice(PIN_CHARS) for _ in range(PIN_LENGTH))
      
    @staticmethod
    def hashPin(pin):
        """ Hashes a pin code """
        return generate_password_hash(pin)
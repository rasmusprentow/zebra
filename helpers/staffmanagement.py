
#transaction.py
from models.model import Transaction, Shop, Pocket, Safe, Location
from red.config import config
from excep.excep import InsufficientVoucherException, InsufficientCoinException, SerialNotFoundException, ConfigException


from helpers.transactor import Transactor
from helpers.master_authenticator import MasterAuthenticator


class StaffManagement(object):


    def __init__(self, session):
        self.session = session
        self.shopName = config.get( 'Shop', 'name'    )
        self.locationName  = config.get( 'Shop', 'location')
        

    def _getShop(self):
        return self.session.query(Shop).filter_by(name=self.shopName).first()

    def _getLocation(self):
        return self.session.query(Location).filter_by(name=self.locationName).first()
       
        

    def checkIn(self, snr):
        self.checkInOut(snr, "checkin")
    
    def checkOut(self, snr):
        self.checkInOut(snr, "checkout")
    
    def checkInOut(self, snr, message):
        if not MasterAuthenticator(self.session).validateCashierPocket(snr):
            raise SerialNotFoundException("The pocket was not a cashier pocket")
        pocket = self.session.query(Pocket).filter_by(snr=snr).first()
        
        if not isinstance(pocket, Pocket):
            raise SerialNotFoundException('Serial not found in model')
        
        t = Transaction(pocket.safe, self._getShop().safe, 0, 0, 0, self._getLocation(), note=message)
        
        self.session.add(t)
        self.session.commit()
        return t

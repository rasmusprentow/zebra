#transaction.py
from models.model import Transaction, Voucher, Shop, Pocket, Safe
from red.config import config
from excep.excep import InsufficientVoucherException, InsufficientCoinException, SerialNotFoundException, ConfigException
import math



class Transactor(object):
    """Helps with tranactions"""

   

    def insertCoins(self, snr, amount):
        pass 
    
    def purchase(self, snr, amount):
        pass

    def voucherPurchase(self, snr, amount):
        pass

    def getShopVoucher(self, pocket):
        pass

    def checkCoins(self, safe, amount):
        pass 

    @staticmethod
    def assertAmount(amount, allowNegative=False):
        if not isinstance(amount, int):
            raise Exception('Programming error, amount can ONLY be int')
        if (not allowNegative) and(amount < 0):
            raise Exception('Programming error, amount cant be negative')



    @staticmethod
    def deductPurchase(fromSafe, toSafe, amount, fee, allowNegativeBalance=False):
        
        if not allowNegativeBalance:
            if fromSafe.amount < amount:
                 raise InsufficientCoinException("Insufficient Coins in safe: (id) " + str(fromSafe.id)) 
        fromSafe.amount -= amount
        toSafe.amount += amount - fee

 
    @staticmethod
    def getVoucherAmount(amount):
       voucherDecimal = float(config.get('Shop', 'voucher_percentage')) / 100
       return int(math.floor(amount * voucherDecimal))

    @staticmethod
    def checkCoins(safe, amount):
        if isinstance(safe, Safe):
            if safe.amount >= amount:
                return True
            else:
                return False    

    @staticmethod
    def useVoucher(session, pocket, shop, amount, obeyLimit=True):
        voucher = Transactor.getShopVoucher(pocket, shop)
        if not isinstance(voucher, Voucher):
            voucher = Voucher(pocket, shop, 0)
            session.add(voucher)
     
        if obeyLimit and amount < 0  and voucher.amount < math.fabs(amount):
            raise InsufficientVoucherException('Insufficient vouchers')

        voucher.amount += amount
        

    @staticmethod
    def getShopVoucher(pocket, shop):
        voucherList = [x for x in pocket.vouchers if lambda x: x.shopId == shop.id]
        if len(voucherList) == 1:
            return voucherList.pop()
        elif len(voucherList) > 1:
            raise Exception('More than one voucher entry found for customer for this shop with snr: ' + pocket.snr)
        else:
            return None

    @staticmethod
    def getSafeOwner(session, safe):
        shop = session.query(Shop).filter_by(safeId=safe.id).first()       
        if isinstance(shop, Shop):
            return shop.name, 'shop', None

        pocket = session.query(Pocket).filter_by(safeId=safe.id).first()   
        if isinstance(pocket, Pocket):
            return pocket.snr, 'pocket', pocket.pin

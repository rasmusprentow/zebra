#transaction.py
from red.config import config
from excep.excep import InsufficientVoucherException, InsufficientCoinException, SerialNotFoundException, ConfigException
import math
from helpers.slavehelper import reportErrors
from helpers.transactor import Transactor

import logging, traceback
logger = logging.getLogger("Helpers.SlaveTransactor")
try:
    from red.drivers.uart import Uart
except ImportError:
    logger.debug("Uart could not be loaded. " + traceback.format_exc() )


class Transaction(object):
    def __init__(self, amount, voucherAmount):
        self.amount = amount
        self.voucherAmount = voucherAmount


class SlaveTransactor(Transactor):
    """Helps with tranactions"""

    def __init__(self):
        logger.info("Slave transaction")
        self.uart = Uart()
        self.uart.startFromConfig(config)

    def insertCoins(self, snr, amount):
        pass #TODO implement@never

    def purchase(self, snr, amount):
        self.uart.sendJson({'head' : 'purchase', 'data' : {'snr' : snr, 'amount' : amount}})
        response = self.uart.receiveJson()
        reportErrors(response)
        transaction = Transaction(response['data']['amount'],response['data']['voucherAmount'])
        return transaction

    def voucherPurchase(self, snr, amount):     
        pass #TODO implement@never

    def assertAmount(self, amount):
        if not isinstance(amount, int):
            raise Exception('Programming error, amount can ONLY be int')
        if (amount < 0):
            raise Exception('Programming error, amount cant be negative')

    
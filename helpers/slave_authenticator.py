#authenticator.py


#transaction.py
from models.model import Pocket, Safe, PocketType
from red.config import config

from excep.excep import InvalidSerialException, SerialNotFoundException
from random import SystemRandom
from werkzeug.security import generate_password_hash, check_password_hash

import logging, traceback
logger = logging.getLogger("Helpers.SlaveTransactor")
try:
    from red.drivers.uart import Uart
except ImportError:
    logger.debug("Uart could not be loaded. " + traceback.format_exc() )

from helpers.slavehelper import reportErrors


from helpers.authenticator import Authenticator


class SlaveAuthenticator(Authenticator):
    """Helps with tranactions"""

    def __init__(self):
        self.uart = Uart()
        self.uart.startFromConfig(config)
        
    def validatePin(self, snr, pin):
        self.uart.sendJson({'head' : 'validatePin', 'data' : {'snr' : snr, 'pin' : pin}})
        response = self.uart.receiveJson()
        reportErrors(response)
        return response['data']
   
    def isValidSerial(self, snr):
        self.uart.sendJson({'head' : 'isValidSerial', 'data' : {'snr' : snr}})
        response = self.uart.receiveJson()
        reportErrors(response)
        return response['data']

    def registerPocket(self, snr):
        pass
        #TODO: implement@never


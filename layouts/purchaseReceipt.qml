import QtQuick 1.0

import "core" as Core

Core.Base {
    id: page
  
    Core.Topbar {
        id: topbar
        text: qsTr("Thank you")
    }

    Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin 
    }
    
    function updateAmount(string ) {
        paidText.text = string
    }

    function updateProduct(string ) {
        productText.text = string
    }

    function updateVoucherAmount(string){
        voucherText.text = string
    }

    Core.LargeText {
        
        x: page.margin
        y: 100
        text: qsTr("Paid: ")
    }

    Core.LargeText {
        x: page.margin
        y: 130
        text: qsTr("voucher(s): ")
    }

    Core.LargeText {
        x: page.margin
        y: 160
        text: qsTr("Product: ")
    }


    Core.LargeText {
        id: paidText
        x: 210
        y: 100
        text: qsTr("300")
    }

    Core.LargeText {
        id: voucherText
        x: 210
        y: 130
        text: qsTr("")
    }

    Core.LargeText {
        id: productText
        x: 210
        y: 160
        text: qsTr("")
    }

}

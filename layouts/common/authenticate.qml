import QtQuick 1.0

import "../core" as Core

Core.Page {
    id: page

    
    property int rowheight: 60
    property int columnwidth: 115
    property int rowoffset: 85
    property int coloffset: page.width / 2 - columnwidth * 1.5

    property int row1: rowoffset + page.margin
    property int row2: rowoffset + rowheight + page.margin
    property int row3: rowoffset + rowheight * 2 + page.margin
    property int row4: rowoffset + rowheight * 3 + page.margin

    property int col1: coloffset + page.margin
    property int col2: coloffset + columnwidth + page.margin
    property int col3: coloffset + columnwidth * 2 + page.margin
    property int col4: coloffset + columnwidth * 3 + page.margin

    property int upperrowy: page.margin + topbar.offset

    function updatePinText(string) {
        pinText.text = string
    }

    Core.Topbar {
        id: topbar
       
    }

    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }

    Core.MediumText {
        id: enterpintext
        text: qsTr("Enter Pin:")
        y: upperrowy + 4
        x: cancel.x + cancel.width + page.margin * 8
    }

    Core.LargeText {
        id: pinText
        x: pinindicator.x + 15
        y: pinindicator.y  + 3
        text: ""
    }

    Image {
       id: pinindicator
       source: "../img/PinShow.png"
        width: accept.x - pinindicator.x - page.margin * 8
        height: enterpintext.height
        
        y: upperrowy + 4
        x: enterpintext.x + enterpintext.width + page.margin
    }

    Core.Accept {
        id: accept
        x: page.width - accept.width - page.margin
        y: page.margin
    } 
    


    Core.Num {
        id: num1
        callId: "num1"
        btnText: "1"
        x: col1  
        y: row1   
    }
    Core.Num {
        id: num2
        callId: "num2"
        btnText: "2"
        x: col2  
        y: row1   
    }
    Core.Num {
        id: num3
        callId: "num3"
        btnText: "3"
        x: col3
        y: row1   
    }
    Core.Num {
        id: num4
        callId: "num4"
        btnText: "4"
        x: col1
        y: row2   
    }
    Core.Num {
        id: num5
        callId: "num5"
        btnText: "5"
        x: col2
        y: row2 
    }
    Core.Num {
        id: num6
        callId: "num6"
        btnText: "6"
        x: col3
        y: row2 
    }
    Core.Num {
        id: num7
        callId: "num7"
        btnText: "7"
        x: col1
        y: row3
    }
    Core.Num {
        id: num8
        callId: "num8"
        btnText: "8"
        x: col2
        y: row3
    }
    Core.Num {
        id: num9
        callId: "num9"
        btnText: "9"
        x: col3
        y: row3
    }
    //Core.Num {
    //    id: clear
    //    callId: "clear"
    //    btnText: "clear"
    //    x: col1
    //    y: row4
    //}
    //Core.Num {
    //    id: num0
    //    callId: "num0"
    //    btnText: "0"
    //    x: col2
    //    y: row4
    //}
    //Core.Num {
    //    id: accept
    //    callId: "accept"
    //    btnText: "accept"
    //    x: col3
    //    y: row4
    //}
}
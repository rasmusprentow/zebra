import QtQuick 1.0

import "../core" as Core
import "../js/stacks.js" as Stack

Core.Base {
    id: page
  
    property int textline1height: 95
    property int textline2height: 132
    property int textspace: 8

    Core.Topbar {
        id: topbar
        text: qsTr("Thank you")
    }

    Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin 
    }

    //Component.onCompleted: Stack.createDPStacks(show2.x +2 , show2.y + 2, 50, 6);

    Image {
        id: balanceBar
        source: "../img/BalanceBar.png"
        x: 0
        y: 73
        smooth: true
    }


    function updateAmount(string ) {
        amount.text = string
    }

    function updateProduct(string ) {
        product.text = "\"" + string + "\""
    }

    function updateVoucherAmount(string){
        voucherText.text = string
       
    }

    function updateTotalVoucherAmount(string){
        voucher.text = string
       
    }

    function updateStack(param1, param2){
        Stack.createDPStacks(show2.x +2 , show2.y + 2, param1, param2)
        
    }

    Core.MediumText {
        id: thankYouText1
        text: qsTr('You bought')
        x: page.getCenterX(thankYouText1) - 140
        y: textline1height
    }

    Core.MediumText {
        id: product
        x: thankYouText1.x + thankYouText1.width + textspace
        y: textline1height
         text: ""
    }

       
    Core.MediumText {
        id: thankYouText2
        text: qsTr('for')
        x: product.x + product.width + textspace
        y: textline1height
    }

    Core.MediumText {
        id: amount
        x: thankYouText2.x + thankYouText2.width + textspace
        y: textline1height - 11
        text: ""
        color: "#00aa04"
        font.pixelSize: 35

    }

    Core.MediumText {
        id: thankYouText3
        text: qsTr('E-Coins')

        x: amount.x + amount.width + textspace
        y: textline1height
    }

    Core.MediumText {
        id: thankYouText4
        text: qsTr('You get')
        x: page.getCenterX(thankYouText1) - 70
        y: textline2height
    }

    Core.MediumText {
            id: voucherText
            x: thankYouText4.x + thankYouText4.width + textspace
            y: textline2height - 11
            text: ""
            color: "#068a82"
            font.pixelSize: 35
    }

        Core.MediumText {
        id: thankYouText5
        text: qsTr('Discount Point(s)')
        x: voucherText.x + voucherText.width + textspace
        y: textline2height
    }

    Image {
        id: discount
        source: "../img/Discount.png"
        x: 33
        y: 194
        height: 64
        fillMode: Image.PreserveAspectFit
        smooth: true
    }

    Image {
        id: show2
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show2.width * 0.5
        y: 190
        width: 172
    }
                
    Image {
        id: show3
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show2.width * 0.5
        y: 230
        width: 172

        Core.MediumText {
            id: voucher
            text: qsTr("")
            horizontalAlignment: Text.AlignRight
            y:3
            width:parent.width - 5
        }
    }

    Core.MediumText {
        id: point
        text: qsTr("Points")
        x: page.width * 0.5 + show3.width * 0.5 +10
        y: show3.y + voucher.y
    }

}

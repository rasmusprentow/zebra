import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
    
    
    function updateInfoText(string ) {
        infoText.text = string
    }
    function updateSubInfoText(string ) {
        subinfoText.text = string
    }

    AnimatedImage {
        id: img
        source: "../img/Loading.gif"
        x: page.getCenterX(img)
        y: page.getCenterY(img) 
    }


    Core.LargeText {
            id: infoText
            // anchors.horizontalCenter: parent.horizontalCenter
            // anchors.verticalCenter: parent.verticalCenter
            x: page.getCenterX(infoText)
            y: page.getCenterY(infoText) - 35
            
            text: qsTr("Please Wait")
    }

    Core.SmallText {
            id: subinfoText
            // anchors.horizontalCenter: parent.horizontalCenter
            // anchors.verticalCenter: parent.verticalCenter
            x: page.getCenterX(subinfoText)
            y: 160
            text: qsTr("")
    }
    
 

}

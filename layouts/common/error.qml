import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

   //Core.Topbar {
   //  id: errorHeadText
   //  text: qsTr("Error")
   //
   //}


    AnimatedImage {
        id: img
        source: "../img/Loading.gif"
        x: page.getCenterX(img)
        y: page.getCenterY(img) + 35
    }


    Core.LargeText {
            id: errorText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            x: page.getCenterX(errorText)
            y: page.getCenterY(errorText)
            
            text: "An Error Occurred"
    }

    
    function updateErrorText(string ) {
        errorText.text = string
    }

  

    MouseArea {
            
        anchors.fill: parent

        onClicked: {
            context.onClicked("continue_e")
        }
    }
}

import QtQuick 1.0

import "../core" as Core

Core.ScanPage {
    id: page

    function updateProduct(str) {
        buyText.text = qsTr("Buy '" + str + "'")
    }

    function updateAmount(str) {
        amountText.text = " - " + str + " E-Coins"
    }



 
    Rectangle {
        id: topbarText
        x: page.getCenterX(topbarText)
        y: page.margin + page.offset
        width: buyText.width + amountText.width
        Core.LargeText {
            id: buyText
           
            x: 0
            text: qsTr("Buy")
        }

        Core.MediumText {
            id: amountText
            x: buyText.x + buyText.width
            y: buyText.y + buyText.height - amountText.height 
            text: qsTr("")
            color: buyText.color
        }
    }
    
   

          
}

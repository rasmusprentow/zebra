import QtQuick 1.0

import "core" as Core

Core.Base {
    id: page

    function updateMessageText(string ) {
        messageText.text = string
    }



    Image {
        id: topbar
        source: "../img/Topbar.png"
        
    }

    Core.LargeText {
        id: headerText
        x: page.getCenterX(headerText)
        y: page.margin + 12
        text: qsTr("Welcome")
    	font.pixelSize: 45
    }

    Core.SmallText {
        id: messageText
        x: page.getCenterX(messageText)
        y: page.margin + headerText.height + 10
        text: qsTr("Touch card")
    }
   
    Image {
        id: swipecard
        source: "../img/SwipeCard.png"
        //antialiasing: true
        x: page.getCenterX(swipecard) - 80
        y: 113
        width: 418*0.62
        height: 249*0.62
        smooth: true
    }

    AnimatedImage {
        id: img
        source: "../img/Arrows.gif"
        x: page.width - img.width - page.margin - 18
        y: page.getCenterY(img) + 50	
  	width: 210*0.65
        height: 98*0.65
        smooth: true
    }
}

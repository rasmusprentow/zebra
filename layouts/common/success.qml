import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    Core.Topbar {
        id: headline
        text: qsTr("Success")
    
    }
    function updateSuccessText(string ) {
        successText.text = string
    }
    Core.MediumText {
        id: successText
        x: page.getCenterX(successText)
        y: page.getCenterY(successText) + 20
    }

    MouseArea {
            
        anchors.fill: parent

        onClicked: {
            context.onClicked("continue_s")
        }
    }
}
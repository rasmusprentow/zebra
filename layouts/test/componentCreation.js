var component;
var sprite;

function createSpriteObjects() {
    for(var i=0; i<5; i++) {
       
        component = Qt.createComponent("Sprite.qml");
        sprite = component.createObject(appWindow, {"x": 50*i, "y": 100});

        if (sprite == null) {
            // Error Handling
            console.log("Error creating object");
        }
    }
}
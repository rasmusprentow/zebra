import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
  
 

    Core.Topbar {
        id: topbar
        text: qsTr("Choose product")
    }

    Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin
    }
    
    Core.Button {
        id: button1

        callId: "product1"
        x: page.getCenterX(button1) - 92
        y: page.getCenterY(button1) + 20
        
        sourceUp: "../img/ToiletU.png"
        sourceDown: "../img/ToiletD.png"
        width: 156
        height: 128
    }

    Core.Button {
        id: button2
        callId: "product2"
        x: page.getCenterX(button1) + 92
        y: page.getCenterY(button1) + 20
        sourceUp: "../img/BathU.png"
        sourceDown: "../img/BathD.png"  
        width: 156
        height: 128
    }

    Core.SmallText {
        id: button1MessageText
        x: button1.x + button1.width/2 - button1MessageText.width/2
        y: button1.y + button1.height + 10
        text: qsTr("300 E-Coins")
    }

        Core.SmallText {
        id: button2MessageText
        x: button2.x + button2.width/2 - button2MessageText.width/2
        y: button2.y + button2.height + 10
        text: qsTr("600 E-Coins")
    }

    
}

import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
  


    function updatePaidText(string ) {
        paidText.text = string
    }

    function updateVoucherText(string){
        voucherText.text = string
    }

    Core.LargeText {
        
        x: page.margin
        y: 100
        text: qsTr("Paid: ")
    }

    Core.LargeText {
        x: page.margin
        y: 130
        text: qsTr("voucher(s): ")
    }

    Core.LargeText {
        id: paidText
        x: 210
        y: 100
        text: qsTr("")
    }

    Core.LargeText {
        id: voucherText
        x: 210  
        y: 130
        text: qsTr("")
    }


    

  

  

}

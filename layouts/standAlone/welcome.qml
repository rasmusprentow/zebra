import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    MouseArea {
        anchors.fill: parent
        hoverEnabled: false
        onClicked: {
                context.onClicked('screen')
            }
    }

    Core.Topbar {
        id: topbar
        text: qsTr("Welcome to")
    }

    Image {
        id: oneStop
        source: "../img/OneStop.png"
            x: page.getCenterX(oneStop)
            y: page.getCenterY(oneStop) + 20
    }

     Core.MediumText {
        id: messageText
        x: page.getCenterX(messageText)
        y: page.height - (messageText.height + page.margin + 30)
        text: qsTr("Touch screen to continue")
    }
}

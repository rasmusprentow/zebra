import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    Core.LargeText {
        x: 59
        y: 111
        width: 396
        height: 32
        text: qsTr("Insufficient funds, \nGo to One Stop Shop")
    }
}
import QtQuick 1.0

import "../core" as Core
import "../js/stacks.js" as Stack

Core.Base {
    id: page
  
    function updateVoucher(string ) {
        voucherAmount.text = string
    }

    function updateECoin(string ) {
        eCoinAmount.text = string
    }
    
    function updateCurrency(string ) {
        currency.text = string
    }

    property int textline1height: 140
    property int textspace: 8

    Core.Topbar {
        id: topbar
        text: qsTr("Success")
    }

    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }

    Core.MediumText {
        id: productText1
        text: qsTr('You received')
        x: page.getCenterX(productText1) - 93
        y: textline1height
    }

    Core.MediumText {
        id: voucherAmount
        x: productText1.x + productText1.width + textspace
        y: textline1height - 11
        text: ""
        color: "#068a82"
        font.pixelSize: 35
    }

    Core.MediumText {
        id: eCoinAmount
        x: productText1.x + productText1.width + textspace
        y: textline1height - 11
        text: ""
        color: "#00aa04"
        font.pixelSize: 35
    }

    Core.MediumText {
        id: currency
        text: ""
        x: voucherAmount.x + voucherAmount.width + eCoinAmount.width + textspace
        y: textline1height
    }
}

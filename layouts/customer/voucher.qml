import QtQuick 1.0

import "../core" as Core
import "../js/stacks.js" as Stack

Core.Base {
    id: page
  
    property int textline1height: 115
    property int textspace: 8

    Core.Topbar {
        id: topbar
        text: qsTr("Thank you")
    }

    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }

    //Component.onCompleted: Stack.createDPStacks(380, 85, 10, -1);

        Image {
        id: balanceBar
        source: "../img/BalanceBar.png"
        x: 0
        y: 73
        smooth: true
    }
    
    Core.MediumText {
        id: productText1
        text: qsTr('You used')
        x: page.getCenterX(productText1) - 93
        y: textline1height
    }

    Core.MediumText {
        id: voucherAmount
        x: productText1.x + productText1.width + textspace
        y: textline1height - 11
        text: ""
        color: "#068a82"
        font.pixelSize: 35
    }

    Core.MediumText {
        id: productText2
        text: qsTr('Discount Points')
        x: voucherAmount.x + voucherAmount.width + textspace
        y: textline1height
    }

    Image {
        id: discount
        source: "../img/Discount.png"
        x: 33
        y: 194
        height: 64
        fillMode: Image.PreserveAspectFit
        smooth: true
    }

    Image {
        id: show2
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show2.width * 0.5
        y: 190
        width: 170
    }
                
    Image {
        id: show3
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show3.width * 0.5
        y: 230
        width: 170

        Core.MediumText {
            id: totalVoucherAmount
            //text: qsTr("4")
            horizontalAlignment: Text.AlignRight
            y:3
            width:parent.width - 5
        }
    }

    Core.MediumText {
        id: point
        text: qsTr("Points")
        x: page.width * 0.5 + show3.width * 0.5 +10
        y: show3.y + voucherAmount.y
    }

    function updateStack(param1, param2){
        Stack.createDPStacks(show2.x + 2, show2.y + 2, param1, param2)
    }

    function updateAmount(string){
        voucherAmount.text = string
    }

    function updateTotalAmount(string){
        totalVoucherAmount.text = string
    }

}
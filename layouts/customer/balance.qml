import QtQuick 1.0

import "../core" as Core
import "../js/stacks.js" as Stack

Core.Base {
    id: page

    Core.Topbar {
        id: topbar
        text: qsTr("Balance:")
    }
    
    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }

    //Component.onCompleted: Stack.createDPStacks(show2.x +2 , show2.y + 2, 50, 6);


    function updateAmount(string ) {
        amount.text = string
    }

    function updateVoucherAmount(string ) {
        voucher.text = string
    }

    function updateStack(param1, param2){
        Stack.createDPStacks(show2.x +2 , show2.y + 2, param1, param2)
        voucher.text = param1
    }

    Image {
        id: balanceBar
        source: "../img/BalanceBar.png"
        x: 0
        y: 73
        smooth: true
    }

    Image {
        id: money
        source: "../img/Money.png"
        x: 30
        y: 90
        height: 75
        fillMode: Image.PreserveAspectFit
        smooth: true
    }

    Core.MediumText {
        id: youGot
        text: qsTr("You have:")
        x: 161
        y: 92
    }

    Image {
        id: show1
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show1.width * 0.5
        y: 125
        width: 172

        Core.MediumText {
            id: amount
            //text: qsTr("600")
            horizontalAlignment: Text.AlignRight
            y:3
            width:parent.width - 5
        }
    }

    Core.MediumText {
        id: ugx
        text: qsTr("E-Coins")
        x: page.width * 0.5 + show1.width * 0.5 +10
        y: show1.y + amount.y
    }

    Image {
        id: discount
        source: "../img/Discount.png"
        x: 33
        y: 194
        height: 64
        fillMode: Image.PreserveAspectFit
        smooth: true
    }

    Image {
        id: show2
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show1.width * 0.5
        y: 190
        width: 172
    }
                
    Image {
        id: show3
        source: "../img/PinShow.png"
        x: page.width * 0.5 - show1.width * 0.5
        y: 230
        width: 172

        Core.MediumText {
            id: voucher
            //text: qsTr("4")
            horizontalAlignment: Text.AlignRight
            y:3
            width:parent.width - 5
        }
    }

    Core.MediumText {
        id: point
        text: qsTr("Points")
        x: page.width * 0.5 + show3.width * 0.5 +10
        y: show3.y + voucher.y
    }


    
}
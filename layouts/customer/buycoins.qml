import QtQuick 1.0

import "../core" as Core

Core.ScanPage {
    id: page

    function updateAmountText(string ) {
        amountText.text = string
    }
    
    Rectangle {
        id: topbarText
        x: page.getCenterX(topbarText)
        y: page.margin + page.offset
        width: buyText.width + amountText.width
        Core.LargeText {
            id: buyText
           
            x: 0
            text: qsTr("Get")
        }

        Core.MediumText {
            id: amountText
            x: buyText.x + buyText.width
            y: buyText.y + buyText.height - amountText.height 
            text: qsTr("")
            color: buyText.color
        }
    }
       
}
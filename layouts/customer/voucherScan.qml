import QtQuick 1.0

import "../core" as Core

Core.ScanPage {
    id: page

 

    function updateAmount(str) {
        amountText.text = " - " + str + " discount points"
    }



 
    Rectangle {
        id: topbarText
        x: page.getCenterX(topbarText)
        y: page.margin + page.offset
        width: buyText.width + amountText.width
        Core.LargeText {
            id: buyText
           
            x: 0
            text: qsTr("Use ")
        }

        Core.MediumText {
            id: amountText
            x: buyText.x + buyText.width
            y: buyText.y + buyText.height - amountText.height 
            text: qsTr("")
            color: buyText.color
        }
    }
    
   

          
}

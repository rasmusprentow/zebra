import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    function updateAmount(string ) {
        amount.text = string
    }

    function updateProduct(string ) {
        product.text = string
    }

    Core.MediumText {
        id: swipeText
        text: qsTr('Swipe to confirm')
        x: page.getCenterX(swipeText)
        y: page.margin
    }
    Core.MediumText {
        id: productText
        text: qsTr('Product: ')
        x: page.getCenterX(productText) - 70
        y: page.getCenterY(productText)
    }
    Core.MediumText {
        id: product
        x: page.getCenterX(product) + 40
        y: page.getCenterY(product)
        text: ""
    }
    Core.MediumText {
        id: amountText
        text: qsTr('Amount: ')
        x: page.getCenterX(amountText) - 70
        y: page.getCenterY(amountText) + 40
    }
    Core.MediumText {
        id: amount
        x: page.getCenterX(amount) + 40
        y: page.getCenterY(amount) + 40
    }
    Core.Cancel {
        id: cancel
        x: page.getCenterX(cancel)
        y: page.height - page.margin - cancel.height
    }
}
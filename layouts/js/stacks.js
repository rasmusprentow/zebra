

var component;
var sprite;
var spacing = 4
var margin = 1
var height = 13
var maxHeight = 10
var interStackDist = 30
var stacks = 1
var count = 0
var extras = 0

function createDPStacks(startX, startY, countStart, extra) {

    //var lastCreated = false
    count = (extra > 0) ? countStart : countStart + extra
    extras = extra
    if (count < 0) {
        count = 0
    }
    for (var i = 0; i < stacks; i++){
        var component = Qt.createComponent("../core/DPIndicator.qml");
        //component.createObject(page, {"z": 2, "x": startX + interStackDist * i , "y": startY});
        
        if (count > 0 || extras != 0)
        {
            createStack(margin + startX + interStackDist * i, margin + startY);
           
        }
    }

}

function createStack(startX, startY) {

    var buttomY = startY + ((maxHeight - 1) * spacing * height)

    for(var i=1; i<=maxHeight; i++) {
        if (count == 0 && extras ==0)
        {
            return
        }
        if (count == 0 && extras < 0){
            component = Qt.createComponent("../core/FadeOutDPBlock.qml");
            extras++
        }
        else if (count == 0 && extras > 0){
            component = Qt.createComponent("../core/FadeInDPBlock.qml");
            extras--;
            
        } else {
            count--;
            component = Qt.createComponent("../core/DPBlock.qml");
        }
        sprite = component.createObject(page, {"z":0, "x": startX  + ((i -1) * (height + spacing)), "y": startY});

        if (sprite == null) {
            // Error Handling
            console.log("Error creating object");
        }
    }
}







import QtQuick 1.0

import "../core" as Core






Core.Base {
    id: page

   Core.Topbar {
        id: topbar
        text: "Scan to login"

    }
   
    Image {
        id: swipe
        source: "../img/SwipeLeft.png"
        x: page.getCenterX(swipe) + 80
        y: page.getCenterY(swipe) + 32
        width: 289 * 0.95
        height: 174 * 0.95
        smooth: true
    }

    AnimatedImage {
        id: img
        source: "../img/ArrowsLeft.gif"
        x: page.margin + 20
        y: page.getCenterY(img) + 3 
        width: 158 * 0.8
        height: 70 * 0.8
        smooth: true
    }

    Core.MediumText {
        id: messageText
        x: page.getCenterX(messageText)
        y: page.height - 40
        text: qsTr("Put card on circle")
    }
          
}

import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    Core.Topbar {
        id: headline
        text: qsTr("Cash Purchase")
    
    }

    Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin 
    }

    function updateText(string ) {
        successText.text = string
    }
    Core.MediumText {
        id: successText
        x: page.getCenterX(successText)
        y: page.getCenterY(successText) + 20
    }

    Core.Button {
            
        id: button1
        width: 235
        height: 60
        callId: "paid"
        btnText: "Cash Received"
        x: page.getCenterX(button1) 
        y: page.margin + 185
    }
   

}


import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    property int btnheight: 80

    Core.Topbar {
        id: topbar
        text: "Check In or Out"

    }

    Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin 
    }


    Core.Button {
        id: inbtn
        width: 220
        height: btnheight
        callId: "in"
        btnText: "In"
        x: page.margin
        y: page.getCenterY(inbtn) + page.margin
    }

    Core.Button {
        id: outbtn
        width: 220
        height: btnheight
        callId: "out"
        btnText: "Out"
        x: page.getCenterX(outbtn) + 130 - page.margin
        y: page.getCenterY(outbtn)  + page.margin
    }



 
}
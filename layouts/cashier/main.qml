import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page

    property int btnheight: 45
    property int btnWidth: 200
    property int offsetheight: 60
    property int offsetwidth: 125
    property int startHeight: 80
    function setTime(string){
        time.text = string
    }

    Core.Topbar {
        id: topbar

            Image {
            id: logo
            source: "../img/CashierLogo.png"
            x: page.getCenterX(logo)
            y: 10
        }
    }

    Core.ManagementButton {
        id: button1
        width: btnWidth
        height: btnheight
        callId: "purchase"
        btnText: "E-Coin Buy"
        btnTextNum: "1"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 0
    }


    Core.ManagementButton {
        id: button2
        width: btnWidth
        height: btnheight
        callId: "voucherPurchase"
        btnText: "Discount Buy"
        btnTextNum: "2"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 1
    }

    Core.ManagementButton {
        id: button3
        width: btnWidth
        height: btnheight
        callId: "cashPurchase"
        btnText: "Cash Buy"
        btnTextNum: "3"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 2
    }

    Core.ManagementButton {
        id: button4
        width: btnWidth
        height: btnheight
        callId: "insertCoins"
        btnText: "Top up E-Coins"
        btnTextNum: "4"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 0
    }


    Core.ManagementButton {
        id: button5
        width: btnWidth
        height: btnheight
        callId: "register"
        btnText: "Register"
        btnTextNum: "5"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 1
    }
    
    Core.ManagementButton {
        id: button6
        width: btnWidth
        height: btnheight
        callId: "management"
        btnText: "Management"
        btnTextNum: "6"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 2
    }


    Core.SmallText {
        id: time
        text: qsTr("")
        y: page.height - 20
        x: page.margin
    }
 
}
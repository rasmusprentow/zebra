

import QtQuick 1.0

import "../core" as Core
import "../js/stacks.js" as Stack

Core.Base {
    id: page
  
    property int widthoffset: 145
    property int heightoffset: 30
    property int topmargin: 90
    property int secondcolx: 270

    function updateCoinin(string){
         coinin.text = string
    }

    function updateCoinout(string){
         coinout.text = string
    } 


    function updateVoucherin(string){
         voucherin.text = string
    }

    function updateVoucherout(string){
         voucherout.text = string
    } 


    function updateTotalVoucher(string){
         totalvoucher.text = string
    }

    function updateTotalCoin(string){
         totalcoins.text = string
    } 

    function updateTotalFees(string){
         totalfees.text = string
    } 

    function updateTopbars(string){
         topbar.text = string
    } 

    function updateButtonText(string){
         total_today_button.btnText = string
    } 
    
    function updateButtonCallId(string){
         total_today_button.callId = string
    } 

    function updateStatus(string){
         status.text = string
    } 

 
    Core.Topbar {
        id: topbar
        text: qsTr("Today")
    }

   Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin 
    }


    Core.SmallText {
        id: coinin_text
        text: qsTr("Purchases ")
        y: topmargin
        x: page.margin
    }

    Core.SmallText {
        id: coinin
        text: qsTr("N/A")
        y: topmargin
        x: page.margin + widthoffset 
    }



    Core.SmallText {
        id: coinout_text
        text: qsTr("UGX received")
        y: topmargin + heightoffset
        x: page.margin
    }

    Core.SmallText {
        id: coinout
        text: qsTr("N/A")
        y: topmargin + heightoffset
        x: page.margin + widthoffset 
    }




    Core.SmallText {
        id: voucherin_text
        text: qsTr("Vouchers Used")
        y: topmargin + heightoffset * 2
        x: page.margin
    }

    Core.SmallText {
        id: voucherin
        text: qsTr("N/A")
        y: topmargin + heightoffset * 2
        x: page.margin + widthoffset 
    }

 

    Core.SmallText {
        id: voucherout_text
        text: qsTr("Vouchers Granted")
        y: topmargin + heightoffset * 3
        x: page.margin
    }

    Core.SmallText {
        id: voucherout
        text: qsTr("N/A")
        y: topmargin + heightoffset * 3
        x: page.margin + widthoffset 
    }


 



    Core.SmallText {
        id: totalvoucher_text
        text: qsTr("Total voucher")
        y: topmargin + heightoffset * 0
        x: secondcolx 
    }

    Core.SmallText {
        id: totalvoucher
        text: qsTr("N/A")
        y: topmargin + heightoffset * 0
        x: secondcolx + widthoffset
      }



    Core.SmallText {
        id: totalcoins_text
        text: qsTr("Total Coins")
        y: topmargin + heightoffset * 1
        x: secondcolx
     }

    Core.SmallText {
        id: totalcoins
        text: qsTr("N/A")
        y: topmargin + heightoffset * 1
        x: secondcolx + widthoffset
   }

    

    // Core.SmallText {
    //     id: totalfees_text
    //     text: qsTr("Total Fees")
    //     y: topmargin + heightoffset * 2
    //     x: secondcolx
    // }

    // Core.SmallText {
    //     id: totalfees
    //     text: qsTr("N/A")
    //     y: topmargin + heightoffset * 2
    //     x:secondcolx + widthoffset
    // }



    Core.SmallText {
        id: status_text
        text: qsTr("Status")
        y: topmargin + heightoffset * 3
        x: secondcolx
    }

    Core.SmallText {
        id: status
        text: qsTr("N/A")
        y: topmargin + heightoffset * 3
        x:secondcolx + widthoffset
    }


    Core.Button {
        id: total_today_button
        btnText: qsTr("Total")
        callId: "total"
        y: topmargin + heightoffset * 4
        x: page.getCenterX(total_today_button)
        height: 40
        width:130
    }

  
    //     Image {
    //    id: dpindicator
    //    source: "../img/PinShow.png"
    //     width: dpamount.width + 4
    //     height: dpamount.height + 4
    //     y: page.getCenterY(dpindicator)
    //     x: page.getCenterX(dpindicator)
    // }

    // function updateVoucherAmount(string){
    //     voucherText.text = " - " + string + " Discount Points"
    // }

    // Rectangle {
    //     id: bbar
    //     height: 50
    //     width: 480
    //     color: "#a6b0b8"
    //     y: page.height - bbar.height
    // }

    // Rectangle {
    //     id: bbarText
    //     x: page.getCenterX(bbarText)
    //     y: page.height - page.margin - 30
    //     width: dpText.width + voucherText.width
    //     Core.MediumText {
    //         id: dpText
           
    //         x: 0
    //         text: qsTr("You get ")
    //     }

    //     Core.MediumText {
    //         id: voucherText
    //         x: dpText.x + dpText.width
    //         y: dpText.y + dpText.height - voucherText.height 
    //         text: qsTr("")
    //     }
    // }
}

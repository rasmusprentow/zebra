

import QtQuick 1.0

import "../core" as Core


Core.Base {
    id: page

    function setPrefilledOff(string ) {
          img.source  = "../img/ButtonD.png"
          img100.source = "../img/ButtonU.png"
    }

    function setPrefilledOn(string ) {
          img.source  = "../img/ButtonU.png"
          img100.source = "../img/ButtonD.png"
    }

    property int offset: topbar.offset

   Core.Topbar {
        id: topbar
        text: "Choose card type"

    }

    Core.Cancel {
        id: cancel
        x: page.margin
        y: page.margin 
    }



    Core.Base {
        id: cash_0
        Image {
            id: img
            source: cash_0.sourceDown
            width: cash_0.width
            height: cash_0.height
            smooth: true
        }
        height: 70
        width: 180
        color: "transparent"
        x: page.getCenterX(cash_0) - 115
        y: 130
      
        property string callId: "empty"
        property string sourceUp: "../img/ButtonU.png"
        property string sourceDown: "../img/ButtonD.png"
        
        MouseArea {
            
            anchors.fill: parent
            
            onPressed: {
                img.source  = "../img/ButtonD.png"
                img100.source = "../img/ButtonU.png"
            }
            onClicked: {
                context.onClicked(cash_0.callId)
            }
        }
        
    
        Text {
            id: textBtn
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "#555555"
            text: "Normal Card"
            font.bold: false
            font.family: "Arial"
            font.pixelSize: 25
        }
        
    }


    Core.Base {
        id: cash_100
        Image {
            id: img100
            source: cash_100.sourceUp
            width: cash_100.width
            height: cash_100.height
            smooth: true
        }
        height: 70
        width: 180
        color: "transparent"
        x: page.getCenterX(cash_100) + 115
        y: 130
        property string callId: "prefilled"
        property string sourceUp: "../img/ButtonU.png"
        property string sourceDown: "../img/ButtonD.png"
        
        MouseArea {
            
            anchors.fill: parent
            
           onPressed: {
                img100.source  = "../img/ButtonD.png"
                img.source = "../img/ButtonU.png"
            }
            onClicked: {
                context.onClicked(cash_100.callId)
            }
        }
        
    
        Text {
            id: textBtn100
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "#555555"
            text: "Donated Card"
            font.bold: false
            font.family: "Arial"
            font.pixelSize: 25
        }
        
    }


    
    Core.MediumText {
        id: messageText
        x: page.getCenterX(messageText)
        y: page.height - 40
        text: qsTr("Put card on circle")
    }
    
    //Core.MediumText {
        //id: headline
        //x: page.getCenterX(headline)
        //y: page.margin + 80
        //text: qsTr("Select prefilled amount")
    //}
          
}

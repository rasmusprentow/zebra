import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
    property int btnheight: 45
    property int btnwidth: 200
    property int offsetheight: 60
    property int offsetwidth: 125
    property int startHeight: 80
    function setTime(string){
        time.text = string
    }

    Core.Topbar {
        id: topbar
        text: "Management"
    }

    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }
    
    Core.ManagementButton {
        id: button1
        width: btnwidth
        height: btnheight
        callId: "checkinout"
        btnText: "Check In/Out"
        btnTextNum: "1"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 0
    }

    Core.ManagementButton {
        id: button2
        width: btnwidth
        height: btnheight
        callId: "reregister"
        btnText: "Re-register"
        btnTextNum: "2"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 1
    }

    Core.ManagementButton {
        id: button3
        width: btnwidth
        height: btnheight
        callId: "checkpocket"
        btnText: "Check Card"
        btnTextNum: "3"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 2
    }

    Core.ManagementButton {
        id: button4
        width: btnwidth
        height: btnheight
        callId: "insertdiscount"
        btnText: "Add Discount"
        btnTextNum: "4"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 0
    }


    Core.ManagementButton {
        id: button5
        width: btnwidth
        height: btnheight
        callId: "status"
        btnText: "Status"

        btnTextNum: "5"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 1
    }

   

    Core.ManagementButton {
        id: button6
        width: btnwidth
        height: btnheight
        callId: "system"
        btnText: "System"
        
        btnTextNum: "6"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 2
    }
}
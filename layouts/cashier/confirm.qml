import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
    property int btnheight: 45
    property int btnwidth: 200
    property int offsetheight: 60
    property int offsetwidth: 125
    property int startHeight: 80
    function updateHeader(string){
        topbar.text = string
    }

    Core.Topbar {
        id: topbar
        text: ""
    }

    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }
    
    Core.Button {
        id: button1
        width: btnwidth
        height: btnheight
        callId: "no"
        btnText: "No"
        x: page.getCenterX(button2) - offsetwidth + page.margin
        y: page.margin + startHeight + offsetheight * 0
    }


 
    Core.Button {
        id: button2
        width: btnwidth
        height: btnheight
        callId: "yes"
        btnText: "Yes"
        x: page.getCenterX(button2) + offsetwidth - page.margin
        y: page.margin + startHeight + offsetheight * 0
    }


}
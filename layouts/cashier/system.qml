

import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
    property int btnheight: 45
    property int btnwidth: 220
    property int offsetheight: 70
    property int offsetwidth: 155
    property int startHeight: 100
    function setTime(string){
        time.text = string
    }

    Core.Topbar {
        id: topbar
        text: "System"
    }

    Core.Cancel {
        id: cancel 
        x: page.margin
        y: page.margin 
    }
    
    Core.Button {
        id: button1
        width: btnwidth
        height: btnheight
        callId: "update"
        btnText: "Update"
        x: page.getCenterX(button1)
        y: page.margin + startHeight
    }


 
    Core.Button {
        id: button2
        width: btnwidth
        height: btnheight
        callId: "shutdown"
        btnText: "Shutdown"
        x: page.getCenterX(button2)
        y: page.margin + startHeight + offsetheight * 1
    }


}
import QtQuick 1.0

import "../core" as Core

Core.Base {
    id: page
    
    
   
    AnimatedImage {
        id: img
        source: "../img/Loading.gif"
        x: page.getCenterX(img)
        y: page.getCenterY(img) + 35
    }


    Core.LargeText {
            id: infoText
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            x: page.getCenterX(infoText)
            y: page.getCenterY(infoText)
            
            text: qsTr("Waiting for customer")
    }

    
 

}

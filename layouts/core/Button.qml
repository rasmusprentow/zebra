import QtQuick 1.0

    Base {
        id: button
        Image {
            id: img
            source: button.sourceUp
             width: button.width
            height: button.height
            smooth: true
        }

        color: "transparent"
        property int realWidth: button.width
        property int realHeight: button.height
        property string btnText
        property string callId
        property string sourceUp: "../img/ButtonU.png"
        property string sourceDown: "../img/ButtonD.png"
        
        MouseArea {
            
            anchors.fill: parent
            
            onPressed: img.source  = button.sourceDown
            onReleased: img.source  = button.sourceUp
            onClicked: {
                context.onClicked(button.callId)
            }
        }
        
    
        Text {
            id: textBtn
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            color: "#555555"
            text: parent.btnText
            font.bold: false
            font.family: "Arial"
            font.pixelSize: 30
        }
        
    }
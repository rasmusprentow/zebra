import QtQuick 1.0
 
    Base {
        id: managementButton
        Image {
            id: img
            source: managementButton.sourceUp
             width: managementButton.width +10
            height: managementButton.height
            smooth: true
        }
 
        color: "transparent"
        property int realWidth: managementButton.width
        property int realHeight: managementButton.height
        property string btnText
        property string btnTextNum
        property string callId
        property string sourceUp: "../img/ManagementU.png"
        property string sourceDown: "../img/ManagementD.png"
       
        MouseArea {
           
            anchors.fill: parent
           
            onPressed: img.source  = managementButton.sourceDown
            onReleased: img.source  = managementButton.sourceUp
            onClicked: {
                context.onClicked(managementButton.callId)
            }
        }
        Rectangle {
            id: rectangle1
            color: "transparent"
       
            x: 0
            y: 0
            width: 40
            height: managementButton.height
            //color: "transparent"
       
            Text {
                id: textBtnNum
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: "#2b2b2b"
                text: parent.parent.btnTextNum
                font.bold: false
                font.family: "Arial"
                font.pixelSize: 22
            }
        }
 
       
        Rectangle {
            id: rectangle2
           
            x: 40
            y: 0
            width: managementButton.width - 40 + 10
            height: managementButton.height
            color: "transparent"
       
            Text {
                id: textBtn
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: "#2b2b2b"
                text: parent.parent.btnText
                font.bold: false
                font.family: "Arial"
                font.pixelSize: 22
            }
        }
    }
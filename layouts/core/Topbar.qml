


import QtQuick 1.0

Base {
    id: topbar

    Image {
        id: topbarImg
        source: "../img/Topbar.png" 
    }

    property string text
    property int offset: 10

    LargeText {
        id: headline
        x: page.getCenterX(headline)
        y: page.margin + offset
        text: topbar.text
        font.pixelSize: 33
    }

   
}

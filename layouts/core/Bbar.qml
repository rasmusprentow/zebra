


import QtQuick 1.0

Base {
    id: bbar

    Image {
        id: bbarImg
        source: "../img/Bbar.png" 
    }

    property string text
    property int offset: 10

    LargeText {
        id: headline
        x: bbar.getCenterX(headline)
        y: bbar.margin + offset
        text: bbar.text
        font.pixelSize: 25
    }

   
}
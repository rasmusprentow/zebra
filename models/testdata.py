



import datetime, os


from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import MetaData
from sqlite3 import OperationalError
from models.model import engine, Voucher, Shop, Pocket, Safe, PocketType, ShopType, Transaction, Location
from excep.excep import InvalidSerialException, SerialNotFoundException
# from helpers.authenticator import Authenticator
import logging, logging.config
logging.config.fileConfig('config/logging.conf')
logger = logging.getLogger("model")
from red.config      import config
# create a Session

config.read('config/db.conf')

def createTestData():
    
    createEmptyDb()
    createServerTestData()
    Session = sessionmaker(bind=engine)
    session = Session()


    ps1 = Safe(0)
    ps2 = Safe(0)
    ps3 = Safe(0)
    ps4 = Safe(0)
    ps5 = Safe(0)
    ps6 = Safe(0) 
    ps7 = Safe(0)
   
    session.add_all([ps1,ps2,ps3,ps4,ps5,ps6,ps7])
    session.commit()

    #pockets
    seller =  session.query(PocketType).filter_by(name="seller").first()
    customer =session.query(PocketType).filter_by(name="customer").first()
    cashier = session.query(PocketType).filter_by(name="cashier").first()
       
    p1 = Pocket(ps1, '7da0b6b4', ('1234'), seller) #white tag
    p2 = Pocket(ps2, '270bdee4', ('1234'), customer)
    p3 = Pocket(ps3, 'a64fdee4', ('1234'), customer)
    p4 = Pocket(ps4, '3429dee4', ('1234'), customer)
    p5 = Pocket(ps5, '795cdee4', ('1234'), customer)
    p6 = Pocket(ps6, '12345678', ('1234'), customer) #test pocket, not real serial
    p7 = Pocket(ps7, 'aaaaaaaa', ('1234'), cashier) 
   
    session.add_all([p1, p2, p3, p4, p5, p6, p7])
    session.commit()

    from helpers.helperFactory import HelperFactory
    HelperFactory.getTransactor(session).insertCoins('12345678',3000)
    HelperFactory.getTransactor(session).insertCoins('270bdee4',2000)
    HelperFactory.getTransactor(session).purchase('12345678',300)
    



    session.close()
    
def createEmptyDb(session=None):

   
    Session = sessionmaker(bind=engine)
    session = Session()
    
    


    # transactor = HelperFactory.getTransactor(session)
    # transactor.insertCoins('12345678',3000)
    # transactor.insertCoins('270bdee4',2000)
    # transactor.purchase('12345678',300)
    # 
    session.close()

def createServerTestData():
    Session = sessionmaker(bind=engine)
    session = Session()
    
    
    seller = PocketType("seller")
    customer = PocketType("customer")
    cashier = PocketType("cashier")

    session.add_all([seller,customer, cashier])
    session.commit()

    pes = ShopType('pes')
    agent = ShopType('agent')
    vendor = ShopType('vendor')

    session.add_all([pes,agent,vendor])
    session.commit()

    #shop safe

    
    ps0 = Safe(0)
    ps1 = Safe(0)
    ps2 = Safe(0)
    ps3 = Safe(0)
    ps4 = Safe(0)
    ps5 = Safe(0)
    ps6 = Safe(0) 
    ps7 = Safe(0)
    ps8 = Safe(0)
    ps9 = Safe(0)
    ps10 = Safe(0)
    ps11 = Safe(0)
    ps12 = Safe(0)
    ps13 = Safe(0)
    ps14 = Safe(0) 
    ps15 = Safe(0)
    ps16 = Safe(0)
   

    ss1 = Safe(0)
    ss2 = Safe(0)
    ss3 = Safe(0)

    session.add_all([ps0,ps1,ps2,ps3,ps4,ps5,ps6,ps7,ps8,ps9,ps10, ps11, ps12, ps13, ps14, ps15, ps16, ss1, ss2, ss3])
    session.commit()

    #pockets
    seller =  session.query(PocketType).filter_by(name="seller").first()
    customer =session.query(PocketType).filter_by(name="customer").first()
    cashier = session.query(PocketType).filter_by(name="cashier").first()
       

    p0 = Pocket(ps0, 'cash_buy', "eeee", customer) #white tag
    p1 =  Pocket(ps1,  '0b600b36', '3414', cashier)
    p2 =  Pocket(ps2,  '8b62ff35', '5526', cashier)
    p3 =  Pocket(ps3,  '8b8e0d36', '9834', cashier)
    p4 =  Pocket(ps4,  'cb130136', '3432', cashier)
    p5 =  Pocket(ps5,  'fbfbff35', '4355', cashier)
    p6 =  Pocket(ps6,  '4b6d0636', '8245', cashier)
    p7 =  Pocket(ps7,  '7b680236', '9124', cashier)
    p8 =  Pocket(ps8,  '5bed0236', '7742', cashier)
    p9 =  Pocket(ps9,  'ebe20936', '8375', cashier)
    p10 = Pocket(ps10, '4b5eff35', '2518', cashier)
    p11 = Pocket(ps11, 'abde0b36', '9698', cashier)
    p12 = Pocket(ps12, '5b140636', '2857', cashier)
    p13 = Pocket(ps13, '3bef4b08', '9361', cashier)
    p14 = Pocket(ps14, 'abb90736', '4831', cashier)
    p15 = Pocket(ps15, '1b5a4d08', '9436', cashier)
    p16 = Pocket(ps16, 'fb1f0936', '8329', cashier)


    # p1 = Pocket(ps1, '7da0b6b4', Authenticator.hashPin('1234'), seller) #white tag
    # p2 = Pocket(ps2, '270bdee4', Authenticator.hashPin('1234'), customer)
    # p3 = Pocket(ps3, 'a64fdee4', Authenticator.hashPin('1234'), customer)
    # p4 = Pocket(ps4, '3429dee4', Authenticator.hashPin('1234'), customer)
    # p5 = Pocket(ps5, '795cdee4', Authenticator.hashPin('1234'), customer)
    # p6 = Pocket(ps6, '12345678', Authenticator.hashPin('1234'), customer) #test pocket, not real serial


    session.add_all([p0,p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16])

    session.commit()  



    #shops
    Gardens = Shop(ss2, 'gardens', vendor)
    Envi = Shop(ss1, 'enviclean', agent)
    PES = Shop(ss3, 'pes', pes)
   

    session.add_all([Gardens, Envi, PES])
    session.commit() 

    #loactions
    onestop1 = Location('onestop1', Envi)
    onestop2 = Location('onestop2', Envi)
    onestop3 = Location('onestop3', Envi)

    session.add_all([onestop2, onestop1, onestop3])

    session.commit()  






    pass #createTestData()
    # Session = sessionmaker(bind=engine)
    # session = Session()
    
    
    # seller = PocketType("seller")
    # customer = PocketType("customer")

    # session.add_all([seller,customer])
    # session.commit()

    # pes = ShopType('pes')
    # agent = ShopType('agent')
    # vendor = ShopType('vendor')

    # session.add_all([pes,agent,vendor])
    # session.commit()

    # #shop safe

    # ps1 = Safe(1000)
    # ss1 = Safe(300000)
    # ss2 = Safe(30000)
    # ss3 = Safe(1)
    # session.add_all([ps1, ss1, ss2, ss3])
    # session.commit()

    # #pockets

    # p1 = Pocket(ps1, '7da0b6b4', Authenticator.hashPin('1234'), seller) #white tag
 
    # session.add_all([p1])
    # session.commit()

    # #shops
    # Gardens = Shop(ss2, 'gardens', vendor)
    # Envi = Shop(ss1, 'enviclean', agent)
    # PES = Shop(ss3, 'pes', pes)
    
    # session.add_all([Gardens, Envi, PES])
    # session.commit()    
    # session.close()

def clearTestData():
    connection =  engine.connect()
    try:
        engine.execute("delete from pockets")
        engine.execute("delete from shops")
        engine.execute("delete from safes")
        engine.execute("delete from pocket_types")
        engine.execute("delete from shop_types")
        engine.execute("delete from vouchers")
        engine.execute("delete from transactions")
    except Exception as e:
        logger.warning("Failed to delete from table. "+ str(e))
    connection.close()
   
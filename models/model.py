#tabelDefine.py

import datetime, os


from red.config import config, get_config
db_url = get_config(config, "Database", "connectionstring", default="sqlite://")

import logging 

logger = logging.getLogger("kernel")
logger.info("Loading db: " + db_url)

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, DateTime, Integer, String, Boolean
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from red.utils.run_once import run_once


engine = create_engine(db_url, echo=False)
Base = declarative_base()

########################################################################
class Voucher(Base):
    """"""
    __tablename__ = "vouchers"
    id = Column(Integer, primary_key=True)
    pid = Column(Integer, ForeignKey('pockets.id'), nullable=False)  
    sid = Column(Integer, ForeignKey('shops.id'), nullable=False)
    amount = Column(Integer, nullable=False)
    
    pocket = relationship("Pocket", back_populates='vouchers')
    shop = relationship("Shop")

    #----------------------------------------------------------------------
    def __init__(self, pocket, shop, amount):
        """"""        
        self.pid = pocket.id
        self.sid = shop.id
        self.amount = amount   

########################################################################
class PocketType(Base):
    """"""
    __tablename__ = "pocket_types"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)



    #### THERE IS AN ERROR HERE

    #----------------------------------------------------------------------
    def __init__(self, name):
        """"""
        self.name = name    


########################################################################
class ShopType(Base):
    """"""
    __tablename__ = "shop_types"
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

    #----------------------------------------------------------------------
    def __init__(self, name):
        """"""
        self.name = name    


########################################################################
class Safe(Base):
    """"""
    __tablename__ = "safes"
    id = Column(Integer, primary_key=True)
    amount = Column(Integer, nullable=False)
    

    #----------------------------------------------------------------------
    def __init__(self, amount):
        """"""
        self.amount = amount

    



########################################################################
class Pocket(Base):
    """"""
    __tablename__ = "pockets"
    id = Column(Integer, primary_key=True)
    createdAt = Column(String, default=datetime.datetime.utcnow)
    snr = Column(String, unique=True, nullable=False, index=True)
    pin = Column(String, nullable=False)
    pocketTypeId = Column(Integer, ForeignKey('pocket_types.id'), nullable=False)
    safeId = Column(Integer, ForeignKey('safes.id'), nullable=False)
    #recoveryCode = Column(String, nullable=False)
    
    safe = relationship("Safe")
    pocketType = relationship("PocketType")
    vouchers = relationship("Voucher")
    #----------------------------------------------------------------------
    def __init__(self, safe, snr, pin, ptype):
        """"""
        self.snr = snr
        self.pin = pin
        self.pocketTypeId = ptype.id
        self.safeId = safe.id

########################################################################
class Location(Base):
   __tablename__ = "locations"
   id = Column(Integer, primary_key=True)
   name = Column(String, nullable=False)
   shopId = Column(Integer, ForeignKey('shops.id'), nullable=False)
   last_synced = Column(String, default='')
   shop = relationship("Shop")
    

   #----------------------------------------------------------------------
   def __init__(self, name, shop):
       """"""
       self.name = name 
       self.shopId = shop.id 


########################################################################
class Shop(Base):
    """"""
    __tablename__ = "shops"
    id = Column(Integer, primary_key=True)
    safeId = Column(Integer, ForeignKey('safes.id'), nullable=False)
    shopTypeId = Column(Integer, ForeignKey('shop_types.id'), nullable=False)
    name = Column(String, unique=True, index=True, nullable=False)
    
    safe = relationship("Safe")
    shopType = relationship("ShopType")
    #----------------------------------------------------------------------
    def __init__(self, safe, name, stype):
        """"""        
        self.name = name  
        self.safeId = safe.id
        self.shopTypeId = stype.id


########################################################################
class Transaction(Base):
    __tablename__ = "transactions"

    id = Column(Integer, primary_key=True)
    timestamp = Column(String, default=datetime.datetime.utcnow)
    synced = Column(String, default='')
    srcId = Column(Integer, ForeignKey('safes.id'), nullable=False)
    destId = Column(Integer, ForeignKey('safes.id'), nullable=False)
    lid = Column(Integer, ForeignKey('locations.id'), nullable=False)
    amount = Column(Integer, nullable=False)
    voucherAmount = Column(Integer, nullable=False)
    fee = Column(Integer, nullable=False)
    note = Column(String, default='')


    src = relationship(Safe, primaryjoin="(Transaction.srcId == Safe.id)")
    dest = relationship(Safe, primaryjoin="(Transaction.destId == Safe.id)")

    location = relationship("Location", backref=backref('transactions', order_by=Location.id))
    #----------------------------------------------------------------------
    def __init__(self, src, dest, amount, vamount, fee, location, synced=None, timestamp=None, note=''):
        """"""

        assert isinstance(src, Safe), src
        assert isinstance(dest, Safe), dest

        if synced != None:
            self.synced = synced

        if timestamp != None:
            self.timestamp = timestamp

        self.lid = location.id
        self.srcId = src.id
        self.destId = dest.id
        self.lid = location.id
        self.amount = amount
        self.voucherAmount = vamount  
        self.fee = fee
        self.note = note
        
def initSchema():
    Base.metadata.create_all(engine)

def dropSchema():
    Base.metadata.drop_all(engine)


@run_once
def init():
    initSchema();

init()


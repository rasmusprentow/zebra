#server.py


#server.py

from flask import Flask, request
from red.config import config
config.read("config/server.conf")

from models.model import engine
from sqlalchemy.orm import sessionmaker
from helpers.synchronizer import Synchronizer
import json

Session = sessionmaker(bind=engine)

class Server(object):
    
    def start(self):
 
        app = Flask(__name__)
        
        @app.route("/", methods=['GET', 'POST'])
        def hello():
            return "Hello World!"
        
        @app.route("/get_transactions", methods=['GET', 'POST'])
        def getTransactions():
            session = Session()
            s = Synchronizer(session)
            lastSync = s.getLastSynced(request.args['location'],request.args['shop'])
            return json.dumps(s.getData(lastSync))

        @app.route("/set_last_synced", methods=['GET', 'POST'])
        def setLatSynced():
            session = Session()
            s = Synchronizer(session)
            lastSync = s.setLastSynced(session, locationName=request.args['location'],shopName=request.args['shop'], time=request.args['time'])
            return "", 200
        
        @app.route("/push_transaction", methods=['GET', 'POST'])
        def postTransactions():
         
            session = Session()
            s = Synchronizer(session)
            s.processTransaction(request.json['data'])

            return "", 200
        # from OpenSSL import SSL
        # context = SSL.Context(SSL.SSLv23_METHOD)
        # context.use_privatekey_file('new.cert.key')

        app.run(host='77.66.51.6',port=2324, debug=True) #, ssl_context=context)  
          

 
# from server.server import Server

Server().start()

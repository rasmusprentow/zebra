#human_nfc_verification.py

import Adafruit_BBIO.UART as UART
import logging, logging.config, sys, time
import serial
import zmq
from  red.config import config
UART.setup('UART4')
config.read("config/standAlone.conf")

config.set("LPC","use_mock_reader",'false')
config.set("LPC","port",'/dev/ttyO4')

logging.config.fileConfig('config/logging.conf')
logger = logging.getLogger(__file__)

def _e(msg):
    sys.stdout.write(msg)

_e("Starting Human Nfc Test\n");

name = 'inproc://lpc'
real_context = zmq.Context.instance();
socket = real_context.socket(zmq.PAIR)
socket.bind(name)


_e("Importing NfcReader...........")
from red.services.lpc import Lpc
_e("ok\n")

_e("Creating LPC............")
lpc = Lpc(name)
_e("ok\n")

_e("Starting LPC............")
lpc.start()
_e("ok\n")

_e("Place TAG on reader:\n")
old = ""
while True:
    socket.send_json({'head' : 'get_tag'})
    msg = socket.recv_json()
    
    if old != msg["data"]:
        print(str(msg["data"]))
        old = msg["data"]



_e("Test Finished")

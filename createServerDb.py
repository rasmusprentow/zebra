#testData.py

import datetime, os

os.system('rm server.db')

from red.config import config

config.add_section('Database')
config.set('Database', 'connectionstring', 'sqlite:///server.db')

from models.testdata import createServerTestData


createServerTestData()

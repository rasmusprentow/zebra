

#test_cashier_purchase.py


import sys

sys.path.append('.')

import unittest
import zmq

from threading import Thread

from activities.customer.welcome import Welcome 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')

from helpers.helperFactory import HelperFactory
from red.config import config
from config.strings import *

from models.model import Pocket

class Test_CustomerWelcome(BaseActivityTest):
 
    def setUp(self):
        super(Test_CustomerWelcome, self).setup()
        self.setServices(["cashier", "display"]) 
        self.setServices(["customer", "display"])
        self.activity = self.getActivity(Welcome)
        self.activity.notificationSleepTime = 0



    def tearDown(self):
        super(Test_CustomerWelcome, self).tearDown()
    
  


    def testCashierMessage(self):

        self.activity.receiveCashierMessage({"head" : "setlayout" , "data" : {'layout':"common/notification", "function" : "updateText", 'param' : "test"}})
        self.assertReceived("display", {"head":"set_layout", "data":"common/notification"})
        self.assertReceived("display", {"head":"call_func", "data":{"func" : "updateText", "param": "test"}})


if __name__ == "__main__":
    unittest.main() # run all tests

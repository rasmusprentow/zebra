import sys

sys.path.append('.')

import unittest

from activities.standAlone.scanPocket import Scanpocket
import logging, logging.config

from red.tests.baseActivityTest import BaseActivityTest
logging.config.fileConfig('config/logging.conf')

from red.config import config
from red.kernel import Kernel


class Test_ScanPocket(BaseActivityTest):
 
    def setUp(self):
        super(Test_ScanPocket, self).setup()

        self.setServices(["display", "lpc"])

        try:
            config.add_section('Pocket')
        except:
            pass
        config.set('Pocket','snr_len', '8')
         
    def tearDown(self):
        super(Test_ScanPocket, self).tearDown()

        """Call after every test case."""
         
        pass
 
    def testScanPocket(self):
        dataIn = {'product' : 'bbbb', 'amount' : '300'}
        dataOut = {'snr' : 'aaaaaaaa', 'product' : 'bbbb', 'amount' : '300'}
        activity = self.getActivity(Scanpocket)
        activity.onCreate(dataIn)
        activity.receiveLpcMessage({'head':'tag', 'data' : 'aaaaaaaa'})
        self.assertSwitchActivity('authenticate', dataOut)
          
if __name__ == "__main__":
    unittest.main() # run all tests
#test_st_receipt.py

import sys

sys.path.append('.')


from activities.standAlone.receipt import Receipt 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from red.tests.baseActivityTest import BaseActivityTest
logging.config.fileConfig('config/logging.conf')
import configparser
from red.config import config
from red.kernel import Kernel





class Test_St_Receipt(BaseActivityTest):
 
    def setUp(self):
        super(Test_St_Receipt, self).setup()
    
        clearTestData()
        createTestData()
        
        self.setServices(["display"])
        self.activity = self.getActivity(Receipt)
        self.activity.notificationSleepTime = 0
        self.data = {"snr" : "12345678", "product" : "peepoobag"}


        try:
            config.add_section('Products')
        except:
            pass

        config.set('Products','peepoobag', '4000')

        try:
            config.add_section('Pocket')
        except:
            pass
        config.set('Pocket','snr_len', '8')


         

    def tearDown(self):
        super(Test_St_Receipt, self).tearDown()
    

        """Call after every test case."""
      
        pass

    def testOnCreateFail1(self):
        data = {"snr" : "8888", "product" : "peepoobag"}
        self.assertRaises(ValueError, self.activity.onCreate,data)
    
    def testOnCreateFail2(self):
        data = {"product" : "peepoobag"}
        self.assertRaises(ValueError, self.activity.onCreate,data)
       
    def testOnCreateFail3(self):
        data = None
        self.assertRaises(ValueError, self.activity.onCreate,data)
       
    def testOnCreateFail4(self):
        data = {"snr" : "12345678"}
        self.assertRaises(ValueError, self.activity.onCreate,data)

    def testOnCreateFail5(self):
        data = {"snr" : "12345678", "product" : "johndoesnotexist"}
        self.assertRaises(configparser.NoOptionError, self.activity.onCreate,data)
    
    def testOnCreatePass(self):
        self.activity.onCreate(self.data)
        self.assertSetLayout("common/loading")
        self.assertSwitchActivity(anyActivity=True)

    def testReceiveDisplayMessage(self):
        self.activity.onCreate(self.data)

        dataIn = {"head" : "button_clicked", "data" : "cancel"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertSwitchActivity(anyActivity=True)         
    
    # def testPayInsuffientCoin(self):
    #     self.activity.pay("12345678", 10000000, 'product1')
    #     self.assertSwitchActivity(anyActivity=True)   
         
    def testPayFakeSerial(self):
        self.activity.pay("88888888", 10, 'product1')
        self.assertSwitchActivity(anyActivity=True)   
    
    def testPayPass(self):
        self.activity.pay("12345678", 10, 'product1')
        self.assertSetLayout("common/purchaseReceipt")   
          
      


if __name__ == "__main__":
    unittest.main() # run all tests
#test_transactor.py

import unittest
from red.config import config
import math, os, datetime


try:
    config.add_section("Database")
except:
    pass
config.set("Database", "connectionstring",'sqlite://')

from helpers.helperFactory import HelperFactory
from excep.excep import InsufficientCoinException, OverLimitCoinException, SerialNotFoundException, InsufficientVoucherException
from sqlalchemy.orm import sessionmaker
from models.testdata import createTestData, clearTestData
from models.model import engine, Pocket, Safe, Shop, Transaction
from helpers.transactor import Transactor
 
class Test_Transactor(unittest.TestCase):
 
    def setUp(self):
        """Call before every test case."""
        self.session = sessionmaker(bind=engine)()
        self.snr = '270bdee4'
        clearTestData()
        createTestData()
        try:
            config.add_section('Shop')
        except:
            pass

        config.set('Shop','voucher_percentage','0.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')
        config.set('Shop','location','onestop1')
        


        self.shopName = config.get('Shop', 'name')
        self.shop = self.session.query(Shop).filter_by(name=self.shopName).first()
 
    def tearDown(self):
        """Call after every test case."""
        
        self.session.close()
        pass
 
    def testPurchase(self):

        voucher_percentage = float(config.get('Shop', 'voucher_percentage')) / 100
        fee_percentage = float(config.get('Shop', 'fee_percentage')) / 100
        shopName = config.get('Shop', 'name')

        amount = 100

        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        s = self.session.query(Shop).filter_by(name=shopName).first()
        
        oldAmount = p.safe.amount
        oldShopAmount = s.safe.amount
        
        HelperFactory.getTransactor(self.session).purchase(self.snr, amount)

        transaction = self.session.query(Transaction).filter_by(srcId=p.safe.id, destId=s.safe.id).first()

        fee = math.ceil(amount * fee_percentage)
        expectedShopAmount = (oldShopAmount + amount - fee)
        
        """
        asserts
        """
        self.assertTrue(isinstance(transaction, Transaction)) 
        self.assertEqual((oldAmount - amount), p.safe.amount) 
        self.assertEqual(transaction.fee, fee), 'Expected: ' + str(fee) + ' got: ' + str(transaction.fee)
        self.assertEqual(expectedShopAmount, transaction.dest.amount), 'Expected: ' + str(expectedShopAmount) + ' got: ' + str(transaction.dest.amount)
        self.assertEqual(transaction.voucherAmount, math.floor(amount * voucher_percentage))
        
        self.assertEqual(oldAmount + oldShopAmount, s.safe.amount + p.safe.amount + transaction.fee)

        envicleanVoucher = [x for x in p.vouchers if lambda x: x.shopId == s.id].pop()
        self.assertEqual(envicleanVoucher.amount, (math.floor(amount * voucher_percentage)))

    def testVoucher(self):

        voucher_percentage = float(config.get('Shop', 'voucher_percentage')) / 100
        shopName = config.get('Shop', 'name')

        amount = 300

        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        s = self.session.query(Shop).filter_by(name=shopName).first()

        HelperFactory.getTransactor(self.session).purchase(self.snr, amount)
        HelperFactory.getTransactor(self.session).purchase(self.snr, amount)

        envicleanVoucher = [x for x in p.vouchers if lambda x: x.shopId == s.id].pop()

        """
        asserts
        """
        self.assertEqual(envicleanVoucher.amount, 1 * 2)
    
    def testCheckCoins(self):
        amount = 1000000000

        """
        asserts
        """
        self.assertRaises(InsufficientCoinException, HelperFactory.getTransactor(self.session).purchase, self.snr, amount)

    def testInsertCoins(self):
        snr = self.snr
        p = self.session.query(Pocket).filter_by(snr=snr).first()
        oldAmount = p.safe.amount
        amount = 200

        shopName = config.get('Shop', 'name')
        s = self.session.query(Shop).filter_by(name=shopName).first()
        oldShopAmount = s.safe.amount

        HelperFactory.getTransactor(self.session).insertCoins(snr, amount)

        transaction = self.session.query(Transaction).filter_by(srcId=s.safe.id, destId=p.safe.id, amount=amount).order_by(Transaction.timestamp).order_by(Transaction.timestamp).first()

        self.assertEqual(p.safe.amount, oldAmount + amount)
        self.assertEqual(s.safe.amount, oldShopAmount - amount)

        self.assertEqual(transaction.amount, amount)
        self.assertEqual(oldAmount + oldShopAmount, p.safe.amount + s.safe.amount)


    def testInsertCoinsOveRLimit(self):
        snr = self.snr
        p = self.session.query(Pocket).filter_by(snr=snr).first()
        oldAmount = p.safe.amount
        amount = 20001 - oldAmount

        shopName = config.get('Shop', 'name')
        s = self.session.query(Shop).filter_by(name=shopName).first()
        oldShopAmount = s.safe.amount

        self.assertRaises(OverLimitCoinException,HelperFactory.getTransactor(self.session).insertCoins,snr, amount, obeyLimit=True)

    def testCorrectVoucherAmount(self):
        snr = self.snr
        p = self.session.query(Pocket).filter_by(snr=snr).first()
        
        HelperFactory.getTransactor(self.session).purchase(self.snr, 300)


        self.assertEqual(HelperFactory.getTransactor(self.session).correctVoucherAmount(p, 10), 10)
        self.assertEqual(HelperFactory.getTransactor(self.session).correctVoucherAmount(p, -1000), -1000)
        self.assertEqual(HelperFactory.getTransactor(self.session).correctVoucherAmount(p, 99), 99)
        self.assertEqual(HelperFactory.getTransactor(self.session).correctVoucherAmount(p, 101), 99)
        self.assertEqual(HelperFactory.getTransactor(self.session).correctVoucherAmount(p, 100), 99)


    def testInsertNegativeCoins(self):
        amount = -200
        self.assertRaises(Exception, HelperFactory.getTransactor(self.session).insertCoins, self.snr, amount)

    def testInsertABCCoins(self):
        amount = 'abc'
        self.assertRaises(Exception, HelperFactory.getTransactor(self.session).insertCoins, self.snr, amount)

    def testSerialNotFound(self):
        unknownSerial = '11a22b3344'
        amount = 100

        self.assertRaises(SerialNotFoundException, HelperFactory.getTransactor(self.session).insertCoins, unknownSerial, amount)
        self.assertRaises(SerialNotFoundException, HelperFactory.getTransactor(self.session).purchase, unknownSerial, amount)

    # def testShopOutOfCoins(self):
    #     shopName = config.get('Shop', 'name')
    #     s = self.session.query(Shop).filter_by(name=shopName).first()
    #     s.safe.amount = 0
    #     amount = 100
    #     self.assertRaises(InsufficientCoinException, HelperFactory.getTransactor(self.session).insertCoins, self.snr, amount)

    # def testInsertCoinsFromPEStoShop(self):
    #     shopName = config.get('Shop', 'name')
    #     pes = self.session.query(Shop).filter_by(name='PES').first()
    #     pes.safe.amount = 0

    #     amount = 100

    #     s = self.session.query(Shop).filter_by(name=shopName).first()
    #     oldShopAmount = s.safe.amount


    #     HelperFactory.getTransactor(self.session).insertCoinsFromPES(amount)


    #     transaction = self.session.query(Transaction).filter_by(srcId=pes.id, destId=p.safe.id).first()

    #     self.assertEqual(p.safe.amount, oldAmount + amount)
    #     self.assertEqual(s.safe.amount, oldShopAmount - amount)

    #     self.assertEqual(transaction.amount, amount)
    #     self.assertEqual(oldAmount + oldShopAmount, p.safe.amount + s.safe.amount)


    def testGetShopVoucher(self):
        amount = 300
        HelperFactory.getTransactor(self.session).purchase(self.snr, amount)
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()

        result = Transactor.getShopVoucher(p, self.shop).amount
        expected = 1
        self.assertEqual(expected, result)

    def testVoucherPurchasePass1(self):
        HelperFactory.getTransactor(self.session).purchase(self.snr, 300)
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()

        #run
        HelperFactory.getTransactor(self.session).voucherPurchase(self.snr, 0)
        v = Transactor.getShopVoucher(p, self.shop)

      
        self.assertEqual(v.amount, 1)

    def testVoucherPurchasePass2(self):
        HelperFactory.getTransactor(self.session).purchase(self.snr, 300)
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        amount = Transactor.getShopVoucher(p, self.shop).amount

        #run
        HelperFactory.getTransactor(self.session).voucherPurchase(self.snr, amount)
        v = Transactor.getShopVoucher(p, self.shop)

        transaction = self.session.query(Transaction).filter_by(srcId=p.safe.id, destId=self.shop.safe.id, amount=0).order_by(Transaction.timestamp).first()

        #assert
        self.assertEquals(0, transaction.amount)
        self.assertEquals(- amount, transaction.voucherAmount)
        self.assertEqual(v.amount, 0)

    def testVoucherPurchaseFail(self):
        HelperFactory.getTransactor(self.session).purchase(self.snr, 300)
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()

        v = Transactor.getShopVoucher(p, self.shop)
        amount = v.amount +1

        self.assertRaises(InsufficientVoucherException, HelperFactory.getTransactor(self.session).voucherPurchase, self.snr, amount)


    def testStatusClean(self):
       

        transactor = HelperFactory.getTransactor(self.session)
        result = transactor.getStatus()

        self.assertEqual(result["outcoins"], 5000)
        self.assertEqual(result["incoins"], 300)
        self.assertEqual(result["invouchers"], 0)
        self.assertEqual(result["outvouchers"], 1)

        self.assertEqual(result["outcoins_today"], 5000)
        self.assertEqual(result["incoins_today"], 300)
        self.assertEqual(result["invouchers_today"], 0)
        self.assertEqual(result["outvouchers_today"], 1)

        self.assertEqual(result["total_vouchers"], 1)
        self.assertEqual(result["total_coins"],-3)
        self.assertEqual(result["total_fees"],3)
        self.assertEqual(result["voucher_status"], True)




    def testStatus(self):
        transactor = HelperFactory.getTransactor(self.session)
        transactor.purchase(self.snr, 300)
        transactor.insertCoins(self.snr, 5000)
        transactor.voucherPurchase(self.snr, 1)
        
        transactor.purchase(self.snr, 900)
        transactor.voucherPurchase(self.snr, 2)
        transactor.insertCoins(self.snr, 4000)


        justbeforetoday = datetime.datetime.combine(datetime.datetime.utcnow().date() - datetime.timedelta(days=1), datetime.time(23,59,59))
        t1 = self.session.query(Transaction).filter_by(amount=900).first()
        t1.timestamp = justbeforetoday
        t2 = self.session.query(Transaction).filter_by(voucherAmount=-2).first()
        t2.timestamp = justbeforetoday
        t3 = self.session.query(Transaction).filter_by(amount=4000).first()
        t3.timestamp = justbeforetoday
        
        self.session.commit()



        result = transactor.getStatus()

        self.assertEqual(result["outcoins"], 14000)
        self.assertEqual(result["incoins"], 1500)
        self.assertEqual(result["invouchers"], 3)
        self.assertEqual(result["outvouchers"], 5)

        self.assertEqual(result["outcoins_today"], 10000)
        self.assertEqual(result["incoins_today"], 600)
        self.assertEqual(result["invouchers_today"], 1)
        self.assertEqual(result["outvouchers_today"], 2)

        self.assertEqual(result["total_vouchers"], 2)
        self.assertEqual(result["total_coins"],-15)
        self.assertEqual(result["total_fees"],15)
        self.assertEqual(result["voucher_status"], True)



if __name__ == "__main__":
    unittest.main() # run all tests

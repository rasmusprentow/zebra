#test_basethread.py

import sys

sys.path.append('.')

from red.services.base import Service
import unittest
import zmq

from threading import Thread


import logging, logging.config
logging.config.fileConfig('config/logging.conf')

class TestThread (Service, Thread ):
    def processMessage(self, _):
        raise Exception("This should not be called, process message in Service might be broken")


TEST_SOCKET_NAME = "inproc://network_sock"
 
class Test_ServiceTestCase(unittest.TestCase):
 
    def setUp(self):
        self.context = zmq.Context()
        self.timeout = 2; 
       
        pass
 
    def tearDown(self):
        """Call after every test case."""
        self.context.term();
        pass
 
    def testServiceStartsAndRepliesOnEcho(self):
        
        thread  =  TestThread(TEST_SOCKET_NAME,self.context)
        thread.start()
        socket = self.context.socket(zmq.PAIR)
        socket.bind(TEST_SOCKET_NAME)
        socket.send_json({"head" : "system_message", "data" : "echo"})


        p = zmq.Poller()
        p.register(socket, zmq.POLLIN)

        msgs = dict(p.poll(self.timeout)) 
        if socket in msgs:
            msg = socket.recv_json()
            self.assertEqual(msg["data"], "echo")
        else:
            assert false
          
          
if __name__ == "__main__":
    unittest.main() # run all tests
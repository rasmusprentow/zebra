#test_authenticator.py

import sys
sys.path.append('.')


import unittest, os
from red.config import config

try:
    config.add_section("Db")
except:
    pass
config.set('Db','url','sqlite://')

from sqlalchemy.orm import sessionmaker
from models.model import engine, Pocket, Safe, Location, Transaction

from helpers.helperFactory import HelperFactory
from excep.excep import SerialNotFoundException,PocketAlreadyRegistered,InvalidSerialException
from models.testdata import createTestData, clearTestData
from helpers.authenticator import Authenticator

class Test_Authenticator(unittest.TestCase):
 
    def setUp(self):
        """Call before every test case."""
        self.session = sessionmaker(bind=engine)()
        self.authenticator = HelperFactory.getAuthenticator(self.session)
        self.snr = '270bdee4'
        clearTestData()
        createTestData()    
    
        
    def tearDown(self):
        """Call after every test case."""
        
        pass
 
    def testValidatePinPass(self):

        #""" setup """
        attemptedPin = '1234'

        #""" run """
        result = self.authenticator.validatePin(pin=attemptedPin, snr=self.snr)
 
        #""" Assert """
        self.assertTrue(result)

    def testValidatePinInvalidPin(self):

        #""" setup """
        attemptedPin1 = 'aaaa'
        attemptedPin2 = 0
        attemptedPin3 = dict()
        attemptedPin4 = lambda x: x

        #""" run """
        result1 = self.authenticator.validatePin(pin=attemptedPin1, snr=self.snr)
        result2 = self.authenticator.validatePin(pin=attemptedPin2, snr=self.snr)
        result3 = self.authenticator.validatePin(pin=attemptedPin3, snr=self.snr)
        result4 = self.authenticator.validatePin(pin=attemptedPin4, snr=self.snr)

        #""" assert """
        self.assertTrue(result1 is not None)
        self.assertTrue(result2 is not None)
        self.assertTrue(result3 is not None)
        self.assertTrue(result4 is not None)

        self.assertFalse(result1)
        self.assertFalse(result2)
        self.assertFalse(result3)
        self.assertFalse(result4)

    def testValidatePinInvalidSerial(self):
        
        #""" setup """
        attemptedPin = '1234'
        attemptedSerial = "11111111"

        self.assertRaises(SerialNotFoundException, self.authenticator.validatePin, attemptedSerial, attemptedPin)

    def testGeneratePin(self):

        result1 = Authenticator.generateRandomPin()
        result2 = Authenticator.generateRandomPin()
        result3 = Authenticator.generateRandomPin()
        result4 = Authenticator.generateRandomPin()
        result5 = Authenticator.generateRandomPin()
        result6 = Authenticator.generateRandomPin()
        result7 = Authenticator.generateRandomPin()
        result8 = Authenticator.generateRandomPin()
        result9 = Authenticator.generateRandomPin()
        result10 = Authenticator.generateRandomPin()


        self.assertTrue(len(result1) == 4)
        self.assertTrue(len(result2) == 4)
        self.assertTrue(len(result3) == 4)
        self.assertTrue(len(result4) == 4)
        self.assertTrue(len(result5) == 4)
        self.assertTrue(len(result6) == 4)
        self.assertTrue(len(result7) == 4)
        self.assertTrue(len(result8) == 4)
        self.assertTrue(len(result9) == 4)
        self.assertTrue(len(result10) == 4)

    def testRegisterPocket(self):

        snr = "99999999"
        pin = self.authenticator.registerPocket(snr)
        result1 = self.authenticator.validatePin(pin=pin, snr=snr)
        self.assertTrue(result1)

    def testRegisterPocketPredefinedPin(self):
        pin  = "1234"
        snr = "99999999"
        res = self.authenticator.registerPocket(snr, pin)
        result1 = self.authenticator.validatePin(pin=pin, snr=snr)
        self.assertTrue(result1)
        self.assertEqual(pin, res)

    

    def testRegisterPocketFail2(self):

        snr = "99999999"
        pin = self.authenticator.registerPocket(snr)     
        self.assertRaises(PocketAlreadyRegistered, self.authenticator.registerPocket, snr)


    def testRegisterPocketFails(self):

        snr = "99999999"
        pin = self.authenticator.registerPocket(snr)
        fail_pin = pin
        while pin == fail_pin:
            fail_pin = Authenticator.generateRandomPin()
        
        result1 = self.authenticator.validatePin(pin=fail_pin, snr=snr)
        self.assertFalse(result1)


    def testReRegisterPocket(self):
        snr = "99999999"
        
        self.authenticator.registerPocket(snr)
        pocket = self.authenticator._getPocket(snr)
        safeId = pocket.safeId

        newsnr = "98989898"
        self.authenticator.reRegisterPocket(snr, newsnr)
        
       
        safe = self.session.query(Safe).filter_by(id=safeId).first()
        pocketold = self.session.query(Pocket).filter_by(snr=snr).first()
        pocketnew = self.session.query(Pocket).filter_by(snr=newsnr).first()
        transaction = self.session.query(Transaction).filter_by(note="reregister:99999999").first()

        self.assertTrue(isinstance( transaction, Transaction))
        self.assertEqual(pocket, pocketnew)  # The new pocket should be matching the original
        self.assertEqual(pocketnew.safe, safe) # New should have olds pocket
        self.assertEqual(None, pocketold)  # Old should be nonexisting
        

    def testReRegisterPocketFailNewExisted(self):
        snr = "99999999"
        newsnr = "98989898"
        
        self.authenticator.registerPocket(snr)
        self.authenticator.registerPocket(newsnr)
     

        self.assertRaises(PocketAlreadyRegistered, self.authenticator.reRegisterPocket,snr, newsnr)
    

    def testReRegisterPocketFailOldDidNotExist(self):
        snr = "99999999"
        newsnr = "98989898"
        



        self.assertRaises(SerialNotFoundException, self.authenticator.reRegisterPocket,snr, newsnr)
        
     
    def testReRegisterExistinwasXashier(self):
        snr = "aaaaaaaa"
        newsnr = "98989898"
        

        self.assertRaises(InvalidSerialException, self.authenticator.reRegisterPocket,snr, newsnr)
           
       

    def testDecsnrToSnr(self):
        self.assertEqual( self.authenticator.decsnrToSnr("4294967295"),"ffffffff")
        self.assertEqual( self.authenticator.decsnrToSnr("0000000000"),"00000000")
        self.assertEqual( self.authenticator.decsnrToSnr("2711724449"),"a1a1a1a1")
        self.assertEqual( self.authenticator.decsnrToSnr("0010592673"),"00a1a1a1")
        self.assertEqual( self.authenticator.decsnrToSnr("10592673"),"00a1a1a1")


    def testSnrToDecsnr(self):
        self.assertEqual("4294967295", self.authenticator.snrToDecsnr("ffffffff"))
        self.assertEqual("0000000000", self.authenticator.snrToDecsnr("00000000"))
        self.assertEqual("2711724449", self.authenticator.snrToDecsnr("a1a1a1a1"))
        self.assertEqual("0010592673", self.authenticator.snrToDecsnr("00a1a1a1"))

    def testValidateCashierPocket_Pass(self):
        result = self.authenticator.validateCashierPocket("aaaaaaaa")
        self.assertTrue(result)

    def testValidateCashierPocket_Fail(self):
        result = self.authenticator.validateCashierPocket("12345678")
        self.assertFalse(result)
    
    def testValidateCashierPocket_Garbage(self):
        result = self.authenticator.validateCashierPocket("dsdfsfsfds")
        self.assertFalse(result)
        #self.assertRaises(SerialNotFoundException,self.authenticator.validateCashierPocket, "1dsfsdfsfds")
  

if __name__ == "__main__":
    unittest.main() # run all tests




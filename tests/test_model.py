#test_model.py

import unittest
import os


import logging, logging.config
logging.config.fileConfig('config/logging.conf')

from red.config import config

try:
    config.add_section("Db")
except:
    pass


config.set('Db','url','sqlite://')
from models.testdata import createTestData, clearTestData
from models.model import engine, Pocket, PocketType, Safe
from sqlalchemy.orm import sessionmaker



class Test_SimpleTestCase(unittest.TestCase):

    def setUp(self):
        self.session = sessionmaker(bind=engine)()
        clearTestData()
        createTestData()    
        
 
    def tearDown(self):
        """Call after every test case."""
        
        self.session.close()
        pass
    
    def testInsert(self):
        customer = self.session.query(PocketType).filter_by(name='customer').first()
        safe = Safe(10000)
        self.session.add(safe)
        self.session.commit()
        p = Pocket(safe, '98765432','1234', customer)
        self.session.add(p)
        self.session.commit()
        pdb = self.session.query(Pocket).filter_by(snr='98765432').first()

        self.assertEqual(p.snr, pdb.snr)
        self.assertEqual(p.safe.amount, pdb.safe.amount)
        self.assertEqual(p.pocketType, pdb.pocketType)

    def testUpdate(self):
        customer = self.session.query(PocketType).filter_by(name='customer').first()
        self.session.query(Pocket).filter_by(snr='270bdee4').update({'pocketTypeId': customer.id})
        pdb = self.session.query(Pocket).filter_by(snr='270bdee4').first()

        self.assertEqual(pdb.pocketType, customer)

    def testDelete(self):
        self.session.query(Pocket).filter_by(snr='270bdee4').delete()
        pdb = self.session.query(Pocket).filter_by(snr='270bdee4').first()

        self.assertTrue(pdb is None)
        

if __name__ == "__main__":
    unittest.main() # run all tests
#test_cashier_adddiscount.py



import sys

sys.path.append('.')


from activities.cashier.addDiscount import Adddiscount
import logging, logging.config
from models.testdata import createTestData, clearTestData
from red.tests.baseActivityTest import BaseActivityTest
logging.config.fileConfig('config/logging.conf')
import configparser
from red.config import config
from red.kernel import Kernel
from excep.excep import InsufficientCoinException, SerialNotFoundException
from helpers.transactor import Transactor 

from models.model import engine, Pocket, Safe, Shop, Transaction



class Test_Cashier_adddiscoutn(BaseActivityTest):
 
    def setUp(self):
        super(Test_Cashier_adddiscoutn, self).setup()

        self.setServices(["customer", "display","lpc"])
        self.activity = self.getActivity(Adddiscount)
        self.activity.returnActivity = "management"
        try:
            config.add_section('Products')
        except:
            pass

        try:
            config.add_section('Shop')
        except:
            pass
        config.set('Shop','peepoobag', '4000')
        config.set('Shop','fee_percentage', '4000')
        config.set('Shop','name', 'enviclean')
        config.set('Shop','location','onestop1')
 
        try:
            config.add_section('Timeouts')
        except:
            pass
        config.set('Timeouts', 'timeout', '0')


         


    def tearDown(self):
        super(Test_Cashier_adddiscoutn, self).tearDown()
    
    def testOnCreateFail1(self):
        data = {"snr" : "8888", "product" : "peepoobag"}
        self.assertRaises(ValueError, self.activity.onCreate,data)
    
    def testOnCreateFail2(self):
        data = None
        self.assertRaises(ValueError, self.activity.onCreate,data)
    
    def testOnCreateFail3(self):
        data = {"amount" : "aa1aa"} 
        self.assertRaises(ValueError, self.activity.onCreate,data)
       
    def testOnCreatePass(self):
        data = {"amount" : "123456"}
        self.activity.onCreate(data)
        self.activity.onPocket("aaaaaaaa")
        self.assertSwitchActivity(None)
        self.assertReceived("customer", {'head' : 'add_discount', 'data' : 123456})

    def testReceiveCustomerMessageFail1(self):
        self.activity.amount = 1000000
        self.activity.currency = "discount"
        data = {'head' : 'pocket_received', 'data' : ''}
        self.activity.onPocket("aaaaaaaa")
        self.activity.receiveCustomerMessage(data)
        self.assertSetLayout("common/error")

    def testReceiveCustomerMessage(self):
        clearTestData()
        createTestData()
        data = {'head' : 'pocket_received', 'data' : '3429dee4'}
        self.activity.amount = 1000000
        self.activity.currency = "discount"
        self.activity.onPocket("aaaaaaaa")
        self.activity.receiveCustomerMessage(data)
        self.assertSetLayout("common/success")


    
  
        pocket = self.activity.session.query(Pocket).filter_by(snr="12345678").first()
      
        shopName = config.get('Shop', 'name')
        shop = self.activity.session.query(Shop).filter_by(name=shopName).first()
        
        voucher = Transactor.getShopVoucher(pocket, shop)
        transaction = self.activity.session.query(Transaction).filter_by(note="aaaaaaaa").first()
        
        self.assertEqual(voucher.amount,100 )
        self.assertTrue(isinstance(transaction, Transaction)) 

        self.assertEqual(transaction.fee, 0)
        self.assertEqual(transaction.amount, 0)
        self.assertEqual(transaction.voucherAmount, 100)
        


if __name__ == "__main__":
    unittest.main() # run all tests
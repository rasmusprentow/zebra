

from services.intercom import Intercom
import unittest
import logging
import json
from sqlalchemy.orm import sessionmaker
from models.model import engine
from models.testdata import createTestData, clearTestData
from config.strings import * 
from red.config import config
logging.config.fileConfig('config/logging.conf') 

class Test_IntercomTestCase(unittest.TestCase):
 
    def setUp(self):
        """Call before every test case."""
        clearTestData()
        createTestData()
        try:
            config.add_section('Shop')
        except:
            pass

        config.set('Shop','voucher_percentage','0.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')
        config.set('Shop','location','onestop1')
 
        self.intercom = Intercom('inproc://something')
        self.intercom.session = sessionmaker(bind=engine)()

        pass
 
    def tearDown(self):
        """Call after every test case."""
        pass
 
    def testGenerateResponseValidatePinPass(self):
        msg = {'head' : 'validatePin', 'data' : {'pin' : '1234', 'snr' : '12345678'}}
        result = self.intercom.generateResponse(msg)
        expected = {'head' : 'ok', 'data' : True}
        self.assertEqual(expected, result)

    def testGenerateResponseValidatePinFail(self):
        msg = {'head' : 'validatePin', 'data' : {'pin' : '4444', 'snr' : '12345678'}}
        result = self.intercom.generateResponse(msg)
        expected = {'head' : 'ok', 'data' : False}
        self.assertEqual(expected, result) 

    def testGenerateResponsePurchasePass(self):
        msg = {'head' : 'purchase', 'data' : {'snr' : '12345678', 'amount' : 300}}
        result = self.intercom.generateResponse(msg)
        expected = {'head' : 'ok', 'data' : {'amount' : 300, 'voucherAmount' : 1}}
        self.assertEqual(expected, result) 

    def testGenerateResponsePurchaseFail(self):
        msg = {'head' : 'purchase', 'data' : {'snr' : '88888888', 'amount' : 300}}
        result = self.intercom.generateResponse(msg)
        result['data']['message'] = "TEST"
        expected = {'head' : 'error', 'data' : {'exception' : 'SerialNotFoundException', 'message' : "TEST"}}
        self.assertEqual(expected, result) 

if __name__ == "__main__":
    unittest.main() # run all tests

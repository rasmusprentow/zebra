

import unittest
import logging, math
from sqlalchemy.orm import sessionmaker
from models.testdata import createTestData, clearTestData
from models.model import engine, Pocket, Safe, Shop, Transaction
from helpers.synchronizer import Synchronizer
from helpers.transactor import Transactor
from excep.excep import SerialNotFoundException
import datetime
logging.config.fileConfig('config/logging.conf') 

class Test_Synchronizor(unittest.TestCase):
 
    def setUp(self):
        self.snr = '12345678'
        self.shopName = 'enviclean'
        self.fee = 3       
        self.amount = 300 
        self.timestamp = '2014-04-17 09:16:12.378366'
        self.data = {
            'head' : 'transaction',
            'data' : {
                'timestamp': self.timestamp,
                'srcOwn' : {
                    'type' : 'pocket',
                    'id' : self.snr,
                    'pin' : '1234'},
                'destOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : self.amount,
                'voucherAmount' : 100,
                'fee' : self.fee,
                'location' : 'onestop1',
                'shop' : 'enviclean'
                }
            }

        self.dataInsertCoins = {
            'head' : 'transaction',
            'data' : {
                'timestamp': self.timestamp,
                'destOwn' : {
                    'type' : 'pocket',
                    'id' : self.snr,
                    'pin' : '1234'},
                'srcOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : self.amount,
                'voucherAmount' : 0,
                'fee' : 0,
                'location' : 'onestop1',
                'shop' : 'enviclean'
                }
            }

        self.dataInsertCoinsNewPocket= {
            'head' : 'transaction',
            'data' : {
                'timestamp': self.timestamp,
                'destOwn' : {
                    'type' : 'pocket',
                    'id' : '88888888',
                    'pin' : '1234'},
                'srcOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : self.amount,
                'voucherAmount' : 0,
                'fee' : 0,
                'location' : 'onestop1',
                'shop' : 'enviclean'
                }
            }

        self.session = sessionmaker(bind=engine)()
       
        clearTestData()
        createTestData()


 
    def tearDown(self):
        """Call after every test case."""
        pass
 
    def testPass(self):
        """Test case A. note that all test method names must begin with 'test.'"""
        assert True 

    def  testReRegister(self):
        


        dataInsertCoinsNewPocket= {
            'head' : 'transaction',
            'data' : {
                'timestamp': '2014-04-18 09:16:12.378366',
                'destOwn' : {
                    'type' : 'pocket',
                    'id' :  '22222222',
                    'pin' : '1234'},
                'srcOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : 0,
                'voucherAmount' : 0,
                'fee' : 0,
                'location' : 'onestop1',
                'shop' : 'enviclean',
                'note' : 'reregister:'+self.snr
                }
            }



        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        s = self.session.query(Shop).filter_by(name=self.shopName).first()
    
        oldAmount = p.safe.amount

        oldShopAmount = s.safe.amount
       

        Synchronizer(self.session).processTransaction(dataInsertCoinsNewPocket['data'])
        p = self.session.query(Pocket).filter_by(snr="22222222").first()
        self.assertTrue(isinstance(p, Pocket)) 
        
        # self.assertRaises(SerialNotFoundException, Synchronizer(self.session).processTransaction,self.data['data'])
        
        
        Synchronizer(self.session).processTransaction(self.data['data'])
        # expectedShopAmount = (oldShopAmount + self.amount - self.fee)
        
        # transaction = self.session.query(Transaction).filter_by(srcId=p.safe.id, destId=s.safe.id).order_by(Transaction.timestamp).first()
        # #p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        # #s = self.session.query(Shop).filter_by(name=self.shopName).first()
    
        # """
        # asserts
        # """
        # self.assertTrue(isinstance(transaction, Transaction)) 
        # self.assertEqual((oldAmount - self.amount), p.safe.amount) 
        # self.assertEqual(transaction.fee, self.fee), 'Expected: ' + str(self.fee) + ' got: ' + str(transaction.fee)
        # self.assertEqual(expectedShopAmount, transaction.dest.amount), 'Expected: ' + str(expectedShopAmount) + ' got: ' + str(transaction.dest.amount)
        # self.assertEqual(transaction.voucherAmount, 100)
        # self.assertEqual(transaction.timestamp, self.timestamp)
        # self.assertEqual(oldAmount + oldShopAmount, s.safe.amount + p.safe.amount + transaction.fee)



    def testProcessTransactionPass(self):
        
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        s = self.session.query(Shop).filter_by(name=self.shopName).first()
    
        oldAmount = p.safe.amount

        oldShopAmount = s.safe.amount
       

        Synchronizer(self.session).processTransaction(self.data['data'])
    

        expectedShopAmount = (oldShopAmount + self.amount - self.fee)
        
        transaction = self.session.query(Transaction).filter_by(srcId=p.safe.id, destId=s.safe.id).order_by(Transaction.timestamp).first()
        #p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        #s = self.session.query(Shop).filter_by(name=self.shopName).first()
    
        """
        asserts
        """
        self.assertTrue(isinstance(transaction, Transaction)) 
        self.assertEqual((oldAmount - self.amount), p.safe.amount) 
        self.assertEqual(transaction.fee, self.fee), 'Expected: ' + str(self.fee) + ' got: ' + str(transaction.fee)
        self.assertEqual(expectedShopAmount, transaction.dest.amount), 'Expected: ' + str(expectedShopAmount) + ' got: ' + str(transaction.dest.amount)
        self.assertEqual(transaction.voucherAmount, 100)
        self.assertEqual(transaction.timestamp, self.timestamp)
        self.assertEqual(oldAmount + oldShopAmount, s.safe.amount + p.safe.amount + transaction.fee)

        envicleanVoucher = [x for x in p.vouchers if lambda x: x.shopId == s.id].pop()
        self.assertEqual(envicleanVoucher.amount, 200)




    def testInsertCoins(self):
      
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        oldAmount = p.safe.amount
       

      
        s = self.session.query(Shop).filter_by(name=self.shopName).first()
        oldShopAmount = s.safe.amount

        Synchronizer(self.session).processTransaction(self.dataInsertCoins['data'])
    

        transaction = self.session.query(Transaction).filter_by(srcId=s.safe.id, destId=p.safe.id).order_by(Transaction.timestamp).first()

        self.assertEqual(p.safe.amount, oldAmount + self.amount)
        self.assertEqual(s.safe.amount, oldShopAmount - self.amount)
        self.assertEqual(transaction.timestamp, self.timestamp)
        self.assertEqual(transaction.amount, self.amount)
        self.assertEqual(oldAmount + oldShopAmount, p.safe.amount + s.safe.amount)

    def testInsertCoinsNewPocket(self):
        Synchronizer(self.session).processTransaction(self.dataInsertCoinsNewPocket['data'])
        p = self.session.query(Pocket).filter_by(snr='88888888').first()
        self.assertEqual('1234', p.pin)


    def testInsertCoinsTwice(self):
        """ Attempts to insert same transaction twice. Should not be a problem, and should not conduct same amount """
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        oldAmount = p.safe.amount
       

      
        s = self.session.query(Shop).filter_by(name=self.shopName).first()
        oldShopAmount = s.safe.amount

        Synchronizer(self.session).processTransaction(self.dataInsertCoins['data'])
        Synchronizer(self.session).processTransaction(self.dataInsertCoins['data'])
    

        transaction = self.session.query(Transaction).filter_by(srcId=s.safe.id, destId=p.safe.id).order_by(Transaction.timestamp).first()

        self.assertEqual(p.safe.amount, oldAmount + self.amount)
        self.assertEqual(s.safe.amount, oldShopAmount - self.amount)
        self.assertEqual(transaction.timestamp, self.timestamp)
        self.assertEqual(transaction.amount, self.amount)
        self.assertEqual(oldAmount + oldShopAmount, p.safe.amount + s.safe.amount)
    
    def testVoucherPurchasePass(self):
        Synchronizer(self.session).processTransaction(self.data['data'])
        
        p = self.session.query(Pocket).filter_by(snr=self.snr).first()
        s = self.session.query(Shop).filter_by(name=self.shopName).first()
    
        amount = 100
        dataVoucherPurchase = {
            'head' : 'transaction',
            'data' : {
                'timestamp': datetime.datetime.utcnow(),
                'srcOwn' : {
                    'type' : 'pocket',
                    'id' : self.snr,
                    'pin' : '1234'},
                'destOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : 0,
                'voucherAmount' : - amount,
                'fee' : 0,
                'location' : 'onestop1',
                'shop' : 'enviclean1'
                }
            }
        #run
        Synchronizer(self.session).processTransaction(dataVoucherPurchase['data'])
        
        v = Transactor.getShopVoucher(p, s)

        transaction = self.session.query(Transaction).filter_by(srcId=p.safe.id, destId=s.safe.id, amount=0).order_by(Transaction.timestamp).first()

        #assert
        self.assertEquals(0, transaction.amount)
        self.assertEquals(- amount, transaction.voucherAmount)
        self.assertEqual(v.amount, 100) # Now he owes. 

    def testVoucherPurchasePassButHighAmount(self):
        Synchronizer(self.session).processTransaction(self.data['data'])
        
        p = self.session.query(Pocket).filter_by(snr="aaaaaaaa").first()
        s = self.session.query(Shop).filter_by(name=self.shopName).first()
    
        amount = 100
        dataVoucherPurchase = {
            'head' : 'transaction',
            'data' : {
                'timestamp': datetime.datetime.utcnow(),
                'srcOwn' : {
                    'type' : 'pocket',
                    'id' : "aaaaaaaa",
                    'pin' : '1234'},
                'destOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : 0,
                'voucherAmount' : - amount,
                'fee' : 0,
                'location' : 'onestop1',
                'shop' : 'enviclean1'
                }
            }
        #run
        Synchronizer(self.session).processTransaction(dataVoucherPurchase['data'])
        
        v = Transactor.getShopVoucher(p, s)

        transaction = self.session.query(Transaction).filter_by(srcId=p.safe.id, destId=s.safe.id, amount=0).order_by(Transaction.timestamp).first()

        #assert
        self.assertEquals(0, transaction.amount)
        self.assertEquals(- amount, transaction.voucherAmount)
        self.assertEqual(v.amount, -100)

    




    def testGetData(self):
        ts1 = '2014-03-17 09:16:12.378366'
        ts2 = '2014-03-18 09:16:12.378366'
        ts3 = '2014-03-19 09:16:12.378366'

        t1 = '2014-04-17 09:16:12.378366'
        t2 = '2014-04-18 09:16:12.378366'
        t3 = '2014-04-19 09:16:12.378366'
        td1 =  {
                'timestamp': ts1,
                'srcOwn' : {
                    'type' : 'pocket',
                    'id' : self.snr,
                    'pin' : '1234'},
                'destOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : self.amount,
                'voucherAmount' : 100,
                'fee' : self.fee,
                'location' : 'onestop1',
                'shop' : 'enviclean',
                'note' : "something"


                }
        td2 =  {
                'timestamp': ts2,
                'srcOwn' : {
                    'type' : 'pocket',
                    'id' : self.snr,
                    'pin' : '1234'},

                'destOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : self.amount,
                'voucherAmount' : 100,
                'fee' : self.fee,
                'location' : 'onestop1',
                'shop' : 'enviclean',

                'note' : "something"


                }

        td3 = {
                'timestamp': ts3,
                'srcOwn' : {
                    'type' : 'pocket',
                    'id' : self.snr,
                    'pin' : '1234'},
                'destOwn' : {
                    'type' : 'shop',
                    'id' : self.shopName,
                    'pin' : None},
                'amount' : self.amount,
                'voucherAmount' : 100,
                'fee' : self.fee,
                'location' : 'onestop1',
                'shop' : 'enviclean',
                'note' : "something"

                } 
        data1 = {
            'head' : 'transaction',
            'data' : td1
            }

        data2 = {
            'head' : 'transaction',
            'data' : td2
            }

        data3 = {
            'head' : 'transaction',
            'data' : td3
            }

        Synchronizer(self.session).processTransaction(data1['data'])
        Synchronizer(self.session).processTransaction(data2['data'])
        Synchronizer(self.session).processTransaction(data3['data']) 

        tr1 = self.session.query(Transaction).filter_by(timestamp=ts1).order_by(Transaction.timestamp).first()
        tr2 = self.session.query(Transaction).filter_by(timestamp=ts2).order_by(Transaction.timestamp).first()
        tr3 = self.session.query(Transaction).filter_by(timestamp=ts3).order_by(Transaction.timestamp).first()
        tr1.synced = t1
        tr2.synced = t2
        tr3.synced = t3
        self.maxDiff = None
        self.session.commit()

        result = Synchronizer(self.session).getData(t2)
        expected =  {'head' : 'list_of_transactions', 'data' : [ td2, td3]}
      
        self.assertEqual(expected, result)

if __name__ == "__main__":
    unittest.main() # run all tests

#test_cashier_main.py


import sys

sys.path.append('.')

import unittest
import zmq

from threading import Thread

from activities.cashier.balance import Balance 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from helpers.helperFactory import HelperFactory
from excep.excep import InsufficientCoinException, SerialNotFoundException

from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')

from red.config import config
from red.kernel import Kernel

from config.strings import *



class Test_CashierBalance(BaseActivityTest):
 
    def setUp(self):
        super(Test_CashierBalance, self).setup()
    
        clearTestData()
        createTestData()
        self.setServices(["customer", "display"])
        try:
            config.add_section('Shop')
        except:
            pass

     
        config.set('Shop','location','onestop1')
        config.set('Shop','voucher_percentage','33.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')

        self.activity = self.getActivity(Balance)
        
        
    def tearDown(self):
        super(Test_CashierBalance, self).tearDown()
    



    def testReceiveCustomerMessage(self):
        self.activity.onCreate()
        data = {'head' : 'get_balance', 'data' : {'snr' : '88888888'}}
        self.activity.receiveCustomerMessage(data)
        self.assertReceived("customer", {'head' : 'error', 'data' : INVALID_POCKET})

        data = {'head' : 'validate_pocket', 'data' : {'snr' : '88888888'}}
        self.activity.receiveCustomerMessage(data)
        self.assertReceived("customer", {'head' : 'error', 'data' : INVALID_POCKET})


        data = {'head' : 'get_balance', 'data' : {'snr' : '12345678'}}
        self.activity.receiveCustomerMessage(data)
        self.assertReceived("customer", {'head': 'balance', 'data': {'amount': 2700,  'vamount': 100}})
       
      

    def testShowBalancePass(self):
        HelperFactory.getTransactor(self.kernel.session).purchase("12345678", 300)
        HelperFactory.getTransactor(self.kernel.session).purchase("12345678", 300)
        HelperFactory.getTransactor(self.kernel.session).purchase("12345678", 300)
   
        self.activity._showBalance("12345678")
        self.assertReceived("customer", {'head': 'balance', 'data': {'amount': 2700 - 300 * 3,  'vamount': 100}})
       
    def testShowBalanceFail(self):
    
        self.activity._showBalance("12345678")
        self.assertReceived("customer", {'head': 'balance', 'data': {'amount': 2700,  'vamount': 100}})

    def testShowBalanceFail2(self):
    
        self.activity._showBalance("8888888")
        self.assertReceived("customer", {'head': 'error', 'data': INVALID_POCKET})
       
 

     
          
if __name__ == "__main__":
    unittest.main() # run all tests
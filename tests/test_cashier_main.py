#test_cashier_main.py


import sys

sys.path.append('.')

import unittest
import zmq

from threading import Thread

from activities.cashier.main import Main 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from helpers.helperFactory import HelperFactory
from excep.excep import InsufficientCoinException, SerialNotFoundException

from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')

from red.config import config
from red.kernel import Kernel

from config.strings import *



class Test_CashierMain(BaseActivityTest):
 
    def setUp(self):
        super(Test_CashierMain, self).setup()
    
        clearTestData()
        createTestData()
        self.setServices(["customer", "display"])
        try:
            config.add_section('Shop')
        except:
            pass

     
        config.set('Shop','name','enviclean')

        config.set('Shop','voucher_percentage','33.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')
        config.set('Shop','location','onestop1')
 
        self.activity = self.getActivity(Main)
         

    def tearDown(self):
        super(Test_CashierMain, self).tearDown()
    

    def testReceiveDisplayMessage(self):
        data = {"head" : "button_clicked", "data" : "purchase"}
        self.activity.receiveDisplayMessage(data)
        self.assertSwitchActivity("purchase")

        data = {"head" : "button_clicked", "data" : "insertCoins"}
        self.activity.receiveDisplayMessage(data)
        self.assertSwitchActivity("enterAmount", {'next_activity' : 'insertCoins'})

        data = {"head" : "button_clicked", "data" : "voucherPurchase"}
        self.activity.receiveDisplayMessage(data)
        self.assertSwitchActivity("enterAmount", {'next_activity' : 'voucherPurchase'})

    def testReceiveCustomerMessage(self):

        data = {'head' : 'checking_balance'}
        self.activity.receiveCustomerMessage(data)
        self.assertSwitchActivity("balance")


   

     
          
if __name__ == "__main__":
    unittest.main() # run all tests
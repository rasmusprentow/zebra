#test_st_authenticate.py

import sys

sys.path.append('.')


from activities.cashier.login import Login 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from excep.excep import InsufficientCoinException, SerialNotFoundException
from helpers.helperFactory import HelperFactory
from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')

from red.config import config
from red.kernel import Kernel





class Test_Cashier_Authenticate(BaseActivityTest):
 
    def setUp(self):
        super(Test_Cashier_Authenticate, self).setup()
    
        clearTestData()
        createTestData()
        
        self.setServices(["display", "lpc"])
        self.activity = self.getActivity(Login)
        self.activity.onCreate()
        self.snrValid = "aaaaaaaa"
        self.snrInvalid = "12345678"

    def tearDown(self):
        super(Test_Cashier_Authenticate, self).tearDown()

    def testOnPocket_ValidPocket(self):
        self.activity.onPocket(self.snrValid)
        self.assertSwitchActivity("main")
           
    def testOnPocket_InvalidPocket(self):
        self.activity.onPocket(self.snrInvalid)
        self.assertSwitchActivity("login")
        self.assertSetLayout("common/error")

    def testOnPocket_Garbage(self):
        self.activity.onPocket({})
        self.assertSwitchActivity("login")
        self.assertSetLayout("common/error")
     
    def testOnPocket_Garbage2(self):
        self.activity.onPocket("212")
        self.assertSwitchActivity("login") 
        self.assertSetLayout("common/error")
          

if __name__ == "__main__":
    unittest.main() # run all tests
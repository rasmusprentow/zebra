#test_st_authenticate.py

import sys

sys.path.append('.')


from activities.standAlone.authenticate import Authenticate 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from excep.excep import InsufficientCoinException, SerialNotFoundException
from helpers.helperFactory import HelperFactory
from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')

from red.config import config
from red.kernel import Kernel





class Test_St_Authenticate(BaseActivityTest):
 
    def setUp(self):
        super(Test_St_Authenticate, self).setup()
    
        clearTestData()
        createTestData()
        
        self.setServices(["display"])
        self.activity = self.getActivity(Authenticate)
        self.activity.notificationSleepTime = 0

        try:
            config.add_section('Timeouts')
        except:
            pass

        config.set('Timeouts','notification', '0')
        config.set('Timeouts','error', '0')
        self.data = {"snr" : "12345678", "product" : "peepoobag"}

    def tearDown(self):
        super(Test_St_Authenticate, self).tearDown()
    

    def testOnCreateFail(self):
        data = {"snr" : "88888888", "product" : "peepoobag"}
        self.activity.onCreate(data)
        self.assertSetLayout("common/error")
        self.assertSwitchActivity(anyActivity=True)

    def testOnCreatePass(self):
        self.activity.onCreate(self.data)
        self.assertSetLayout("common/authenticate")
        self.assertSwitchActivity(None)

    def testReceiveDisplayMessage(self):
        
        self.activity.onCreate(self.data)

        dataIn = {"head" : "button_clicked", "data" : "num2"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.pin, '2')

        dataIn = {"head" : "button_clicked", "data" : "num2"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.pin, '22')

        dataIn = {"head" : "button_clicked", "data" : "num4"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.pin, '224')

        dataIn = {"head" : "button_clicked", "data" : "num4"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.pin, '2244')

        dataIn = {"head" : "button_clicked", "data" : "num4"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.pin, '2244')

        dataIn = {"head" : "button_clicked", "data" : "cancel"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.pin, '')
        self.assertEqual(self.activity.pin_secret, '')

        dataIn = {"head" : "button_clicked", "data" : "cancel"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertSwitchActivity(anyActivity=True)
        
    def testReceiveDisplayMessageWrongPin(self):
        
        self.activity.onCreate(self.data)
        self.activity.pin = '9999'
        dataIn = {"head" : "button_clicked", "data" : "accept"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertSetLayout("common/error")
        self.assertSwitchActivity()

    def testReceiveDisplayMessageCorrectPin(self):
        
        self.activity.onCreate(self.data)
        self.activity.pin = '1234'
        dataIn = {"head" : "button_clicked", "data" : "accept"}
        self.activity.receiveDisplayMessage(dataIn)

        self.assertSwitchActivity("receipt")

           
    
    def testOnAccept(self):
        pass

    def testOnCancel(self):
        self.activity.onCreate(self.data)
        self.activity.receiveDisplayMessage({"head" : "button_clicked", "data": "cancel"})
        self.assertSwitchActivity("welcome")
         
     
          
if __name__ == "__main__":
    unittest.main() # run all tests
#test_cashier_enteramount.py



import sys

sys.path.append('.')


from activities.cashier.enterAmount import Enteramount 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from red.tests.baseActivityTest import BaseActivityTest
logging.config.fileConfig('config/logging.conf')
import configparser
from red.config import config
from red.kernel import Kernel





class Test_Cashier_Enteramount(BaseActivityTest):
 
    def setUp(self):
        super(Test_Cashier_Enteramount, self).setup()

        self.setServices(["customer", "display"])
        self.activity = self.getActivity(Enteramount)
      

        try:
            config.add_section('Products')
        except:
            pass

        config.set('Products','peepoobag', '4000')

        config.set('Shop','location','onestop1')
         

    def tearDown(self):
        super(Test_Cashier_Enteramount, self).tearDown()
    
    def testOnCreateFail1(self):
        data = {"snr" : "8888", "product" : "peepoobag"}
        self.assertRaises(ValueError, self.activity.onCreate,data)
    
    def testOnCreateFail2(self):
        data = None
        self.assertRaises(ValueError, self.activity.onCreate,data)
       
    def testOnCreatePass(self):
        data = {"next_activity" : "someactivity"}
        self.activity.onCreate(data)
        self.assertSwitchActivity(None)

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '1')  

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '11')  

        dataIn = {"head" : "button_clicked", "data" : "accept"}
        self.activity.receiveDisplayMessage(dataIn)
        
        self.assertSwitchActivity("someactivity",{'amount' : '11'})     

    def testReceiveDisplayMessageButtonsAndCancel(self):
        data = {"next_activity" : "someactivity"}
        self.activity.onCreate(data)
        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '1')  

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '11')  

        dataIn = {"head" : "button_clicked", "data" : "num6"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '116') 

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '1161')   

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '11611')  

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '11611')  

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '11611')  

        dataIn = {"head" : "button_clicked", "data" : "num1"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '11611')  

        dataIn = {"head" : "button_clicked", "data" : "backspace"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertEqual(self.activity.amount, '1161') 
      
        dataIn = {"head" : "button_clicked", "data" : "cancel"}
        self.activity.receiveDisplayMessage(dataIn)
        self.assertSwitchActivity(anyActivity=True)  

  
      


if __name__ == "__main__":
    unittest.main() # run all tests
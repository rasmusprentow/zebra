
#test_cashier_purchase.py


import sys

sys.path.append('.')

import unittest
import zmq

from threading import Thread

from activities.cashier.voucherPurchase import Voucherpurchase 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')

from helpers.helperFactory import HelperFactory
from red.config import config
from config.strings import *

from models.model import Pocket

class Test_CashierVoucherPurchaseTestCase(BaseActivityTest):
 
    def setUp(self):
        super(Test_CashierVoucherPurchaseTestCase, self).setup()
    
        
        self.setServices(["customer", "display"])
        self.activity = self.getActivity(Voucherpurchase)
        self.activity.notificationSleepTime = 0

        clearTestData()
        createTestData()
        try:
            config.add_section('Products')
        except:
            pass
       
        config.set('Products','product1', '4000')
        config.set('Products','product2', '300')
        
        try:
            config.add_section('Shop')
        except:
            pass

     
        config.set('Shop','name','enviclean')

        config.set('Shop','voucher_percentage','0.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')
        config.set('Shop','location','onestop1')
          

    def tearDown(self):
        super(Test_CashierVoucherPurchaseTestCase, self).tearDown()
    
    def testOnCancelClicked(self):
      
        self.activity.receiveCustomerMessage( {"head" : "button_clicked", "data" : "cancel"})
     
        self.assertSetLayout("common/error")
        self.assertSwitchActivity("main")
   


    def testReceivePocket(self):
        snr_valid = "12345678"
        message = {'head' : 'validate_pocket', 'data' : {'snr' : snr_valid}}
        self.activity.receiveCustomerMessage(message)
        self.assertReceived("customer", {'head' : 'valid_pocket', 'data' : {'snr' : snr_valid}})
       
        snr_invalid = "88888888"
        message = {'head' : 'validate_pocket', 'data' : {'snr' : snr_invalid}}
        self.activity.receiveCustomerMessage(message)
        self.assertReceived("customer", {'head' : 'error', 'data' : INVALID_POCKET})

    def testReceivePinInsufficientVoucher(self):
        snr_valid = "12345678"
        pin_valid = "1234"
        product = 'product2'
        amount = 300
        voucherAmount = 100

        self.activity.onCreate({'amount' : amount})

        message = {'head' : 'pin_received', 'data' : {'snr' : snr_valid, 'pin' : pin_valid}}
        self.activity.receiveCustomerMessage(message)
        self.assertReceived("customer", {'head' : 'error', 'data' : INSUFFICIENT_VOUCHER})

    def testReceivePinSufficientVoucher(self):
        snr_valid = "12345678"
        pin_valid = "1234"
        product = 'product2'
        amount = 300
        voucherAmount = 1

        transactor = HelperFactory.getTransactor(self.activity.session)
        transactor.purchase(snr_valid, amount)
        self.activity.onCreate({'amount' : voucherAmount})

        message = {'head' : 'pin_received', 'data' : {'snr' : snr_valid, 'pin' : pin_valid}}
        self.activity.receiveCustomerMessage(message)
        self.assertReceived("customer", {'head' : 'voucherReceipt', 'data' : {'amount' : voucherAmount, 'balance_vamount': 1}})
        self.assertSetLayout("common/success")

             
    def testReceivePinFail(self):
        snr_valid = "12345678"
        pin_invalid = "4444"
        message = {'head' : 'pin_received', 'data' : {'snr' : snr_valid, 'pin' : pin_invalid}}
        self.activity.receiveCustomerMessage(message)
        self.assertReceived("customer", {'head' : 'pin_error', 'data' : INVALID_PIN})
        
if __name__ == "__main__":
    unittest.main() # run all tests

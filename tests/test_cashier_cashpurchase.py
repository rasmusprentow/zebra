#test_st_authenticate.py

import sys

sys.path.append('.')


from activities.cashier.cashPurchase import Cashpurchase 
import logging, logging.config
from models.testdata import createTestData, clearTestData
from excep.excep import InsufficientCoinException, SerialNotFoundException
from helpers.helperFactory import HelperFactory
from red.tests.baseActivityTest import BaseActivityTest, MockSerivce
logging.config.fileConfig('config/logging.conf')
from models.model import engine, Voucher, Shop, Pocket, Safe, PocketType, ShopType, Transaction, Location

from red.config import config
from red.kernel import Kernel





class Test_Cashier_CashPurchase(BaseActivityTest):
 
    def setUp(self):
        super(Test_Cashier_CashPurchase, self).setup()
    
        clearTestData()
        createTestData()
        
        self.setServices(["display", "lpc", "customer"])
        self.activity = self.getActivity(Cashpurchase)
        self.activity.onCreate()
        
        try:
            config.add_section('Products')
        except:
            pass
       
        config.set('Products','product1', '4000')
        config.set('Products','product2', '300')
        
        try:
            config.add_section('Shop')
        except:
            pass

     
        config.set('Shop','name','enviclean')
        config.set('Shop','location','onestop1')
 
        config.set('Shop','voucher_percentage','33.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')

    def tearDown(self):
        super(Test_Cashier_CashPurchase, self).tearDown()

    def testReceiveDisplayMessageProduct1_Pass(self):
        self.activity.receiveDisplayMessage({'head':'button_clicked', 'data' : 'product1'})
        self.assertSetLayout("cashier/cashbuy")
        self.assertEqual(self.activity.amount, 4000)


    def testReceiveDisplayMessagePaid_Pass(self):
        
        #Prepare
        shopName = config.get('Shop', 'name')

        amount = 4000

        p = self.activity.session.query(Pocket).filter_by(snr="cash_buy").first()
        s = self.activity.session.query(Shop).filter_by(name=shopName).first()
        
        oldAmount = p.safe.amount
        oldShopAmount = s.safe.amount

        #Run
        self.activity.receiveDisplayMessage({'head':'button_clicked', 'data' : 'product1'})
        self.activity.receiveDisplayMessage({'head':'button_clicked', 'data' : 'paid'})


        #Test
        transaction = self.activity.session.query(Transaction).filter_by(srcId=p.safe.id, destId=s.safe.id).first()

        expectedShopAmount = oldShopAmount # No changes
        
        """
        asserts
        """
        self.assertTrue(isinstance(transaction, Transaction)) 
        self.assertEqual(0, p.safe.amount) 
        self.assertEqual(transaction.fee, 0)
        self.assertEqual(transaction.amount, amount)
        self.assertEqual(expectedShopAmount, transaction.dest.amount), 'Expected: ' + str(expectedShopAmount) + ' got: ' + str(transaction.dest.amount)
        self.assertEqual(transaction.voucherAmount,0)
        
        self.assertEqual(oldAmount + oldShopAmount, s.safe.amount + p.safe.amount + 0)

  
          

if __name__ == "__main__":
    unittest.main() # run all tests
#test_transactor.py

import unittest
from red.config import config
import math, os, datetime
from sqlalchemy.orm import sessionmaker
from models.model import engine, Pocket, Safe, Shop, Transaction
from models.testdata import createTestData, clearTestData
from excep.excep import InsufficientVoucherException, InsufficientCoinException, SerialNotFoundException, ConfigException

try:
    config.add_section("Database")
except:
    pass
config.set("Database", "connectionstring",'sqlite://')

from helpers.staffmanagement import StaffManagement


class Test_Transactor(unittest.TestCase):
 
    def setUp(self):
        """Call before every test case."""
        self.session = sessionmaker(bind=engine)()
        self.snr = '270bdee4'
        clearTestData()
        createTestData()
        try:
            config.add_section('Shop')
        except:
            pass

        config.set('Shop','voucher_percentage','33.3334')
        config.set('Shop','fee_percentage','1')
        config.set('Shop','name','enviclean')
        config.set('Shop','location','onestop1')
        


        self.shopName = config.get('Shop', 'name')
        self.shop = self.session.query(Shop).filter_by(name=self.shopName).first()
 
    def tearDown(self):
        """Call after every test case."""
        
        self.session.close()
        pass
 
    def testCheckinPass(self):

        StaffManagement(self.session).checkIn("aaaaaaaa")  
 
        transaction = self.session.query(Transaction).filter_by(note="checkin").first()

        
        self.assertTrue(isinstance(transaction, Transaction)) 
        self.assertEqual(transaction.fee, 0)
        self.assertEqual(transaction.voucherAmount, 0)
        self.assertEqual(transaction.note, "checkin")
    
    def testCheckOutPass(self):

        StaffManagement(self.session).checkOut("aaaaaaaa")  
 
        transaction = self.session.query(Transaction).filter_by(note="checkout").first()

        self.assertTrue(isinstance(transaction, Transaction)) 
        self.assertEqual(transaction.fee, 0)
        self.assertEqual(transaction.voucherAmount, 0)
        self.assertEqual(transaction.note, "checkout")
            
      
    def testCheckInOutInvalidSerial(self):

        self.assertRaises(SerialNotFoundException, StaffManagement(self.session).checkInOut, "12345678", "checkin")
        self.assertRaises(SerialNotFoundException, StaffManagement(self.session).checkInOut, "sdfsfsd", "checkin")
        
  

if __name__ == "__main__":
    unittest.main() # run all tests

#!/usr/bin/env python

from distutils.core import setup

setup(name='zebra',
      version='1.0',
      description='Electronic Payment System',
      author='Rasmus Steiniche, Rasmus Prentow',
      author_email='info@pay-e-safe.com',
      url='http://www.pay-e-safe.com',
      packages=['.'],
     )